package com.junen.vtaxi.activity.account;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.junen.vtaxi.R;
import com.junen.vtaxi.activity.base.BaseActivity;
import com.junen.vtaxi.app.BaseApplication;
import com.junen.vtaxi.engine.AccountEngine;
import com.junen.vtaxi.http.ApiClient;
import com.junen.vtaxi.model.ValuationResult;
import com.junen.vtaxi.rpc.RpcExcutor;
import com.junen.vtaxi.rpc.api.RpcApi;

public class ValuationRulesActivity extends BaseActivity {
	/** 计价规则 */
	private RpcExcutor<ValuationResult> valuationExcutor;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_valuation_rules);
		
		initView();
		initData();
	}

	@Override
	public void initView() {
		super.initView();
		Button backView = (Button)findViewById(R.id.close_view);
		backView.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}

	@Override
	public void initData() {
		super.initData();
		initRpcExcutor();
		valuationExcutor.start();
	}

	protected void initRpcExcutor() {
		valuationExcutor = new RpcExcutor<ValuationResult>(this, 0, false) {
			@Override
			public void excute(Object... params) {
				ApiClient.createApi(ValuationRulesActivity.this, RpcApi.class)
						.getValuation(3, 
								BaseApplication.getInstance().getDriverId(),
								this);
			}

			@Override
			public void onRpcFinish(ValuationResult result, Object... params) {
				doResult(result);
			}

			@Override
			public void onRpcException(int code, String msg, Object... params) {
				toast(msg, Toast.LENGTH_SHORT);
				updateView();
			}

		};
		valuationExcutor.setShowProgressDialog(true);
		valuationExcutor.setShowNetworkErrorView(false);
	}
	
	private void doResult(ValuationResult result){
		if(null==result){
			return;
		}
		AccountEngine.saveValuationRules(result);
		updateView();
	}
	
	private void updateView(){
		TextView distanceRules = (TextView)findViewById(R.id.distance_rules_view);
		TextView timeRules = (TextView)findViewById(R.id.time_rules_view);
		String[] valuationRules = AccountEngine.getValuationText();
		distanceRules.setText(valuationRules[0]);
		timeRules.setText(valuationRules[1]);
	}
}
