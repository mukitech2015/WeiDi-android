package com.junen.vtaxi.model;

/**
 * Created by new on 16/7/8.
 * 消息
 */
public class MessageItem {
    public String id;
    public String title;
    public String time;
    public String url;
    public int state;//1-已读，0-未读
}
