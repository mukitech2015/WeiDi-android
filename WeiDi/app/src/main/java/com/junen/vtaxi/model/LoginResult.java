package com.junen.vtaxi.model;

public class LoginResult {
	public UserInfo info;
	public MilesValuation milesvaluation;
	public TimeValuation timevaluation;
	public PadBanner padbanner;
	public ApkInfo apkinfo;
	public HolidayInfo holidayinfo;
}
