package com.junen.vtaxi.activity.account;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;

import com.junen.vtaxi.R;
import com.junen.vtaxi.activity.base.BaseActivity;
import com.junen.vtaxi.adapter.VideoAdapter;
import com.junen.vtaxi.engine.AccountEngine;
import com.junen.vtaxi.model.VideoInfo;
import com.junen.vtaxi.util.SdUtil;

import java.io.File;
import java.util.ArrayList;

public class VideoCenterActivity extends BaseActivity {
	private GridView downloadGrideView;
	private GridView downloadingGrideView;
	private ArrayList<VideoInfo> downloadArray;
	private ArrayList<VideoInfo> loadingArray;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_video_center);
		initView();
		initData();
	}
	@Override
	public void initView() {
		super.initView();
		Button backView = (Button)findViewById(R.id.close_view);
		backView.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		downloadGrideView = (GridView)findViewById(R.id.download_grid_view);
		downloadingGrideView = (GridView)findViewById(R.id.downloading_grid_view);
	}

	@Override
	public void initData() {
		super.initData();
		new Handler().postDelayed(new Runnable(){

			@Override
			public void run() {
				initDownloadVideo();
			}
		}, 200);
	}
	@SuppressLint("NewApi")
	private void initDownloadVideo(){
		downloadArray = new ArrayList<VideoInfo>();
		loadingArray = new ArrayList<VideoInfo>();
		VideoAdapter adapter = new VideoAdapter(this,downloadArray,true);
		downloadGrideView.setAdapter(adapter);
		VideoAdapter loadingAdapter = new VideoAdapter(this,loadingArray,false);
		downloadingGrideView.setAdapter(loadingAdapter);
		ArrayList<VideoInfo> list = AccountEngine.getVideoArray();
		if(list!=null){
			for(int i=0;i<list.size();i++){
				VideoInfo videoInfo = list.get(i);
				if(!TextUtils.isEmpty(videoInfo.sdPath)){
					File videoFile = new File(videoInfo.sdPath);
					int fileSize = (int) SdUtil.getFileSize(videoFile);
					if(videoFile.exists() && !videoFile.isDirectory() && fileSize==videoInfo.size){
						downloadArray.add(videoInfo);
						adapter.notifyDataSetChanged();
					}else{
						loadingArray.add(videoInfo);
						loadingAdapter.notifyDataSetChanged();
					}
				}else{
					loadingArray.add(videoInfo);
					loadingAdapter.notifyDataSetChanged();
				}
			}
		}
	}
}
