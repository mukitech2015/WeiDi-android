package com.junen.vtaxi.widget;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.junen.vtaxi.R;

/**
 * Created by root on 16-12-17.
 */

public class CustomDialog  extends Dialog {
    public TextView  yeTv;
    public EditText  sqEt;
    public  TextView  cancel;
    public  TextView  confirm;
    private InputMethodManager imm;
    private RelativeLayout txLat;
    public CustomDialog(Context context) {
        super(context, R.style.activity_dialog);
        imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setCancelable(false);  // 是否可以撤销
        setContentView(R.layout.withdraw_dialog);
        yeTv= (TextView) findViewById(R.id.yeTv);
        sqEt= (EditText) findViewById(R.id.sqEt);
        cancel= (TextView) findViewById(R.id.cancel);
        confirm= (TextView) findViewById(R.id.confirm);
        txLat= (RelativeLayout) findViewById(R.id.txLat);
        txLat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showKeyboard(sqEt);
            }
        });
    }

    public  void  setCancelBtn(View.OnClickListener onClickListener ){
        cancel.setOnClickListener(onClickListener);
    }
    public  void  setConfirmBtn(View.OnClickListener onClickListener){
        confirm.setOnClickListener(onClickListener);
    }
    public  void   setYeTV(String   yeStr){
         yeTv.setText(yeStr);
    }
    public  void  setSqEt(String  sqStr){
        sqEt.setText(sqStr);
    }
    public String   getSqEt(  ){
        return  sqEt.getText().toString();
    }
    /**
     * 展示键盘
     *
     * @param et 编辑框
     */
    public void showKeyboard(EditText et) {
        et.requestFocus();
        et.setSelectAllOnFocus(true);
        et.selectAll();
        // 显示键盘
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    /**
     * 隐藏键盘
     *
     * @param v 编辑框
     */
    public void hideKeyboard(View v) {
        if (imm.isActive()) {
            // 如果键盘为激活状态，则隐藏键盘
            imm.hideSoftInputFromWindow(v.getApplicationWindowToken(), 0);
        }
    }

}




