package com.junen.vtaxi.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.graphics.Bitmap;
import android.os.Environment;
import android.util.Log;

/*
 * SD卡管理类
 */
public class SdUtil {
	// 判断SD卡是否可用
	public static boolean getStatusSD() {
		if (Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED)) {
			return true;
		} else {
			return false;
		}
	}

	// 到的该目录的File
	public static File getFileByString(String path) {

		File file = null;
		if (getStatusSD() == true) {
			// SD卡路径
			file = new File(path);
		}

		return file;
	}

	// 得到word文件夹里的文件
	public static File getFile(String path) {
		String FilePath = Environment.getExternalStorageDirectory() + path;
		File file = null;
		if (getStatusSD() == true) {
			// SD卡路径
			file = new File(FilePath);
		}

		return file;
	}

	/**
	 * 存储图片
	 * 
	 * @param bp
	 *            ,位图
	 * @param packages
	 *            ,SD卡上文件夹
	 * @return path
	 */
	public static String createFile(Bitmap bp, String packages) {
		if (bp == null) {
			return null;
		}
		ByteArrayOutputStream baos = null;
		String _path = null;
		try {
			String sign_dir = Environment.getExternalStorageDirectory()
					+ File.separator + packages + File.separator;
			File mDirFile = new File(sign_dir);
			if (!mDirFile.exists()) {
				mDirFile.mkdirs();
			}
			_path = sign_dir + System.currentTimeMillis() + ".png";
			baos = new ByteArrayOutputStream();
			bp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
			byte[] photoBytes = baos.toByteArray();
			if (photoBytes != null) {
				new FileOutputStream(new File(_path)).write(photoBytes);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (baos != null)
					baos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return _path;
	}

	/**
	 * 存储图片
	 * 
	 * @param bp
	 *            ,位图
	 * @param packages
	 *            ,SD卡上文件夹
	 * @return name,文件名称
	 */
	public static String createFileName(Bitmap bp, String packages) {
		if (bp == null) {
			return null;
		}
		ByteArrayOutputStream baos = null;
		String _path = null;
		String name = null;
		try {
			String sign_dir = Environment.getExternalStorageDirectory()
					+ File.separator + packages + File.separator;

			File mDirFile = new File(sign_dir);
			if (!mDirFile.exists()) {
				mDirFile.mkdirs();
			}
			name = System.currentTimeMillis() + ".jpg";
			_path = sign_dir + name;
			baos = new ByteArrayOutputStream();
			bp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
			byte[] photoBytes = baos.toByteArray();
			if (photoBytes != null) {
				new FileOutputStream(new File(_path)).write(photoBytes);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (baos != null)
					baos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return name;
	}

	/**
	 * 获取指定文件大小
	 * 
	 * @param f
	 * @return
	 * @throws Exception
	 */
	public static long getFileSize(File file){
		long size = 0;
		try {
			if (file.exists()) {
				FileInputStream fis = null;
				fis = new FileInputStream(file);
				size = fis.available();
			} else {
				file.createNewFile();
				Log.e("获取文件大小", "文件不存在!");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return size;
	}
}
