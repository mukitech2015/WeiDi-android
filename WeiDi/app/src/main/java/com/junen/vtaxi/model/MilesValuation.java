package com.junen.vtaxi.model;

public class MilesValuation {
	public int tag;
	public int version;
	public float initprice;//起步价/元
	public float initmiles;//起步价内的公里数
	public float everymilesprice;//每公里价格/元
	public String premiumstime;//溢价开始时间
	public String premiumetime;//溢价结束时间
	public float premiumtimepricepercent;//溢价百分比
	public String reducestime;//打折开始时间
	public String reduceetime ;//打折结束时间
	public float reducepricepercent;//打折百分比
	public float premiummiles;//溢价里程
	public float premiummilespricepercent;//溢价里程的溢价百分比
	public int waittime;//等待时间折合1公里
	public float intervaltime;//间隔跳表价
}
