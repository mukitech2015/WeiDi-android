package com.junen.vtaxi.common;

/**
 * 公共信息
 */
public class Constant {
	public static final int SYSTEM_ERROR_CODE = 10000;
	public static final int NETWORK_ERROR_CODE = 10001;


	public static final String UPLOAD_POSITION_TAG = "upload-position.json";
	
	
	public static final String UPDATE_LOCATION_ACTION = "com.zxw.weidi.update.location";
	public static final String LOCATION_STATUS = "com.zxw.weidi.location.status";
	public static final String LOCATION_DATA = "com.zxw.weidi.location.data";
	public static final int LOCATION_SUCESS = 1;
	public static final int LOCATION_START = 2;
	public static final int LOCATION_END = 3;
	
	
	public static final String SAVE_DATA_ACTION = "com.zxw.weidi.save.data";
	public static final String NOTICATION_CLICK_ACTION = "com.zxw.weidi.notication.click.action";
	
	//下载服务
	public static final String PROGRESS_ACTION = "com.dwd.rider.progressbar.progress.action";
	public static final String PROGRESS_STATUS_KEY = "PROGRESS_STATUS";
	public static final String PROGRESSBAR_PROGRESS_KEY = "PROGRESSBAR_PROGRESS";
	
	public static final String DOWNLOAD_TYPE = "DOWNLOAD_TYPE";
	public static final String DOWNLOAD_URL = "DOWNLOAD_URL";
	public static final String DOWNLOAD_FILE = "DOWNLOAD_FILE";
	public static final String DOWNLOAD_PACKAGE = "DOWNLOAD_PACKAGE";
	public static final String DOWNLOAD_TIP = "DOWNLOAD_TIP";

	public static final String DOWNLOAD_FILE_TYPE= "DOWNLOAD_TIP";

 	public static final String VIDEO_COMPETE = "com.zxw.weidi.video.download.complete";
	public static final String VOICE_START_COMPETE = "com.zxw.weidi.startvoice.download.complete";
	public static final String VOICE_END_COMPETE = "com.zxw.weidi.endvoice.download.complete";
	public static final String DOWNLOAD_SD_PATH = "DOWNLOAD_SD_PATH";

	public static final String SELECT_PRINTER_ACTION = "com.zxw.weidi.printer.select";

	
	public static final String LOGIN_ACCOUNT_KEY = "LOGIN_ACCOUNT";
	public static final String LOGIN_USER_NAME_KEY = "LOGIN_USER_NAME";
	
	//打印机mac地址
	public static final String PRINTER_MAC_ADDRESS_KEY = "PRINTER_MAC_ADDRESS";
	
	//账户信息
	public static final String LOGIN_RESULT_KEY = "LOGIN_RESULT";
	public static final String USER_INFO_ID_KEY = "USER_INFO_ID";
	public static final String USER_INFO_NAME_KEY = "USER_INFO_NAME";
	public static final String USER_INFO_PHONE_KEY = "USER_INFO_PHONE";
	public static final String USER_INFO_CARNO_KEY = "USER_INFO_CARNO";
	public static final String USER_INFO_SGCARDNO_KEY = "USER_INFO_SGCARDNO";
	public static final String USER_INFO_ADDRESS_KEY = "USER_INFO_ADDRESS";
	public static final String USER_INFO_LOGOURL_KEY = "USER_INFO_LOGOURL";
	public static final String USER_INFO_MONEY_KEY = "USER_INFO_MONEY";
	public static final String USER_INFO_SCORE_KEY = "USER_INFO_SCORE";
	public static final String USER_INFO_ORDERNUM_KEY = "USER_INFO_ORDERNUM";
	public static final String USER_INFO_GNAME_KEY = "USER_INFO_GNAME";
	public static final String USER_INFO_SEX_KEY = "USER_INFO_SEX";
	
	//里程计价
	public static final String MILE_VERSION_KEY = "MILE_VERSION";
	public static final String MILE_INIT_PRICE_KEY = "MILE_INIT_PRICE";
	public static final String MILE_INIT_MILES_KEY = "MILE_INIT_MILES";
	public static final String MILE_PER_KM_PRICE_KEY = "MILE_PER_KM_PRICE";
	public static final String MILE_PREMIUM_START_TIME_KEY = "MILE_PREMIUM_START_TIME";
	public static final String MILE_PREMIUM_END_TIME_KEY = "MILE_PREMIUM_END_TIME";
	public static final String MILE_PREMIUM_TIME_PRICE_KEY = "MILE_PREMIUM_PRICE";
	public static final String MILE_PREMIUM_KM_KEY = "MILE_PREMIUM_KM";
	public static final String MILE_PREMIUM_KM_PRICE_KEY = "MILE_PREMIUM_KM_PRICE";
	public static final String MILE_WAIT_TIME_KM_KEY = "MILE_WAIT_TIME_KM";
	public static final String MILE_INTERVAL_PRICE_KEY = "MILE_INTERVAL_PRICE";
	//减价
	public static final String MILE_REDUCE_START_TIME_KEY = "MILE_REDUCE_START_TIME";
	public static final String MILE_REDUCE_END_TIME_KEY = "MILE_REDUCE_END_TIME";
	public static final String MILE_REDUCE_TIME_PRICE_KEY = "MILE_REDUCE_TIME_PRICE";


	//时间计价
	public static final String TIME_VERSION_KEY = "TIME_VERSION";
	public static final String TIME_PER_PRICE_KEY = "TIME_PER_PRICE";
	//Banner
	public static final String BANNER_VERSION_KEY = "BANNER_VERSION";
	public static final String BANNER_CONTENT_KEY = "BANNER_CONTENT";

	/**
	  语音播报
	 * */
	public static final String VOICE_START_RESULT_KEY = "VOICE_START_RESULT";
	public static final String VOICE_START_VERSION_KEY = "VOICE_START_VERSION";
	public static final String VOICE_START_ID_KEY = "VOICE_START_ID";
	public static final String VOICE_START_NAME_KEY = "VOICE_START_NAME";
	public static final String VOICE_START_SIZE_KEY = "VOICE_START_SIZE";
	public static final String VOICE_START_URL_KEY = "VOICE_START_URL";
	public static final String VOICE_START_PATH_KEY = "VOICE_START_PATH";

	public static final String VOICE_END_RESULT_KEY = "VOICE_END_RESULT";
	public static final String VOICE_END_VERSION_KEY = "VOICE_END_VERSION";
	public static final String VOICE_END_ID_KEY = "VOICE_END_ID";
	public static final String VOICE_END_NAME_KEY = "VOICE_END_NAME";
	public static final String VOICE_END_SIZE_KEY = "VOICE_END_SIZE";
	public static final String VOICE_END_URL_KEY = "VOICE_END_URL";
	public static final String VOICE_END_PATH_KEY = "VOICE_END_PATH";



	//视频
	public static final String ADVERT_RESULT_KEY = "ADVERT_RESULT";
	public static final String ADVERT_VERSION_KEY = "ADVERT_VERSION";
	public static final String ADVERT_SUM_KEY = "ADVERT_SUM";
	public static final String ADVERT_VIDEO_ID_KEY = "ADVERT_VIDEO_ID_";
	public static final String ADVERT_VIDEO_NAME_KEY = "ADVERT_VIDEO_NAME_";
	public static final String ADVERT_VIDEO_SIZE_KEY = "ADVERT_VIDEO_SIZE_";
	public static final String ADVERT_VIDEO_URL_KEY = "ADVERT_VIDEO_URL_";
	public static final String ADVERT_VIDEO_LOGOURL_KEY = "ADVERT_VIDEO_LOGOURL_";
	public static final String ADVERT_VIDEO_PATH_KEY = "ADVERT_VIDEO_PATH_";


	public static final String WEBVIEW_TITLE_KEY = "WEBVIEW_TITLE";
	public static final String WEBVIEW_URL_KEY = "WEBVIEW_URL";
	public static final String MESSAGE_ID_KEY = "MESSAGE_ID";

	public static final int READ_MESSAGE_DETAIL = 20011;
}
