package com.junen.vtaxi.model;

import android.graphics.Bitmap;

public class VideoInfo {
	public String id;
	public String name;
	public int size;
	public String url;
	public String logourl;
    public String sdPath;
	public int complete;//1-下载完成
	public Bitmap bmp;
}
