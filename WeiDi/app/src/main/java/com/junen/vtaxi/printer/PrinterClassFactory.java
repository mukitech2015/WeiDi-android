package com.junen.vtaxi.printer;

import android.content.Context;
import android.os.Handler;

import com.junen.vtaxi.printer.service.BtService;
import com.junen.vtaxi.printer.service.WifiService;

public class PrinterClassFactory {
	public static PrinterClass create(int type,Context _context,Handler _mhandler,Handler _handler){
        if(type==0){
               return new BtService(_context,_mhandler, _handler); 
        }else if(type==1){
              return new WifiService(_context,_mhandler, _handler); 
        }
		return null;
  }

}

