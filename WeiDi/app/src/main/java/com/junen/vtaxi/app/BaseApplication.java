package com.junen.vtaxi.app;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Toast;

import com.junen.vtaxi.R;
import com.junen.vtaxi.activity.commom.LoadingActivity;
import com.junen.vtaxi.activity.commom.LoginActivity;
import com.junen.vtaxi.engine.AccountEngine;
import com.junen.vtaxi.model.PrintEvent;
import com.junen.vtaxi.printer.PrintService;
import com.junen.vtaxi.printer.PrinterClass;
import com.junen.vtaxi.printer.PrinterClassFactory;
import com.junen.vtaxi.util.AppUtils;
import com.wch.wchusbdriver.CH34xAndroidDriver;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Stack;

import de.greenrobot.event.EventBus;

public class BaseApplication extends Application {
	final static String TAG = BaseApplication.class.getSimpleName();
	
	private static BaseApplication instance;
	/**
	 * 屏幕分辨率
	 */
	public static int screenWidth = 720;
	public static int screenHeight = 1230;
		
	/**
	 * 该App的Activity栈
	 */
	private Stack<WeakReference<Activity>> mActivitys;
	// App版本号
	public static String appVersion = null;
	// 系统版本号
	public static String sysVersion = null;
	// 设备唯一标志
	public static String mac = null;
	//userId
	public static String driverId;
		
	public static int locationAccuracy = 150;
	
	
	
	public BaseApplication() {
		mActivitys = new Stack<WeakReference<Activity>>();
	}
	
	/*
	 * 行程用时
	 */
	public static int usedTimeS = 0;
	public static int usedTimeM = 0;
	public static int usedTimeH = 0;
	/*
	 * 行程等待用时
	 */
	public static int waitTimeS = 0;
	public static int waitTimeM = 0;
	public static int waitTimeH = 0;

	//里程
	public static float lastDistance = 0;
	//总价
	public static float totalPrice = 0;
	public static int valuationType;//0-里程，1-时间
	public static int isPremium;//0-不溢价，1-溢价,2-减价
	public static int locationType = 0;//0-普通定位，1-计价定位
	public static int locationStatus = 0;//0－未计价，1-正在计价，2-暂停计价,3-停止计价
	public static String orderId = null;
	//打印信息
	public static String orderNo;
	public static String orderDate;
	public static String orderStartTime;
	public static String orderEndTime;
	public static String orderTotalPrice;
	public static String zfbqrcode;
	public static String wxqrcode;
	//消息ID
	public static String readMessageId;
	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
		mac = AppUtils.getLocalMacAddress();
		appVersion = AppUtils.getVersionName(this);
		sysVersion = "android" + Build.VERSION.RELEASE;
		initScreen();
		System.setProperty("http.keepAlive", "false");
		initPrinter();


	}
	public final void destroyActivitys() {
		WeakReference<Activity> reference;
		Activity activity;
		while (!mActivitys.isEmpty() && (reference = mActivitys.pop()) != null) {
			activity = reference.get();
			if (activity != null && !activity.isFinishing()) {
				activity.finish();
			}
		}
	}

	/**
	 * Activity入栈
	 * 
	 * @param activity
	 */
	public final void pushActivity(Activity activity) {
		if (!mActivitys.isEmpty() && mActivitys.peek().get() == null) {// 被恢复的时候替换
			mActivitys.pop();
		}
		WeakReference<Activity> item = new WeakReference<Activity>(activity);
		mActivitys.push(item);
		Log.v(TAG, "pushActivity(): "
				+ activity.getComponentName().getClassName());
	}

	/**
	 * 移除Activity
	 * 
	 * @param activity
	 */
	public void removeActivity(Activity activity) {
		WeakReference<Activity> dirtyItem = null;
		for (WeakReference<Activity> item : mActivitys) {
			if (item.get() == null) {
				Log.w(TAG, "activity has be finallized.");
				continue;
			}
			if (item.get() == activity) {
				dirtyItem = item;
				break;
			}
		}
		mActivitys.remove(dirtyItem);
		Log.d(TAG, "remove Activity:" + activity.getClass().getName());
	}

	/**
	 * 通过Hashcode查找Activity
	 * 
	 * @param code
	 *            Hashcode
	 * @return
	 */
	public Activity findActivityByHashcode(int code) {
		for (WeakReference<Activity> reference : mActivitys) {
			Activity activity = reference.get();
			if (activity == null)
				continue;
			if (activity.hashCode() == code) {
				return activity;
			}
		}
		return null;
	}

	/**
	 * 获得焦点
	 */
	public void windowFocus() {
		// getMicroApplicationContext().onWindowFocus(this);
	}

	/**
	 * 获取栈顶Activity
	 */
	public Activity getTopActivity() {
		if (mActivitys.isEmpty())
			return null;
		return mActivitys.peek().get();
	}

	public void saveState(Editor editor) {
		// editor.putInt(getAppId() + ".stack", mActivitys.size());
	}

	public void restoreState(SharedPreferences preferences) {
		// int num = preferences.getInt(getAppId() + ".stack", 0);
		// for (int i = 0; i < num; i++) {
		// mActivitys.push(new WeakReference<Activity>(null));
		// }
	}

	public static BaseApplication getInstance() {
		return instance;
	}
	
	
	private void initScreen() {
		DisplayMetrics dm;
		dm = getResources().getDisplayMetrics();
		screenWidth = dm.widthPixels;
		screenHeight = dm.heightPixels;
		System.out.println("width:"+screenWidth);
		System.out.println("height:"+screenHeight);
		Log.e("", "width:"+screenWidth);
		Log.e("", "height:"+screenHeight);
	}
	
	
	class MyUnCaughtExceptionHandler implements Thread.UncaughtExceptionHandler{

        @Override
        public void uncaughtException(Thread thread, Throwable ex) {
            ex.printStackTrace();
            Log.e("", "异常报错处理。。。");
            /*
            Intent saveIntent = new Intent(Constant.SAVE_DATA_ACTION);
            sendBroadcast(saveIntent);
            */
            
            Intent intent = new Intent(BaseApplication.this, LoadingActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            BaseApplication.this.startActivity(intent);
            
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
        }
    }

	/*
	 * 打印机
	 */
	public static PrinterClass pl = null;// 打印机操作类
	public static CH34xAndroidDriver uartInterface;// USB Printer Control
	private static final String ACTION_USB_PERMISSION = "com.wch.wchusbdriver.USB_PERMISSION";

	public static final int MESSAGE_STATE_CHANGE = 1;
	public static final int MESSAGE_READ = 2;
	public static final int MESSAGE_WRITE = 3;
	public static final int MESSAGE_DEVICE_NAME = 4;
	public static final int MESSAGE_TOAST = 5;
	Handler mhandler = null;
	Handler handler = null;
	
	BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
	private ArrayList<BluetoothDevice> deviceArray = new ArrayList<BluetoothDevice>();
	private String macAddress;
	private boolean tvFlag;
	private Thread tv_update;
	
	public ArrayList<BluetoothDevice> getDevices(){
		return deviceArray;
	}
	
	public void createPrinter() {
		//disconnect();
		if(pl==null){
			pl = PrinterClassFactory.create(0, this, mhandler, handler);
		}
		if (pl.getState() != PrinterClass.STATE_CONNECTED) {
			startScan();
		}else{
			print();
		}
	}
	
	public void openBluetooth(){
		if (pl!=null && !pl.IsOpen()) {
			pl.open(this);
			return;
		}
	}
	private void startScan(){
		adapter = BluetoothAdapter.getDefaultAdapter();
		if(adapter==null){
			return;
		}
		deviceArray.clear();
		if (!pl.IsOpen()) {
			pl.open(this);
			return;
		}
		IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
		IntentFilter filter2 = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
		registerReceiver(new BlutetoothReceiver(), filter);
		registerReceiver(new BlutetoothReceiver(), filter2);
		adapter.startDiscovery();
		tvFlag = true;
		tv_update = new Thread() {
			public void run() {
				while (tvFlag) {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if (pl != null) {
						if (pl.getState() == PrinterClass.STATE_CONNECTED) {
							tvFlag=false;
							pl.stopScan();
							if(!TextUtils.isEmpty(macAddress)){
								AccountEngine.savePrinterMac(macAddress);
							}
							print();
						}
					}
				}
			}
		};
		tv_update.start();
	}
	
	public  class BlutetoothReceiver extends BroadcastReceiver{
		@Override  
		public void onReceive(Context context, Intent intent) { 
			if(intent.getAction()==BluetoothDevice.ACTION_FOUND){
				Log.e("==========","BluetoothDevice.ACTION_FOUND");
				//从收到的intent对象中将代表远程蓝牙设配器的对象取出  
			    BluetoothDevice devices = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE); 
			    if(devices.getBondState() == BluetoothDevice.BOND_BONDED){
			    	if(TextUtils.isEmpty(macAddress)){
			    		int size = deviceArray.size();
			    		if(size==0){
			    			Log.e("", "devices1:"+devices.getName()+"-:"+devices.getAddress());
			    			deviceArray.add(devices);
			    		}else{
			    			boolean isHas = false;
			    			for(int i=0;i<size;i++){
					    		if(TextUtils.equals(deviceArray.get(i).getAddress(), devices.getAddress())){
					    			isHas = true;
					    			break;
					    		}
					    	}
			    			if(!isHas){
			    				Log.e("", "devices:"+devices.getName()+"-:"+devices.getAddress());
				    			deviceArray.add(devices);
			    			}
			    		}
				    	
			    	}else{
			    		if(TextUtils.equals(macAddress, devices.getAddress())){
			    			pl.connect(macAddress);
			    		}
			    	}
			    }
			}else if(intent.getAction()==BluetoothAdapter.ACTION_DISCOVERY_FINISHED){
				Log.e("==========","ACTION_DISCOVERY_FINISHED");

					if(deviceArray!=null){
					int size = deviceArray.size();
					boolean canConnect = false;
					for(int i=0;i<size;i++){
						BluetoothDevice device = deviceArray.get(i);
						if(TextUtils.equals(device.getAddress(), macAddress)){
							canConnect = true;
						}
					}
					if(!canConnect && size>0){
						EventBus.getDefault().post(new PrintEvent(true));
					}
				}
			}
		}  
	} 
	
	public void connect(String mac){
		//disconnect();
		macAddress = mac;
		Log.e("", "macAddress:"+macAddress);
		pl.connect(macAddress);
	}

	public void disconnect(){
		if(pl!=null && pl.getState()==PrinterClass.STATE_CONNECTED){
			pl.disconnect();
		}
	}
	
	public void close(){
		if(pl!=null){
			pl.close(getApplicationContext());
		}
		pl = null;
	}
	/**
	 *  蓝牙打印图文
	 */
	public void print(){
		new Thread() {
			public void run() {
				Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.printer_icon_150);
				String msg = AccountEngine.getPrinterOrderInfo();
				Bitmap code = BitmapFactory.decodeResource(getResources(), R.drawable.printer_code);
                pl.printImageAndText(icon,msg,code);
			}
		}.start();
	}
	private void initPrinter() {
		macAddress = AccountEngine.getPrinterMac();
		mhandler =  new Handler() {
			public void handleMessage(Message msg) {
				switch (msg.what) {
				case MESSAGE_READ:
					byte[] readBuf = (byte[]) msg.obj;
					if (readBuf[0] == 0x13) {
						PrintService.isFUll = true;
						ShowMsg(getResources().getString(
								R.string.str_printer_state)
								+ ":"
								+ getResources().getString(
										R.string.str_printer_bufferfull));
					} else if (readBuf[0] == 0x11) {
						PrintService.isFUll = false;
						ShowMsg(getResources().getString(
								R.string.str_printer_state)
								+ ":"
								+ getResources().getString(
										R.string.str_printer_buffernull));
					} else if (readBuf[0] == 0x08) {
						ShowMsg(getResources().getString(
								R.string.str_printer_state)
								+ ":"
								+ getResources().getString(
										R.string.str_printer_nopaper));
					} else if (readBuf[0] == 0x01) {
						// ShowMsg(getResources().getString(R.string.str_printer_state)+":"+getResources().getString(R.string.str_printer_printing));
					} else if(readBuf[0] == 0x00){
						Log.e("", "未打印.....");
					} else if(readBuf[0] == 0x01){
						Log.e("", "正在打印.....");
					}else if (readBuf[0] == 0x04) {
						ShowMsg(getResources().getString(
								R.string.str_printer_state)
								+ ":"
								+ getResources().getString(
										R.string.str_printer_hightemperature));
					} else if (readBuf[0] == 0x02) {
						ShowMsg(getResources().getString(
								R.string.str_printer_state)
								+ ":"
								+ getResources().getString(
										R.string.str_printer_lowpower));
					} else {
						String readMessage = new String(readBuf, 0, msg.arg1);
						if (readMessage.contains("800"))// 80mm paper
						{
							PrintService.imageWidth = 72;
							Toast.makeText(getApplicationContext(), "80mm",
									Toast.LENGTH_SHORT).show();
						} else if (readMessage.contains("580"))// 58mm paper
						{
							PrintService.imageWidth = 48;
							Toast.makeText(getApplicationContext(), "58mm",
									Toast.LENGTH_SHORT).show();
						}
					}
					break;
				case MESSAGE_STATE_CHANGE:// 蓝牙连接状
					switch (msg.arg1) {
					case PrinterClass.STATE_CONNECTED:// 已经连接
						break;
					case PrinterClass.STATE_CONNECTING:// 正在连接
						Toast.makeText(getApplicationContext(),
								"正在连接", Toast.LENGTH_SHORT).show();
						break;
					case PrinterClass.STATE_LISTEN:
					case PrinterClass.STATE_NONE:
						break;
					case PrinterClass.SUCCESS_CONNECT:
						pl.write(new byte[] { 0x1b, 0x2b });// 检测打印机型号
						Toast.makeText(getApplicationContext(),
								"连接成功", Toast.LENGTH_SHORT).show();
						break;
					case PrinterClass.FAILED_CONNECT:
						Toast.makeText(getApplicationContext(),
								"连接失败", Toast.LENGTH_SHORT).show();

						break;
					case PrinterClass.LOSE_CONNECT:
						Toast.makeText(getApplicationContext(), "连接已断开",
								Toast.LENGTH_SHORT).show();
					}
					break;
				case MESSAGE_WRITE:
					break;
				}
				super.handleMessage(msg);
			}
		};

		handler = new Handler() {
			@Override
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				switch (msg.what) {
				case 0:
					break;
				case 1:// 扫描完毕
					Toast.makeText(getApplicationContext(),
							"扫描完毕", Toast.LENGTH_SHORT).show();
					break;
				case 2:// 停止扫描
					Toast.makeText(getApplicationContext(),
							"停止扫描", Toast.LENGTH_SHORT).show();
					break;
				}
			}
		};
	}
	
	private void ShowMsg(String msg) {
		Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
	}
	
	public void setDriverId(String id){
		this.driverId = id;
	}
	
	public String getDriverId(){
		if(TextUtils.isEmpty(driverId)){
			driverId = AccountEngine.getDriverId();
		}
		return driverId;
	}
	
	
	/*
	 * 关闭应用程序
	 */
	public void exit() {
		WeakReference<Activity> reference;
		Activity activity;
		while (!mActivitys.isEmpty() && (reference = mActivitys.pop()) != null) {
			activity = reference.get();
			if (activity != null && !activity.isFinishing()) {
				activity.finish();
			}
		}
		mActivitys.clear();
		System.exit(0);
	}

	/*
	 * 重启App
	 */
	public void restart(Activity currentActivity) {
		WeakReference<Activity> reference;
		Activity activity;
		while (!mActivitys.isEmpty() && (reference = mActivitys.pop()) != null) {
			activity = reference.get();
			if (activity != null && !activity.isFinishing()) {
				activity.finish();
			}
		}
		mActivitys.clear();
		Intent intent = new Intent(currentActivity,LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        currentActivity.startActivity(intent);
        currentActivity.finish();
	}
	
	public void initData(){
		/*
		 * 行程用时
		 */
		usedTimeS = 0;
		usedTimeM = 0;
		usedTimeH = 0;
		/*
		 * 行程等待用时
		 */
		waitTimeS = 0;
		waitTimeM = 0;
		waitTimeH = 0;

		//里程
		lastDistance = 0;
		//总价
		totalPrice = 0;
		valuationType = 0;//0-里程，1-时间
		isPremium = 0;//0-不溢价，1-溢价
		locationType = 0;//0-普通定位，1-计价定位
		locationStatus = 0;//0－未计价，1-正在计价，2-暂停计价,3-停止计价
		orderId = null;
	}
}
