package com.junen.vtaxi.util;

import java.lang.reflect.Type;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.parser.Feature;

public class JsonUtils
{
  public static <T> T parseObject(String data, Class<T> cls)
  {
    return JSON.parseObject(data, cls);
  }

  public static Object parseObject(String data, Type type)
  {
    return JSON.parseObject(data, type, new Feature[0]);
  }

  public static JSONObject parseObject(String data)
  {
    return JSON.parseObject(data);
  }

  
  public static String jsonToString(Object object)
  {
    return JSON.toJSONString(object);
  }

  public static <T> List<T> jsonToArray(String paramString, Class<T> paramClass)
  {
    return JSON.parseArray(paramString, paramClass);
  }
}