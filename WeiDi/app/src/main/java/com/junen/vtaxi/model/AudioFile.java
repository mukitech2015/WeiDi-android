package com.junen.vtaxi.model;

/**
 * Created by muki on 2016/11/22.
 */

public class AudioFile {
    public int version;
    public int tag;
    public String fileurl;
    public String sdPath;
    public String name;
    public String id;
    public int   size;
}
