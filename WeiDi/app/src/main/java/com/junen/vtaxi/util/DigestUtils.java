package com.junen.vtaxi.util;

import java.security.MessageDigest;

/**
 * MD5 SHA-1加密
 * 
 * @author menglin.li
 * 
 */
public class DigestUtils {
	private static char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7',
			'8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

	public final static String MD5(String s) {
		String result = null;
		try {
			byte[] btInput = s.getBytes("UTF-8");
			MessageDigest mdInst = MessageDigest.getInstance("MD5");
			mdInst.update(btInput);
			byte[] md = mdInst.digest();
			result = new String(encodeHex(md));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	public static String SHA(String val) {
		String result = null;
		try {
			MessageDigest messageDiest = MessageDigest.getInstance("SHA-1");
			messageDiest.update(val.getBytes("UTF-8"));
			byte[] m = messageDiest.digest();// 加密
			result = new String(encodeHex(m));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	private static char[] encodeHex(final byte[] data) {
		final int l = data.length;
		final char[] out = new char[l << 1];
		for (int i = 0, j = 0; i < l; i++) {
			out[j++] = hexDigits[(0xF0 & data[i]) >>> 4];
			out[j++] = hexDigits[0x0F & data[i]];
		}
		return out;
	}
}
