package com.junen.vtaxi.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import com.junen.vtaxi.common.ICallBack;

import java.io.IOException;

/**
 * Created by muki on 2016/11/20.
 */

public class MusicalPlayService  extends Service {
    private MediaPlayer player;
    private boolean pause=false;
    @Override
    public void onCreate() {
        // TODO 自动生成的方法存根
        super.onCreate();
        player=new MediaPlayer();
    }
    @Override
    public IBinder onBind(Intent intent) {
        // TODO 自动生成的方法存根
        return new PlayerBinder();
    }

    private class PlayerBinder extends Binder implements MusicalPlayerINterface{

        @Override
        public void play(String path,ICallBack   iCallBack) {
            // TODO 自动生成的方法存根
            playing(path,iCallBack);
        }
        @Override
        public void pause() {
            // TODO 自动生成的方法存根
            pauseing();
        }

        @Override
        public void stop() {
            // TODO 自动生成的方法存根
            stoping();
        }

        @Override
        public void replay() {
            // TODO 自动生成的方法存根
            replaying();
        }

        @Override
        public boolean isVoicePlaying() {
            if (player.isPlaying()){
               return true;
            }
            return false;
        }
    }


    @Override
    public void onDestroy() {
        // TODO 自动生成的方法存根
        super.onDestroy();
        player.release();
    }
    private void playing (String path, final ICallBack iCallBack){
        // 音量控制,初始化定义
        final AudioManager mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        // 最大音量
        final int maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        player.reset();
        try {
            player.setDataSource(path);
            player.prepare();
        } catch (IllegalStateException e) {
            // TODO 自动生成的 catch 块
            e.printStackTrace();
        } catch (IOException e) {
            // TODO 自动生成的 catch 块
            e.printStackTrace();
        }
        player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

            @Override
            public void onPrepared(MediaPlayer mp) {
                // TODO 自动生成的方法存根
                mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC,maxVolume,0);
                player.setVolume(1.0f,1.0f);
                player.start();
            }
        });
        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                Log.e("tag", "播放完毕");
                //根据需要添加自己的代码。。。
                iCallBack.postExec();
            }
        });
    }
    private void pauseing(){
        if(player.isPlaying()){
            player.pause();
            pause=true;
        }else {
            if(pause){
                player.start();
                pause=false;
            }

        }
    }

    private void stoping(){
        if(player.isPlaying()){
            player.stop();
        }
    }

    private void replaying(){
        if(player.isPlaying()){
            player.seekTo(0);
        }
    }
}










