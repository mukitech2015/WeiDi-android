package com.junen.vtaxi.service;

import com.junen.vtaxi.common.ICallBack;

/**
 * Created by muki on 2016/11/20.
 */

public interface MusicalPlayerINterface {
     void  play(String   path, ICallBack  iCallBack);
     void  pause(  );
     void  stop(  );
     void  replay( );
     boolean isVoicePlaying( );
}
