package com.junen.vtaxi.model;

public class StopOrderResult {
	public String distanceno;
	public String sdate;
	public String stime;
	public String etime;
	public String zfbqrcode;
	public String wxqrcode;
}
