package com.junen.vtaxi.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.junen.vtaxi.activity.commom.LoadingActivity;

/**
 * Created by muki on 2016/12/19.
 */

public class BootBroadcastReceiver  extends BroadcastReceiver {
    static final String ACTION = "android.intent.action.BOOT_COMPLETED";
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(ACTION)) {
            Intent mainActivityIntent = new Intent(context,LoadingActivity.class);  // 要启动的Activity
            mainActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(mainActivityIntent);
        }
    }
}
