package com.junen.vtaxi.http;

import java.util.concurrent.TimeUnit;

import android.content.Context;

import com.squareup.okhttp.OkHttpClient;

public class AndroidOkHttpClient {
	private static OkHttpClient singleton;

	public static OkHttpClient getInstance(Context context) {
		if (singleton == null) {
			synchronized (AndroidOkHttpClient.class) {
				if (singleton == null) {
					singleton = new OkHttpClient();
					singleton.setConnectTimeout(12, TimeUnit.SECONDS);
					singleton.setReadTimeout(12, TimeUnit.SECONDS);
					singleton.setWriteTimeout(12, TimeUnit.SECONDS);
				}
			}
		}
		return singleton;
	}
}
