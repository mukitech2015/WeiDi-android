package com.junen.vtaxi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.junen.vtaxi.R;
import com.junen.vtaxi.model.MessageItem;
import com.junen.vtaxi.model.Record;

import java.util.ArrayList;

/**
 * Created by root on 16-12-16.
 */

public class RecordAdapter  extends BaseAdapter {
    private Context context;
    private LayoutInflater mInflater;
    private ArrayList<Record> recordItemArrayList;

    public RecordAdapter(Context context,ArrayList<Record> recordItemArrayList){
        super();
        this.context = context;
        this.recordItemArrayList=recordItemArrayList;
        mInflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return recordItemArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return recordItemArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
           ViewHolder holder = null;
        if(convertView==null){
            holder = new  ViewHolder();
            convertView = mInflater.inflate(R.layout.layout_recoed_item, null);
            holder.recordNumTV= (TextView) convertView.findViewById(R.id.record_numTv);
            holder.recordMoneyTv= (TextView) convertView.findViewById(R.id.record_moneyTv);
            holder.recordTimeTV= (TextView) convertView.findViewById(R.id.record_timeTv);
            holder.recordStateTV= (TextView) convertView.findViewById(R.id.record_stateTv);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        Record   record=recordItemArrayList.get(position);
        holder.recordNumTV.setText(position+1+":");
        holder.recordTimeTV.setText(record.applytime+"  至  "+record.overtime);
        holder.recordMoneyTv.setText(record.tx+"");
        int  state=record.state;
        if (state==1){
           holder.recordStateTV.setText("审核中");
        }else if(state==2){
           holder.recordStateTV.setText("完成结算");
        }else if(state==3){
            holder.recordStateTV.setText("审核不通过");
        }
        return convertView;
    }

    public class ViewHolder{
        public  TextView   recordNumTV;
        public  TextView   recordMoneyTv;
        public  TextView   recordTimeTV;
        public  TextView   recordStateTV;
    }
}
