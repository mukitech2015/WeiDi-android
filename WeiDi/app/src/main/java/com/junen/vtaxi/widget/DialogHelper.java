package com.junen.vtaxi.widget;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnKeyListener;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.junen.vtaxi.R;


/**
 * 对话框辅助类
 * 
 * @author menglin.li
 * 
 */
public class DialogHelper {
	private Activity mActivity;
	private AlertDialog mAlertDialog;
	private AlertDialog mProgressDialog;
	private Toast mToast;

	public DialogHelper(Activity activity) {
		mActivity = activity;
	}

	/**
	 * 弹对话框
	 * 
	 * @param title
	 *            标题
	 * @param msg
	 *            消息
	 * @param positive
	 *            确定
	 * @param positiveListener
	 *            确定回调
	 * @param negative
	 *            否定
	 * @param negativeListener
	 *            否定回调
	 */
	public void alert(final String title, final String msg,
			final String positive,
			final DialogInterface.OnClickListener positiveListener,
			final String negative,
			final DialogInterface.OnClickListener negativeListener) {
		alert(title, msg, positive, positiveListener, negative,
				negativeListener, false);
	}

	/**
	 * 弹对话框
	 * 
	 * @param title
	 *            标题
	 * @param msg
	 *            消息
	 * @param positive
	 *            确定
	 * @param positiveListener
	 *            确定回调
	 * @param negative
	 *            否定
	 * @param negativeListener
	 *            否定回调
	 * @param isCanceledOnTouchOutside
	 *            是否可以点击外围框
	 */
	public void alert(final String title, final String msg,
			final String positive,
			final DialogInterface.OnClickListener positiveListener,
			final String negative,
			final DialogInterface.OnClickListener negativeListener,
			final Boolean isCanceledOnTouchOutside) {
		dismissAlertDialog();

		mActivity.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (mActivity == null || mActivity.isFinishing()) {
					return;
				}
				try {

					AlertDialog.Builder builder = new AlertDialog.Builder(
							mActivity);
					if (title != null) {
						builder.setTitle(title);
					}
					if (msg != null) {
						builder.setMessage(msg);
					}
					if (positive != null) {
						builder.setPositiveButton(positive, positiveListener);
					}
					if (negative != null) {
						builder.setNegativeButton(negative, negativeListener);
					}
					mAlertDialog = builder.show();
					mAlertDialog
							.setCanceledOnTouchOutside(isCanceledOnTouchOutside);
					mAlertDialog.setCancelable(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * 自定义界面弹对话框
	 * 
	 * @param title
	 *            标题
	 * @param msg
	 *            消息
	 * @param positive
	 *            确定
	 * @param positiveListener
	 *            确定回调
	 * @param negative
	 *            否定
	 * @param negativeListener
	 *            否定回调
	 * @param isCanceledOnTouchOutside
	 *            是否可以点击外围框
	 */
	public void customAlert(final String title, final String msg,
			final String positive, final View.OnClickListener positiveListener,
			final String negative, final View.OnClickListener negativeListener,
			final Boolean isCanceledOnTouchOutside,
			final boolean isCanceledBackClick) {
		dismissAlertDialog();

		mActivity.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (mActivity == null || mActivity.isFinishing()) {
					return;
				}
				try {

					AlertDialog.Builder builder = new AlertDialog.Builder(
							mActivity);
					mAlertDialog = builder.show();
					Window window = mAlertDialog.getWindow();
					window.setContentView(R.layout.confirm_dialog);
					TextView content = (TextView) window.findViewById(R.id.msg);
					TextView negativeView = (TextView) window
							.findViewById(R.id.cancel);
					TextView positiveView = (TextView) window
							.findViewById(R.id.confirm);
					//View line = window.findViewById(R.id.dialog_btn_line);
					if (!TextUtils.isEmpty(msg)) {
						content.setText(msg);
					}
					if (!TextUtils.isEmpty(positive)) {
						positiveView.setText(positive);
					}
					if (!TextUtils.isEmpty(negative)) {
						negativeView.setText(negative);
						negativeView.setVisibility(View.VISIBLE);
						/*
						line.setVisibility(View.VISIBLE);
						positiveView
								.setBackgroundResource(R.drawable.dwd_dialog_right_fillet_frame);
								*/
					} else {
						negativeView.setVisibility(View.GONE);
						/*
						line.setVisibility(View.GONE);
						positiveView
								.setBackgroundResource(R.drawable.dwd_dialog_bottom_fillet_frame);
								*/
					}
					if (positiveListener != null) {
						positiveView.setOnClickListener(positiveListener);
					}
					if (negativeListener != null) {
						negativeView.setOnClickListener(negativeListener);
					}
					mAlertDialog
							.setCanceledOnTouchOutside(isCanceledOnTouchOutside);
					if (!isCanceledBackClick) {
						mAlertDialog.setOnKeyListener(new OnKeyListener() {
							@Override
							public boolean onKey(DialogInterface dialog,
									int keyCode, KeyEvent event) {
								if (keyCode == KeyEvent.KEYCODE_BACK) { // 监控/拦截/屏蔽返回键
									return true;
								}
								return false;
							}
						});
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * TOAST
	 * 
	 * @param msg
	 *            消息
	 * @param period
	 *            时长
	 */
	public void toast(final String msg, final int period) {
		mActivity.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (TextUtils.isEmpty(msg)) {
					return;
				}
				mToast = new Toast(mActivity);
				View view = LayoutInflater.from(mActivity).inflate(
						R.layout.transient_notification, null);
				TextView tv = (TextView) view
						.findViewById(android.R.id.message);
				tv.setText(msg);
				mToast.setView(view);
				mToast.setDuration(period);

				mToast.setGravity(Gravity.BOTTOM, 0, 200);
				mToast.show();
			}
		});
	}

	/**
	 * 显示进度对话框
	 * 
	 * @param showProgressBar
	 *            是否显示圈圈
	 * @param msg
	 *            对话框信息
	 */
	public void showProgressDialog(boolean showProgressBar, String msg) {
		showProgressDialog(msg, true, null, showProgressBar);
	}

	/**
	 * 显示进度对话框
	 * 
	 * @param msg
	 *            消息
	 */
	public void showProgressDialog(final String msg) {
		showProgressDialog(msg, true, null, true);
	}

	/**
	 * 显示可取消的进度对话框
	 * 
	 * @param msg
	 *            消息
	 */
	public void showProgressDialog(final String msg, final boolean cancelable,
			final OnCancelListener cancelListener, final boolean showProgressBar) {
		dismissProgressDialog();

		mActivity.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (mActivity == null || mActivity.isFinishing()) {
					return;
				}
				try {
					mProgressDialog = new GenericProgressDialog(mActivity);
					mProgressDialog.setMessage(msg);
					((GenericProgressDialog) mProgressDialog)
							.setProgressVisiable(showProgressBar);
					mProgressDialog.setCancelable(cancelable);
					mProgressDialog.setOnCancelListener(cancelListener);

					mProgressDialog.show();

					mProgressDialog.setCanceledOnTouchOutside(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	public void dismissProgressDialog() {
		mActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				try {
					if (mProgressDialog != null && mProgressDialog.isShowing()
							&& !mActivity.isFinishing()) {
						mProgressDialog.dismiss();
						mProgressDialog = null;
					}
				} catch (Exception e) {
					mProgressDialog = null;
				}

			}
		});
	}

	public void dismissAlertDialog() {
		mActivity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				try {
					if (mAlertDialog != null && mAlertDialog.isShowing()
							&& !mActivity.isFinishing()) {
						mAlertDialog.dismiss();
						mAlertDialog = null;
					}
				} catch (Exception e) {
					mAlertDialog = null;
				}

			}
		});
	}
}
