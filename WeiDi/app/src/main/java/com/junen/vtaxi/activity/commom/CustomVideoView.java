package com.junen.vtaxi.activity.commom;

import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.VideoView;

/**
 * Created by muki on 2016/11/22.
 */

public class CustomVideoView extends VideoView {

    private final String TAG="MainActivity";

    public int videoWidth = 1155;
    public int videoHeight = 950;

    public CustomVideoView(Context context) {
        super(context);
    }
    public CustomVideoView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    public CustomVideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public static int getScreenWidth(Context context)
    {
        WindowManager wm = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(outMetrics);
        return outMetrics.widthPixels;
    }

    /**
     * 获得屏幕宽度
     *
     * @param context
     * @return
     */
    public static int getScreenHeight(Context context)
    {
        WindowManager wm = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics outMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(outMetrics);
        return outMetrics.heightPixels;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        boolean switchOn = false;

        if( switchOn){
            return;
        }

            int targetW = 1155;
            int targetH = 720;

            int containerHeight = 1155;
            int containerWidth = 950;

            float ratioW = (float) videoWidth / (float) containerWidth;
            float ratioH = (float) videoHeight / (float) containerHeight;

            if (ratioW > ratioH) {
                targetW = (int)((float)containerHeight / videoHeight) * videoWidth ;
                targetH = containerHeight;
            } else {
                targetW = containerWidth;
                targetH = (int)((float)containerWidth / videoWidth) * videoHeight;
            }

            if( targetW == 0 || targetH == 0 ) {
                return;
            }else {
                setMeasuredDimension(targetW, targetH);
            }


    }

}
