package com.junen.vtaxi.activity.base;

import android.annotation.SuppressLint;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;

import com.junen.vtaxi.app.BaseApplication;

/**
 * Fragment封装类
 * 
 * 封装生命周期监控，对话框等
 * 
 * @author menglin.li
 * 
 */
@SuppressLint("NewApi")
public abstract class BaseFragmentActivity extends FragmentActivity implements
		ActivityResponsable {
	/**
	 * 所属APP
	 */
	protected BaseApplication mApp;
	/**
	 * Activity辅助类
	 */
	private ActivityHelper mActivityHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mActivityHelper = new ActivityHelper(this);

		String deviceModel = android.os.Build.DEVICE;
		if (TextUtils.equals(deviceModel, "M040")) {
			if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
				// 当前view去掉硬件加速 防止mx crash
				getWindow().getDecorView().setLayerType(
						View.LAYER_TYPE_SOFTWARE, null);
			}
		}

	}

	@Override
	public void finish() {
		if (mActivityHelper != null) {
			mActivityHelper.finish();
		}
		super.finish();
	}

	/**
	 * 显示进度对话框
	 * 
	 * @param msg
	 *            消息
	 */
	@Override
	public void showProgressDialog(String msg) {
		mActivityHelper.showProgressDialog(msg);
	}

	/**
	 * 显示可取消的进度对话框
	 * 
	 * @param msg
	 *            消息
	 */
	public void showProgressDialog(final String msg, final boolean cancelable,
			final OnCancelListener cancelListener) {
		mActivityHelper.showProgressDialog(msg, cancelable, cancelListener);
	}

	@Override
	public void alert(String title, String msg, String positive,
			OnClickListener positiveListener, String negative,
			OnClickListener negativeListener) {
		mActivityHelper.alert(title, msg, positive, positiveListener, negative,
				negativeListener);
	}

	@Override
	public void alert(String title, String msg, String positive,
			OnClickListener positiveListener, String negative,
			OnClickListener negativeListener, Boolean isCanceledOnTouchOutside) {
		mActivityHelper.alert(title, msg, positive, positiveListener, negative,
				negativeListener, isCanceledOnTouchOutside);
	}

	@Override
	public void customAlert(String title, String msg, String positive,
			View.OnClickListener positiveListener, String negative,
			View.OnClickListener negativeListener, Boolean isCanceledBackClick) {
		mActivityHelper.customAlert(title, msg, positive, positiveListener,
				negative, negativeListener, false, isCanceledBackClick);
	}

	@Override
	public void customAlert(String msg, String positive,
			View.OnClickListener positiveListener, String negative,
			View.OnClickListener negativeListener, Boolean isCanceledBackClick) {
		mActivityHelper.customAlert(null, msg, positive, positiveListener,
				negative, negativeListener, false, isCanceledBackClick);
	}

	@Override
	public void toast(String msg, int period) {
		mActivityHelper.toast(msg, period);
	}

	@Override
	public void dismissProgressDialog() {
		mActivityHelper.dismissProgressDialog();
	}
	
	@Override
	public void dismissAlertDialog() {
		mActivityHelper.dismissAlertDialog();
	}

	@Override
	protected void onDestroy() {
		dismissProgressDialog();
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		return super.dispatchTouchEvent(event);
	}
}
