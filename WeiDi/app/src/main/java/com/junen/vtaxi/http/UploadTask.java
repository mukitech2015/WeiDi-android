package com.junen.vtaxi.http;

import java.io.File;
import java.util.Map;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;
import com.junen.vtaxi.activity.base.ActivityResponsable;
import com.junen.vtaxi.rpc.RpcException.ErrorCode;
import com.junen.vtaxi.util.JsonUtils;
import com.junen.vtaxi.util.UploadUtils;

public class UploadTask extends AsyncTask<Object, Integer, Boolean> {
	private ActivityResponsable activityResponseable;
	private Handler mHandler;
	private int mType;

	public UploadTask(Activity context, Handler handler, int type) {
		if (context != null && context instanceof ActivityResponsable) {
			this.activityResponseable = (ActivityResponsable) context;
		}
		this.mHandler = handler;
		mType = type;

	}

	@SuppressWarnings("unchecked")
	@Override
	protected Boolean doInBackground(Object... params) {
		boolean isSuccess = false;
		try {
			String result = UploadUtils.uploadFile(
					(Map<String, String>) params[0],
					(Map<String, File>) params[1], mHandler);

			JSONObject jsonObject = JsonUtils.parseObject(result);
			if (jsonObject != null) {
				int resultStatus = jsonObject.getIntValue("status");
				if (resultStatus == ErrorCode.OK) {
					isSuccess = true;
				}
			}
		} catch (Exception e) {
		}
		return isSuccess;
	}

	@Override
	protected void onPostExecute(Boolean result) {
		if (mType != 5) {
			if (activityResponseable != null) {
				activityResponseable.dismissProgressDialog();
			}
			if (result) {
				activityResponseable.toast("上传成功", Toast.LENGTH_SHORT);
			}
		}

		Message message = Message.obtain();
		message.arg1 = 101;
		message.obj = result;
		message.arg2 = mType;
		mHandler.sendMessage(message);
	}

	@Override
	protected void onPreExecute() {
		if (activityResponseable != null && mType != 5) {
			activityResponseable.showProgressDialog("上传中...");
		}
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		// Message msg=Message.obtain();
		// msg.arg1=100;
		// msg.arg2=values[0];
		// mHandler.sendMessageDelayed(msg, 0);

	}

}
