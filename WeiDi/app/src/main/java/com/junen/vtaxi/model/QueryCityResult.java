package com.junen.vtaxi.model;

/**
 * 根据经纬度查询城市
 */
public class QueryCityResult  {
	/** 城市ID */
	public String cityId;
	/** 城市名 */
	public String cityName;
	/** URL */
	public String server;
}