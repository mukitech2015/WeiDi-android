package com.junen.vtaxi.activity.account;

import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.junen.vtaxi.R;
import com.junen.vtaxi.activity.base.BaseActivity;
import com.junen.vtaxi.app.BaseApplication;

import java.util.ArrayList;

public class SelectPrinterActivity extends BaseActivity {
	private ListView listView;
	DeviceAdapter adapter;
	ArrayList<BluetoothDevice> blueDeviceArray = new ArrayList<BluetoothDevice>();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select_printer);
		initView();
		initData();
	}

	@Override
	public void initView() {
		super.initView();
		listView = (ListView)findViewById(R.id.list);
		Button backView = (Button)findViewById(R.id.close_view);
		backView.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				BluetoothDevice device = blueDeviceArray.get(arg2);
				BaseApplication.getInstance().connect(device.getAddress());
				finish();
			}
		});
	}

	@Override
	public void initData() {
		super.initData();
		blueDeviceArray = BaseApplication.getInstance().getDevices();
		adapter = new DeviceAdapter(blueDeviceArray);
		listView.setAdapter(adapter);
	}
	class DeviceAdapter extends BaseAdapter{
		private LayoutInflater mInflater;
		private ArrayList<BluetoothDevice> deviceArray;
		private DeviceAdapter(ArrayList<BluetoothDevice> deviceArray) {
			super();
			this.deviceArray = deviceArray;
			mInflater = LayoutInflater.from(SelectPrinterActivity.this);
			if(deviceArray==null){
				deviceArray = new ArrayList<BluetoothDevice>();
			}
		}

		@Override
		public int getCount() {
			return deviceArray.size();
		}

		@Override
		public Object getItem(int position) {
			return deviceArray.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder = null;
			if(convertView==null){
				holder = new ViewHolder();
				convertView = mInflater.inflate(R.layout.list_printer_item, null);
				holder.name = (TextView)convertView.findViewById(R.id.name_view);
				holder.mac = (TextView)convertView.findViewById(R.id.mac_view);
				convertView.setTag(holder);
			}else{
				holder = (ViewHolder) convertView.getTag();
			}
			BluetoothDevice device = deviceArray.get(position);
			if(device!=null){
				holder.name.setText(device.getName());
				holder.mac.setText(device.getAddress());
			}
			return convertView;
		}
		public class ViewHolder{
			public TextView name;
			public TextView mac;
		}
	}
}
