package com.junen.vtaxi.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {

	/**
	 * 获取当前日期
	 * @param format日期格式
	 * @return
	 */
	public static String getNowDate(String format) {
		SimpleDateFormat mFormatter = new SimpleDateFormat(format,Locale.getDefault());
		Date curDate = new Date(System.currentTimeMillis());// 获取当前时间

		return mFormatter.format(curDate);
	}
	
	/**
	 * 获取当前星期几
	 * @return
	 */
	public static String getNowWeek(){
		Date curDate = new Date(System.currentTimeMillis());// 获取当前时间
		Calendar c = Calendar.getInstance();
		c.setTime(curDate);
		int week = c.get(Calendar.DAY_OF_WEEK);
		String mWeek = null;
		switch(week){
		case 1:
			mWeek = "周日";
			break;
		case 2:
			mWeek = "周一";
			break;
		case 3:
			mWeek = "周二";
			break;
		case 4:
			mWeek = "周三";
			break;
		case 5:
			mWeek = "周四";
			break;
		case 6:
			mWeek = "周五";
			break;
		case 7:
			mWeek = "周六";
			break;
		}
		return mWeek;
	}
	
	/**
	 * 
	 * @param dateString
	 * @param formatString
	 * @return
	 */
	public static Date stringToDate(String dateString,String formatString){
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(formatString,Locale.getDefault());
		try {
			date = sdf.parse(dateString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return date;
	}
	
	/**
	 * Date转到字符串
	 * @param date
	 * @param formatString
	 * @return
	 */
	public static String dateToString(Date date,String formatString){
		String dateString;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat(formatString,Locale.getDefault());
			dateString = sdf.format(date);
		} catch (Exception e) {
			dateString = "";
			e.printStackTrace();
		}
		
		return dateString;
	}
	
	public static String stringToFormat(String date,String oldFormat,String newFormat){
		Date mDate = stringToDate(date, oldFormat);
		
		return dateToString(mDate,newFormat);
	}
	
	/**
	 * 比较日期
	 * @param DATE1
	 * @param DATE2
	 * @return 1-在前，-1在后，0-相等
	 */
	public static int compareDateY(String DATE1, String DATE2,String format) {
		DateFormat df = new SimpleDateFormat(format,Locale.getDefault());
		try {
			Date dt1 = df.parse(DATE1);
			Date dt2 = df.parse(DATE2);
			if (dt1.getTime() > dt2.getTime()) {
				System.out.println("dt1 在dt2前");
				return 1;
			} else if (dt1.getTime() < dt2.getTime()) {
				System.out.println("dt1在dt2后");
				return -1;
			} else {
				return 0;
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return 0;
	}
    /**
     * 将yyyy-MM-dd hh:mm:ss的时间转换成对象
     *
     * @param date
     * @return
     */
    public static FormatDate getFormatDate(String date) {
        FormatDate formatDate = new FormatDate();
        String[] splitData = date.split("\\ ");
        if (splitData.length > 1) {
            String[] date1 = splitData[0].split("\\-");//年月日
            if (date1.length == 3) {
                formatDate.YEAR = date1[0];
                formatDate.MONTH = date1[1];
                formatDate.DAY = date1[2];
            }

            String[] date2 = splitData[1].split("\\:");
            if (date2.length == 3) {
                formatDate.HOUR = date2[0];
                formatDate.MINUTE = date2[1];
                formatDate.SECOND = date2[2];
            } else if (date2.length == 2) {
                formatDate.HOUR = date2[0];
                formatDate.MINUTE = date2[1];
            }
        }
        return formatDate;
    }

    public static class FormatDate {
        public String YEAR = "";
        public String MONTH = "";
        public String DAY = "";
        public String HOUR = "";
        public String MINUTE = "";
        public String SECOND = "";
    }

	/**
	 * 获取日期字符串(精确到天)
	 *
	 * @return
	 */
	public static String getCurrentDayString() {
		try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd",Locale.getDefault());
			return simpleDateFormat.format(new Date(System.currentTimeMillis()));
		} catch (Exception e) {
			return "";
		}
	}

	/**
	 * 讲YYYY年MM月DD日格式的数据转换成YYYY-MM-DD格式
	 * @param dateStr
	 * @return
	 */
	public static String convertDate(String dateStr) {
		String year = "";
		String month = "";
		String day = "";
		try {
			if (dateStr.contains("年")) {
				String[] yearSplit = dateStr.split("年");
				if (yearSplit != null && yearSplit.length == 2) {
					year = yearSplit[0];//年
					if (yearSplit[1] != null && yearSplit[1].contains("月")) {
						String[] monthSplt = yearSplit[1].split("月");
						if (month != null && monthSplt.length == 2) {
							month = monthSplt[0];//月
							if (monthSplt[1].contains("日")) {
								day = monthSplt[1].split("日")[0];
							}
						}
					}
				}
			}
		} catch (Exception e) {
			return "";
		}

		return year + "-" + month + "-" + day;
	}
}

