package com.junen.vtaxi.activity.account;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.junen.vtaxi.R;
import com.junen.vtaxi.activity.base.BaseActivity;
import com.junen.vtaxi.activity.commom.WebViewActivity;
import com.junen.vtaxi.adapter.MessageAdapter;
import com.junen.vtaxi.app.BaseApplication;
import com.junen.vtaxi.common.Constant;
import com.junen.vtaxi.http.ApiClient;
import com.junen.vtaxi.model.MessageItem;
import com.junen.vtaxi.model.MessageRusult;
import com.junen.vtaxi.rpc.RpcExcutor;
import com.junen.vtaxi.rpc.api.RpcApi;
import com.junen.vtaxi.widget.XListView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by new on 16/7/8.
 */
public class MessageActivity extends BaseActivity {
    private XListView listView;
    private TextView emptyView;
    private ArrayList<MessageItem> messageItemArray = new ArrayList<MessageItem>();
    private MessageAdapter messageAdapter;

    private boolean canClick = true;
    private RpcExcutor<MessageRusult> messageExcutor;
    private int page = 1;
    private int tempPage = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        initView();
        initData();
    }
    @Override
    public void initView() {
        super.initView();
        Button backView = (Button) findViewById(R.id.close_view);
        backView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                back();
            }
        });

        listView = (XListView) findViewById(R.id.list_view);
        emptyView = (TextView) findViewById(R.id.empty_view);

        listView.setEmptyView(emptyView);
        listView.setVisibility(View.VISIBLE);
        listView.setPullRefreshEnable(true);
        listView.setPullLoadEnable(true);
        listView.setXListViewListener(listViewListener);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                int pos = position - 1;
                if (pos < 0) {
                    return;
                }
                MessageItem messageItem = messageItemArray.get(pos);
                if (messageItem == null || TextUtils.isEmpty(messageItem.url) || !canClick) {
                    return;
                }
                canClick = false;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        canClick = true;
                    }
                }, 3000);
                Intent intent = new Intent(MessageActivity.this, WebViewActivity.class);
                intent.putExtra(Constant.WEBVIEW_TITLE_KEY, messageItem.title);
                intent.putExtra(Constant.WEBVIEW_URL_KEY, messageItem.url);
                intent.putExtra(Constant.MESSAGE_ID_KEY, messageItem.id);
                startActivityForResult(intent, Constant.READ_MESSAGE_DETAIL);
            }
        });
    }

    @Override
    public void initData() {
        super.initData();
        initRpcExcutor();
        //performData();
        messageItemArray = new ArrayList<MessageItem>();
        messageAdapter = new MessageAdapter(MessageActivity.this, messageItemArray);
        listView.setAdapter(messageAdapter);

        tempPage = 0;
        page = 0;
        messageExcutor.start();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==Constant.READ_MESSAGE_DETAIL){
            String id = BaseApplication.readMessageId;
            if(messageItemArray!=null && messageItemArray.size()>0){
                int size = messageItemArray.size();
                for(int i=0;i<size;i++){
                    MessageItem messageItem = messageItemArray.get(i);
                    if(TextUtils.equals(messageItem.id,id)){
                        messageItemArray.get(i).state = 1;
                        break;
                    }
                }
                messageAdapter.notifyDataSetChanged();
            }
        }
    }

    private void onLoad() {
        listView.stopRefresh();
        listView.stopLoadMore();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date curDate = new Date(System.currentTimeMillis());// 获取当前时间
        String str = formatter.format(curDate);
        listView.setRefreshTime(str);
    }

    /**
     * ListView上拉下拉监听
     */
    XListView.IXListViewListener listViewListener = new XListView.IXListViewListener() {

        @Override
        public void onRefresh() {
            tempPage = 1;
            page = 1;
            messageExcutor.start();
        }

        @Override
        public void onLoadMore() {
            tempPage = page + 1;
            messageExcutor.start();
        }

    };


    protected void initRpcExcutor() {
        messageExcutor = new RpcExcutor<MessageRusult>(this, 0, false) {
            @Override
            public void excute(Object... params) {
                ApiClient.createApi(MessageActivity.this, RpcApi.class).getMessageList(11, BaseApplication
                        .getInstance().getDriverId(), tempPage, this);
            }

            @Override
            public void onRpcFinish(MessageRusult result, Object... params) {
                doResult(result);
            }

            @Override
            public void onRpcException(int code, String msg, Object... params) {
                toast(msg, Toast.LENGTH_SHORT);
            }

        };
        messageExcutor.setShowProgressDialog(false);
        messageExcutor.setShowNetworkErrorView(false);
    }

    private void doResult(MessageRusult result) {
        if (result == null) {
            onLoad();
            return;
        }
        page = tempPage;
        if(messageItemArray==null){
            messageItemArray = new ArrayList<MessageItem>();
        }
        if(1==page){
            messageItemArray.clear();
        }
        ArrayList<MessageItem> messageArray = (ArrayList<MessageItem>)result.list;
        if(messageArray!=null){
            int size = messageArray.size();
            for(int i=0;i<size;i++){
                messageItemArray.add(messageArray.get(i));
            }
        }
        if (messageAdapter == null) {
            messageAdapter = new MessageAdapter(MessageActivity.this, messageItemArray);
            listView.setAdapter(messageAdapter);
        } else {
            messageAdapter.notifyDataSetChanged();
        }
        onLoad();
        if(messageArray==null || messageArray.size()<10){
            //不可加载更多
            listView.setPullLoadEnable(false);
        }else{
            listView.setPullLoadEnable(true);
        }
    }

    public void performData() {
        messageItemArray = new ArrayList<MessageItem>();
        for (int i = 0; i < 10; i++) {
            MessageItem messageItem = new MessageItem();
            messageItem.state = 1;
            messageItem.title = "计费规则更新";
            messageItem.time = "06-16 22:10";
            messageItem.url = "http://www.baidu.com";
            messageItemArray.add(messageItem);
        }
    }

}
