package com.junen.vtaxi.common;

/**
 * 提示信息字符集
 * 
 * @author menglin.li
 * 
 */
public class Strings {
	/** 系统错误 */
	public static final String NETWORK_UNKNOWN_ERROR_MSG = "系统错误，请重试!";
	/** 网络异常 */
	public static final String NETWORK_ERROR_MSG = "网络异常，请检测网络后重试!";
	/** 网络不可用 */
	public static final String NETWORK_UNAVAILABLE_MSG = "网络不可用";
	/** 网络连接超时 */
	public static final String NETWORK_CONNECTION_EXCEPTION_MSG = "网络连接超时";
	/** 网络速度过慢 */
	public static final String NETWORK_SOCKET_EXCEPTION_MSG = "网络速度过慢";
	/** 网络IO异常 */
	public static final String NETWORK_IO_EXCEPTION_MSG = "网络IO异常";
	/** 客户端网络请求调度错误 */
	public static final String NETWORK_SCHEDULE_ERROR_MSG = "客户端网络请求调度错误";
	/** 数据异常 */
	public static final String DADA_ERROR_MSG = "数据异常，请重试!";
}
