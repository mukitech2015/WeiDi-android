package com.junen.vtaxi.http;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import retrofit.client.OkClient;
import retrofit.client.Request;
import retrofit.client.Response;
import retrofit.mime.MimeUtil;
import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedOutput;
import android.text.TextUtils;

import com.junen.vtaxi.app.BaseApplication;
import com.junen.vtaxi.common.Constant;
import com.junen.vtaxi.util.EncryptUtils;
import com.squareup.okhttp.OkHttpClient;

public class EncryptClient extends OkClient {

	public EncryptClient(OkHttpClient client) {
		super(client);
	}

	@Override
	public Response execute(Request request) throws IOException {
		Request newRequest = encrypt(request);
		return super.execute(newRequest);
	}

	private Request encrypt(Request request) {
		String method = request.getMethod();
		String url = request.getUrl();
		if (url.contains(Constant.UPLOAD_POSITION_TAG)) {
			return request;
		}
		if (TextUtils.equals("GET", method)) {
			String newParams = EncryptUtils.sign(
					splicingParams(getQueryParams(url)), getToken());
			StringBuffer urlBuffer = new StringBuffer(getUrlTag(url));
			urlBuffer.append("?").append(newParams);
			return new Request(request.getMethod(), urlBuffer.toString(),
					request.getHeaders(), request.getBody());
		} else {
			String requestType = request.getBody().mimeType();
			String params = EncryptUtils.sign(
					splicingParams(getBodyAsString(request)), getToken());
			TypedOutput body = null;
			body = new TypedByteArray(requestType, params.getBytes());
			Request newRequest = new Request(request.getMethod(), url,
					request.getHeaders(), body);
			return newRequest;
		}
	}

	@SuppressWarnings("deprecation")
	public String getBodyAsString(Request request) {
		String bodyParams = "";
		TypedOutput body = request.getBody();
		if (body != null) {
			try {
				if (!(body instanceof TypedByteArray)) {
					String bodyMime = body.mimeType();
					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					body.writeTo(baos);
					body = new TypedByteArray(bodyMime, baos.toByteArray());
				}
				bodyParams = new String(((TypedByteArray) body).getBytes(),
						MimeUtil.parseCharset(body.mimeType()));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return bodyParams;
	}

	public String getQueryParams(String url) {
		String params = "";
		if (!TextUtils.isEmpty(url) && url.contains("?")) {
			params = url.split("\\?")[1];
		}
		return params;
	}

	public String getUrlTag(String url) {
		String tag = "";
		if (!TextUtils.isEmpty(url) && url.contains("?")) {
			tag = url.split("\\?")[0];
		}
		return tag;
	}

	private String splicingParams(String params) {
		StringBuffer newParams = new StringBuffer(params);
		newParams.append("&token=").append(getToken());
		return newParams.toString();

	}

	public String getToken() {
		//return BaseApplication.getInstance().getToken();
		return null;
	}

}
