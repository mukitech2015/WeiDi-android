package com.junen.vtaxi.activity.commom;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationClientOption.AMapLocationMode;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.maps.AMapUtils;
import com.amap.api.maps.model.LatLng;
import com.junen.vtaxi.R;
import com.junen.vtaxi.activity.account.MessageActivity;
import com.junen.vtaxi.activity.account.SelectPrinterActivity;
import com.junen.vtaxi.activity.account.UserInfoActivity;
import com.junen.vtaxi.activity.account.ValuationRulesActivity;
import com.junen.vtaxi.activity.account.VideoCenterActivity;
import com.junen.vtaxi.activity.base.BaseActivity;
import com.junen.vtaxi.app.BaseApplication;
import com.junen.vtaxi.common.Constant;
import com.junen.vtaxi.common.ICallBack;
import com.junen.vtaxi.engine.AccountEngine;
import com.junen.vtaxi.http.ApiClient;
import com.junen.vtaxi.model.AdvertResult;
import com.junen.vtaxi.model.AudioFile;
import com.junen.vtaxi.model.BannerResult;
import com.junen.vtaxi.model.CreateOrderResult;
import com.junen.vtaxi.model.GpsResult;
import com.junen.vtaxi.model.HolidayData;
import com.junen.vtaxi.model.HolidayResult;
import com.junen.vtaxi.model.PayOrder;
import com.junen.vtaxi.model.PrintEvent;
import com.junen.vtaxi.model.SpeedWarnResult;
import com.junen.vtaxi.model.StopOrderResult;
import com.junen.vtaxi.model.ValuationResult;
import com.junen.vtaxi.model.VideoInfo;
import com.junen.vtaxi.model.VoiceResult;
import com.junen.vtaxi.rpc.RpcExcutor;
import com.junen.vtaxi.rpc.api.RpcApi;
import com.junen.vtaxi.service.DownloadService;
import com.junen.vtaxi.service.GpsService;
import com.junen.vtaxi.service.MusicalPlayService;
import com.junen.vtaxi.service.MusicalPlayerINterface;
import com.junen.vtaxi.service.RemoveFileService;
import com.junen.vtaxi.util.CHZZQRCodeUtil;
import com.junen.vtaxi.util.QRCodeEncoder;
import com.junen.vtaxi.util.SdUtil;
import com.junen.vtaxi.util.Utils;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import de.greenrobot.event.EventBus;

public class LauncherActivity extends BaseActivity implements OnClickListener, AMapLocationListener {
    private Button startView, stopView, messageView;
    private TextView messageNumView;
    private static final long WAIT_TIME = 1000L;
    private static final long REQ_GPS_1_TIME = 60 * 1000L;
    private static final long REQ_GPS_2_TIME = 20 * 1000L;
    private LinearLayout userView;
    private TextView bannerView;
    private LinearLayout mileageLayout;
    private TextView counterTypeView, priceView, unitPriceView, mileageView, waitTimeView, usedTimeView;
    private float lastSpeed = 0L;
    CustomVideoView videoView;
    MediaController mediaController;
    private ArrayList<File> videoArray;
    private int currentIndex = 0;
    private LinearLayout videoOperationView;
    private Button soundView, nextVideoView;
    // 播放点
    private int videoPosition = 0;
    PopupWindow menuPopup;
    private TextView userInfoView, valuationRulesView, videoCenterView, switchCounterView,logoutView,userMessageView;
    private LinearLayout confirmLayout;
    private TextView confirmOkView, confirmCancelView;
    // 广告下载
    private ArrayList<VideoInfo> videoInfoArray;

    private AudioFile   startaudioFile;
   private  AudioFile  endaudioFile;
    // 图片下载
    private ImageLoader imageLoader;
    private DisplayImageOptions options;

    /** 退出 */
    private RpcExcutor<String> logoutExcutor;
    /** 计价规则 */
    private RpcExcutor<ValuationResult> valuationExcutor;
    /** 上报GPS */
    private RpcExcutor<GpsResult> gpsExcutor;
    /** 字幕 */
    private RpcExcutor<BannerResult> bannerExcutor;
    /** 视频 */
    private RpcExcutor<AdvertResult> advertExcutor;
    /**语音*/
    private  RpcExcutor<VoiceResult> voiceStartExcutor;

    private  RpcExcutor<VoiceResult>  voiceEndExcutor;

    /** 创建行程 */
    private RpcExcutor<CreateOrderResult> createOrderExcutor;
    /** 上报行程 */
    private RpcExcutor<StopOrderResult> stopOrderExcutor;
    /** 节假日 */
    private RpcExcutor<HolidayResult> holidayExcutor;
    /** 获取超速信息*/
    private RpcExcutor<SpeedWarnResult>   speedExcutor;
    /**
     *  上报超速
     */
    private  RpcExcutor<String>    stringSpeedRpcExcutor;

    // 底部确认菜单
    private int menuType = -1;// 0-开始，1-停止，2-打印
    //节假日信息
    HolidayData holidaydata;
    private PlayerConn conn=new PlayerConn();
    private MusicalPlayerINterface binder;
    MediaPlayer  videomediaPlayer;
    private  double speedWarnValue;
    private boolean  isSpeendWarm=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_launcher);
        initView();
        initData();
    }
    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }
    @Override
    public void initView() {
        super.initView();
        userView = (LinearLayout) findViewById(R.id.user_layout_view);
        bannerView = (TextView) findViewById(R.id.advertisement_view);
        counterTypeView = (TextView) findViewById(R.id.valuation_method_view);
        mileageLayout = (LinearLayout) findViewById(R.id.mileage_layout_view);
        priceView = (TextView) findViewById(R.id.price_view);
        unitPriceView = (TextView) findViewById(R.id.unit_price_show_view);
        mileageView = (TextView) findViewById(R.id.mileage_show_view);
        waitTimeView = (TextView) findViewById(R.id.wait_time_show_view);
        usedTimeView = (TextView) findViewById(R.id.used_time_show_view);
        unitPriceView.setText(String.valueOf(AccountEngine.getEveryMilesPrice()));
        messageView = (Button) findViewById(R.id.message_view);
        startView = (Button) findViewById(R.id.start_view);
        stopView = (Button) findViewById(R.id.stop_view);
        messageNumView = (TextView) findViewById(R.id.message_num_view);
        confirmLayout = (LinearLayout) findViewById(R.id.confirm_layout_view);
        confirmOkView = (TextView) findViewById(R.id.confirm_ok_view);
        confirmCancelView = (TextView) findViewById(R.id.confirm_cancel_view);
        messageView.setOnClickListener(this);
        messageNumView.setOnClickListener(this);
        startView.setOnClickListener(this);
        stopView.setOnClickListener(this);
        userView.setOnClickListener(this);
        confirmOkView.setOnClickListener(this);
        confirmCancelView.setOnClickListener(this);
        //语音
        Intent intent=new Intent();
        intent.setClass(this, MusicalPlayService.class);
        startService(intent);
        bindService(intent, conn, BIND_AUTO_CREATE);
        // 视频
        initVideoView();
        initMediaPlayer();
        initStartVoiceData();
        initEndVoiceData();
    }
    @Override
    public void initData() {
        super.initData();
        EventBus.getDefault().register(this);
        BaseApplication.getInstance().initData();
        initImageLoader();
        initRpcExcutor();
        initLocation(0);
        startLocation();
        startGpsTimer(REQ_GPS_1_TIME);
        advertExcutor.start();
        voiceStartExcutor.start();
        voiceEndExcutor.start();
        speedExcutor.start();
        updateView();
        updateBanner();
        initCounterView();
    }
    /**
     * 更新顶部View数据
     */
    private void updateView() {
        SharedPreferences shared = BaseApplication.getInstance().getSharedPreferences(Constant.LOGIN_RESULT_KEY,
				Context.MODE_PRIVATE);
        ImageView headView = (ImageView) findViewById(R.id.user_image_view);
        TextView nameView = (TextView) findViewById(R.id.user_name_view);
        TextView areaView = (TextView) findViewById(R.id.address_view);
        String headUrl = shared.getString(Constant.USER_INFO_LOGOURL_KEY, null);
        imageLoader.displayImage(headUrl, headView, options);
        nameView.setText(shared.getString(Constant.USER_INFO_NAME_KEY, ""));
        if (1 == shared.getInt(Constant.USER_INFO_SEX_KEY, 0)) {
            nameView.append(" 的姐");
        } else {
            nameView.append(" 的哥");
        }
        areaView.setText(shared.getString(Constant.USER_INFO_GNAME_KEY, ""));
    }
    /**
     * 更新Banner文字
     */
    private void updateBanner() {
        String bannerText = AccountEngine.getBannerText();
        if (!TextUtils.isEmpty(bannerText)) {
            String s = bannerText + getString(R.string.banner_space) + bannerText + getString(R.string.banner_space)
					+ bannerText;
            bannerView.setText(s);
        }
    }
    /**
     * 初始化Video相关控件
     */
    private void initVideoView() {
        videoView = (CustomVideoView) findViewById(R.id.video_view);
        videoOperationView = (LinearLayout) findViewById(R.id.video_operation_view);
        soundView = (Button) findViewById(R.id.sound_video_view);
        nextVideoView = (Button) findViewById(R.id.next_video_view);
        initVideoData();
        initVideoSound();
        mediaController = new MediaController(this);
        mediaController.setVisibility(View.GONE);
        videoView.setKeepScreenOn(true);
        videoView.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (View.GONE == videoOperationView.getVisibility()) {
                    videoOperationView.setVisibility(View.VISIBLE);
                } else {
                    videoOperationView.setVisibility(View.GONE);
                }
                return false;
            }
        });

        videoView.setOnCompletionListener(new OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                videomediaPlayer=mp;
                if (videoArray != null && videoArray.size() > 0) {
                    currentIndex++;
                    if (videoArray.size() <= currentIndex) {
                        currentIndex = 0;
                    }
                    videoCurrent();
                }
            }
        });
        videoView.setOnPreparedListener(new OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                Log.e(" videoView", videoView.getWidth()+":"+videoView.getHeight());
                videomediaPlayer=mp;
                videoView.videoHeight = mp.getVideoHeight();
                videoView.videoWidth = mp.getVideoWidth();
                videoView.start();
                if(videomediaPlayer!=null&&videomediaPlayer.isPlaying()){
                    if(binder!=null&&binder.isVoicePlaying()){
                        videomediaPlayer.setVolume(0f,0f);
                    }
                }
                bannerView.requestFocus();
            }
        });

        videoView.setOnErrorListener(new OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                  videomediaPlayer=mp;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        videoNextOperation();
                    }
                }, 2000);
                return true;
            }

        });
        soundView.setOnClickListener(this);
        nextVideoView.setOnClickListener(this);
        currentIndex = 0;
        videoCurrent();
    }

    @Override
    protected void onStop() {
        super.onStop();
        videoPause();
        binder.stop();
    }
    /**
     *  初始化语音播报数据
     */
     private   void   initStartVoiceData(  ) {
         AudioFile audioFile = AccountEngine.getStartAudioFile();
         if(audioFile!=null) {
             startaudioFile=audioFile;
         }
    }

    private  void   initEndVoiceData(  ){
       AudioFile   audioFile=AccountEngine.getEndAudioFile();
        if(audioFile!=null) {
              endaudioFile=audioFile;
        }
    }
    /**
    * 初始化Video播放的视频数据
    */
    @SuppressLint("NewApi")
    private void initVideoData() {
        ArrayList<VideoInfo> list = AccountEngine.getVideoArray();
        videoArray = new ArrayList<File>();
        if (list != null) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                VideoInfo videoInfo = list.get(i);
                if (!TextUtils.isEmpty(videoInfo.sdPath)) {
                    File videoFile = new File(videoInfo.sdPath);
                    int fileSize = (int) SdUtil.getFileSize(videoFile);
                    if (videoFile.exists() && !videoFile.isDirectory() && fileSize == videoInfo.size) {
                        videoArray.add(videoFile);
                    }
                }
            }
        }
        RelativeLayout videoLayout = (RelativeLayout) findViewById(R.id.right_layout_view);
        if (videoArray.size() == 0) {
            videoLayout.setBackgroundResource(R.drawable.video_init_bg);
            videoView.setVisibility(View.GONE);
        } else {
            videoLayout.setBackgroundResource(R.drawable.btn_black_bg);
            videoView.setVisibility(View.VISIBLE);
        }
    }
    public void onEventMainThread(PrintEvent event) {
        if(event.getFind()){
            startActivity(new Intent(LauncherActivity.this, SelectPrinterActivity.class));
            unregisterPrinter();
        }
    }
    /**
     * 设置Video当前播放数据
     */
    private void videoCurrent() {
        if (videoArray == null || videoArray.size() <= currentIndex) {
            return;
        }
        File file = videoArray.get(currentIndex);
        if (file.exists()) {
            videoView.setVideoPath(file.getAbsolutePath());
            videoView.requestFocus();
        }
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopTimer();
        stopGpsTimer();
        stopDownloadVideo(this);
        stopLocation();
        unregisterVideo();
        unbindService(conn);
        BaseApplication.getInstance().disconnect();
        binder.stop();
        EventBus.getDefault().unregister(this);
    }

    /**
     * 初始化计价数据
     */
    private void initCounterView() {
        if (0 == BaseApplication.valuationType) {
            mileageLayout.setVisibility(View.VISIBLE);
            counterTypeView.setText("按里程");
            unitPriceView.setText(String.valueOf(AccountEngine.getEveryMilesPrice()) + "元/公里");
            priceView.setText("¥" + AccountEngine.getInitPrice());
            mileageView.setText("0.00");
            if (switchCounterView != null) {
                switchCounterView.setText(getString(R.string.counter_type_time));
            }
        } else if (1 == BaseApplication.valuationType) {
            mileageLayout.setVisibility(View.GONE);
            counterTypeView.setText("按时间");
            priceView.setText("¥0.0");
            unitPriceView.setText(String.valueOf(AccountEngine.getEveryHourPrice()) + "元/小时");
            if (switchCounterView != null) {
                switchCounterView.setText(getString(R.string.counter_type_distance));
            }
        }
        waitTimeView.setText("00'00");
        usedTimeView.setText("00'00");
    }
    /**
     * 切换计价方式
     */
    private void changeCounterView() {
        if (0 == BaseApplication.valuationType) {
            if (0 == AccountEngine.getEveryHourPrice()) {
                toast("暂不支持“按时间计价”", Toast.LENGTH_LONG);
            } else {
                BaseApplication.valuationType = 1;
                mileageLayout.setVisibility(View.GONE);
                counterTypeView.setText("按时间");
                priceView.setText("¥0.0");
                unitPriceView.setText(String.valueOf(AccountEngine.getEveryHourPrice()) + "元/小时");
                if (switchCounterView != null) {
                    switchCounterView.setText(getString(R.string.counter_type_distance));
                }
            }
        } else if (1 == BaseApplication.valuationType) {
            BaseApplication.valuationType = 0;
            mileageLayout.setVisibility(View.VISIBLE);
            counterTypeView.setText("按里程");
            unitPriceView.setText(String.valueOf(AccountEngine.getEveryMilesPrice()) + "元/公里");
            priceView.setText("¥" + AccountEngine.getInitPrice());
            mileageView.setText("0.00");
            if (switchCounterView != null) {
                switchCounterView.setText(getString(R.string.counter_type_time));
            }
        }
        waitTimeView.setText("00'00");
        usedTimeView.setText("00'00");
    }
    /**
     * 更新里程，价格数据
     * @param distances
     */
    private void updateCounterView(String distances) {
        mileageView.setText("" + AccountEngine.handDistance());
        BaseApplication.totalPrice = AccountEngine.valuationPrice();
        priceView.setText("¥" + AccountEngine.handPrice());
    }
    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.start_view:
               showConfirmView(0);
                break;
            case R.id.stop_view:
                showConfirmView(1);
            break;
            case R.id.user_layout_view:
                showUserMenu();
                break;
            case R.id.user_info_view:
                showUserInfo();
                break;
            case R.id.valuation_rules_view:
                showValuationRules();
                break;
            case R.id.vedio_info_view:
                showVideoCenter();
                break;
            case R.id.switch_counter_view:
                changeCounterType();
                break;
            case R.id.confirm_ok_view:
                menuConfirmOk();
                break;
            case R.id.confirm_cancel_view:
                confirmLayout.setVisibility(View.GONE);
                break;
            case R.id.sound_video_view:
                videoSoundOperation();
                break;
            case R.id.next_video_view:
                videoNextOperation();
                break;
            case R.id.message_view:
                goMessage();
                break;
            case R.id.message_num_view:
                goMessage();
                break;
            case R.id.message_center_view:
                goMessage();
                break;
            case R.id.logout_view:
                showLogout();
                break;
            default:
                break;
        }
    }


    /**
     * 消息中心
     */
    private void goMessage() {
        startActivity(new Intent(LauncherActivity.this, MessageActivity.class));
    }
    /**
     * 切换计价方式
     */
    private void changeCounterType() {
        if (1 == BaseApplication.locationStatus || 2 == BaseApplication.locationStatus) {
            toast("行程已开始", Toast.LENGTH_LONG);
            return;
        }
        changeCounterView();
        closeUserMenu();
    }
    /**
     * 确认按钮事件处理
     */
    private void menuConfirmOk() {
        if (menuType == 0) {
              BaseApplication.getInstance().disconnect();
            if (0 == BaseApplication.locationStatus || 3 == BaseApplication.locationStatus) {
                if(startaudioFile!=null&&!TextUtils.isEmpty(startaudioFile.sdPath)){
                    if(videomediaPlayer!=null&&videoView.isPlaying()){
                        videomediaPlayer.setVolume(0.0f,0.0f);
                    }
                    binder.play(startaudioFile.sdPath, new ICallBack() {
                        @Override
                        public void postExec() {
                            videomediaPlayer.setVolume(1.0f,1.0f);
                        }
                    });
                }
                // 开始计价
                AccountEngine.updatePremium();
                BaseApplication.orderId = null;
                valuationExcutor.start();
            } else if (1 == BaseApplication.locationStatus) {
                BaseApplication.locationStatus = 2;
                pauseCounter();
                if (AccountEngine.checkBluetoothOpen(this)) {
                    unregisterPrinter();
                    registerPrinter();
                    BaseApplication.getInstance().createPrinter();
                } else {
                    BaseApplication.getInstance().openBluetooth();
                }
            } else if (2 == BaseApplication.locationStatus) {
                BaseApplication.locationStatus = 1;
                startCounter();
            }
        } else if (menuType == 1) {
                stopCounter();
        } else if (menuType == 2) {
            if (AccountEngine.checkBluetoothOpen(this)) {
                unregisterPrinter();
                registerPrinter();
                BaseApplication.getInstance().createPrinter();
            } else {
                BaseApplication.getInstance().openBluetooth();
            }
        }
        confirmLayout.setVisibility(View.GONE);
    }

    /**
     * 开始行程
     */
    private void startCounter() {
        startView.setBackgroundResource(R.drawable.counter_pause);
        BaseApplication.locationAccuracy = 150;
        initLocation(1);
        startLocation();
        startTimer();
    }
/**
 *   播放超速语音
 */
   MediaPlayer speedWarnPlayer;
   public   void   playSpeenWarnAudio(  ){
       if(speedWarnPlayer==null){
           speedWarnPlayer = MediaPlayer.create(this,R.raw.warm);
       }
       speedWarnPlayer.setLooping(false);
       speedWarnPlayer.start();
       if(videomediaPlayer!=null&&videoView.isPlaying()){
           videomediaPlayer.setVolume(0.0f,0.0f);
       }
       speedWarnPlayer.setOnCompletionListener(new OnCompletionListener() {
           @Override
           public void onCompletion(MediaPlayer mp) {
               videomediaPlayer.setVolume(1.0f,1.0f);
           }
       });
   }
    /**
     * 停止行程
     */
    private void stopCounter() {
        if (0 == BaseApplication.locationStatus) {
            return;
        }
        //showOrderInfo();
        holidayExcutor.start();
        //stopOrderExcutor.start();
    }
    /**
     * 暂停行程
     */
    private void pauseCounter() {
        startView.setBackgroundResource(R.drawable.counter_start);
        initLocation(0);
        startLocation();
        stopTimer();
    }
    /**
     * 初始化时间数据
     */
    private void timeInit() {
        BaseApplication.usedTimeS = 0;
        BaseApplication.usedTimeM = 0;
        BaseApplication.usedTimeH = 0;
        BaseApplication.waitTimeS = 0;
        BaseApplication.waitTimeM = 0;
        BaseApplication.waitTimeH = 0;
    }

    /**
     * 计算时间
     */
    private void timeAdd() {
        BaseApplication.usedTimeS++;
        if (BaseApplication.usedTimeS == 60) {
            BaseApplication.usedTimeM++;
            BaseApplication.usedTimeS = 0;
        }
        if (BaseApplication.usedTimeM == 60) {
            BaseApplication.usedTimeH++;
            BaseApplication.usedTimeM = 0;
        }
        if (lastSpeed <= 0) {
            BaseApplication.waitTimeS++;
            if (BaseApplication.waitTimeS == 60) {
                BaseApplication.waitTimeM++;
                BaseApplication.waitTimeS = 0;
            }
            if (BaseApplication.waitTimeM == 60) {
                BaseApplication.waitTimeH++;
                BaseApplication.waitTimeM = 0;
            }
            if (BaseApplication.waitTimeH <= 0) {
                waitTimeView.setText(getString(R.string.wait_time_show, timeAddZero(BaseApplication.waitTimeM),
						timeAddZero(BaseApplication.waitTimeS)));
            } else {
                waitTimeView.setText(getString(R.string.used_time_show, timeAddZero(BaseApplication.waitTimeH),
						timeAddZero(BaseApplication.waitTimeM), timeAddZero(BaseApplication.waitTimeS)));
            }
        }
        if (BaseApplication.usedTimeH <= 0) {
            usedTimeView.setText(getString(R.string.wait_time_show, timeAddZero(BaseApplication.usedTimeM),
					timeAddZero(BaseApplication.usedTimeS)));
        } else {
            usedTimeView.setText(getString(R.string.used_time_show, timeAddZero(BaseApplication.usedTimeH),
					timeAddZero(BaseApplication.usedTimeM), timeAddZero(BaseApplication.usedTimeS)));
        }
        updateCounterView("" + singleDistance);
    }
    /**
     * 时间显示格式处理
     * @param time
     * @return
     */
    private String timeAddZero(int time) {
        if (time < 10) {
            return "0" + time;
        } else {
            return String.valueOf(time);
        }
    }
    /**
     * 时间Handler
     */
    Handler handlerRider = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == 1) {
                timeAdd();
            }
            super.handleMessage(msg);
        }
    };
    Timer timerRider = new Timer();
    TimerTask taskRider = new TimerTask() {
        @Override
        public void run() {
            // 需要做的事:发送消息
            Message message = new Message();
            message.what = 1;
            handlerRider.sendMessage(message);
        }
    };
    private void stopTimer() {
        if (timerRider != null) {
            timerRider.cancel();
            timerRider = null;
        }
        if (taskRider != null) {
            taskRider.cancel();
            taskRider = null;
        }
    }
    private void startTimer() {
        stopTimer();
        if (timerRider == null) {
            timerRider = new Timer();
        }
        if (taskRider == null) {
            taskRider = new TimerTask() {
                @Override
                public void run() {
                    // 需要做的事:发送消息
                    Message message = new Message();
                    message.what = 1;
                    handlerRider.sendMessage(message);
                }
            };
        }
        timerRider.schedule(taskRider, 0, WAIT_TIME);
    }

    /**
     * 确认View
     */
    private void showConfirmView(int type) {
        if (type == 1) {
            if (0 == BaseApplication.locationStatus || 3 == BaseApplication.locationStatus) {
                toast("还未开始计价",Toast.LENGTH_LONG);
                return;
            }
        }
        menuType = type;
        confirmLayout.setVisibility(View.VISIBLE);
    }

    /**
     * 展示用户中心弹窗
     */
    private void showUserMenu() {
        if (menuPopup == null) {
            View contentView = LayoutInflater.from(this).inflate(R.layout.popup_menu, null);
            menuPopup = new PopupWindow(contentView, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);
            menuPopup.setTouchable(true);
            userInfoView = (TextView) contentView.findViewById(R.id.user_info_view);
            valuationRulesView = (TextView) contentView.findViewById(R.id.valuation_rules_view);
            videoCenterView = (TextView) contentView.findViewById(R.id.vedio_info_view);
            switchCounterView = (TextView) contentView.findViewById(R.id.switch_counter_view);
            logoutView = (TextView) contentView.findViewById(R.id.logout_view);
            userMessageView = (TextView) contentView.findViewById(R.id.message_center_view);
            userInfoView.setOnClickListener(this);
            valuationRulesView.setOnClickListener(this);
            videoCenterView.setOnClickListener(this);
            switchCounterView.setOnClickListener(this);
            logoutView.setOnClickListener(this);
            userMessageView.setOnClickListener(this);
            menuPopup.setTouchInterceptor(new OnTouchListener() {

                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    return false;
                }
            });
            menuPopup.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.ic_launcher));
        }

        if (menuPopup.isShowing()) {
            menuPopup.dismiss();
        } else {
            menuPopup.showAsDropDown(userView, -57, 10);
        }
    }

    /**
     * 关闭用户中心弹窗
     */
    private void closeUserMenu() {
        if (menuPopup != null && menuPopup.isShowing()) {
            menuPopup.dismiss();
        }
    }

    /**
     * 展示用户信息
     */
    private void showUserInfo() {
        closeUserMenu();
        Intent intent = new Intent();
        intent.setClass(LauncherActivity.this, UserInfoActivity.class);
        startActivity(intent);
    }

    /**
     * 展示用户信息
     */
    private void showValuationRules() {
        closeUserMenu();
        Intent intent = new Intent();
        intent.setClass(LauncherActivity.this, ValuationRulesActivity.class);
        startActivity(intent);
    }

    /**
     * 视频中心
     */
    private void showVideoCenter() {
        closeUserMenu();
        Intent intent = new Intent();
        intent.setClass(LauncherActivity.this, VideoCenterActivity.class);
        startActivity(intent);
    }

    /**
     * 登出
     */
    private void showLogout() {
        if (1 == BaseApplication.locationStatus) {
            toast("行程未结束", Toast.LENGTH_LONG);
            return;
        }
        customAlert(getString(R.string.logout_confirm_msg), getString(R.string.confirm_ok), new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        logoutExcutor.start();
                    }
                }, getString(R.string.confirm_cancel), new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        dismissAlertDialog();
                    }
                }, false);
    }

    /**
     * 静音控制
     */
    private void videoSoundOperation() {
        videoOperationView.setVisibility(View.GONE);
        // 音量控制,初始化定义
        AudioManager mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        // 最大音量
        int maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        // 当前音量
        int currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        if (0 == currentVolume) {
            soundView.setBackgroundResource(R.drawable.video_sound_close);
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, maxVolume / 2, 0);
        } else {
            soundView.setBackgroundResource(R.drawable.video_sound_open);
            mAudioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
        }
    }

    /**
     * 初始化音量控制
     */
    private void initVideoSound() {
        // 音量控制,初始化定义
        AudioManager mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        // 最大音量
        int maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        // 当前音量
        int currentVolume = mAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        if (0 == currentVolume) {
            soundView.setBackgroundResource(R.drawable.video_sound_open);
        } else {
            soundView.setBackgroundResource(R.drawable.video_sound_close);
        }
    }
    /**
     * 下一个
     */
    private void videoNextOperation() {
        videoOperationView.setVisibility(View.GONE);
        if (videoArray != null && videoArray.size() > 0) {
            currentIndex++;
            if (videoArray.size() <= currentIndex) {
                currentIndex = 0;
            }
            videoCurrent();
        }
    }
    /**
     * 暂定播放
     */
    private void videoPause() {
        try {
            if (videoView != null && videoView.isPlaying() && videoView.canPause()) {
                videoView.pause();
                videoPosition = videoView.getCurrentPosition();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 继续播放
     */
    private void videoResume() {
        try {
            if (videoView != null && !videoView.isPlaying()) {
                videoView.seekTo(videoPosition);
                videoView.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        videoResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        videoPause();
    }

    /**
     * 初始化视频下载数据
     */
    private void initVideoInfo() {
        if (-1 != currentDownloadPosition) {
            return;
        }
        videoInfoArray = AccountEngine.getVideoArray();
        if (videoInfoArray == null || videoInfoArray.size() < 1) {
            return;
        }
        registerVideo();
        //currentDownloadPosition = 0;
        startDownload(null, null);
    }


    // 当前下载的序号
    private int currentDownloadPosition = -1;
    /**
     * 下载
     * @param url
     * @param path
     */
    private void startDownload(String url, String path) {
        if (videoInfoArray != null && videoInfoArray.size() > 0) {
            int size = videoInfoArray.size();
            for (int i = 0; i < size; i++) {
                VideoInfo videoInfo = videoInfoArray.get(i);
                if (TextUtils.equals(url, videoInfo.url)) {
                    videoInfoArray.get(i).sdPath = path;
                    videoInfo.sdPath = path;
                }
                File videoFile = null;
                int fileSize = -1;
                if (!TextUtils.isEmpty(videoInfo.sdPath)) {
                    videoFile = new File(videoInfo.sdPath);
                    fileSize = (int) SdUtil.getFileSize(videoFile);
                }
                if (videoFile == null || !videoFile.exists() || fileSize != videoInfo.size) {
                    if (currentDownloadPosition == i) {
                        //continue;
                    }
                    downloadVideo(this, videoInfo.url, videoInfo.id);
                    currentDownloadPosition = i;
                    break;
                }
            }
        }
    }
    /**
     * 调起下载服务
     * @param context
     * @param url
     * @param name
     */
    public void downloadVideo(Context context, String url, String name) {
        String systems = context.getResources().getString(R.string.app_name);
        Intent intent = new Intent(context, DownloadService.class);
        intent.setAction("com.zxw.weidi.download.service");
        intent.setPackage(context.getPackageName());
        intent.putExtra(Constant.DOWNLOAD_TYPE, 0);
        intent.putExtra(Constant.DOWNLOAD_TIP, systems);
        intent.putExtra(Constant.DOWNLOAD_FILE, name);
        intent.putExtra(Constant.DOWNLOAD_FILE_TYPE,"video");
        url = url.replace(" ", "");
        intent.putExtra(Constant.DOWNLOAD_URL, url);
        intent.putExtra(Constant.DOWNLOAD_PACKAGE, "weidi");
        context.startService(intent);
    }
    /**
     * 下载进度更新
     */
    private BroadcastReceiver videoDownloadReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            final String action = intent.getAction();
            if (TextUtils.equals(Constant.VIDEO_COMPETE, action) && intent != null) {
                Log.e("action",action);
                int progress = intent.getIntExtra(Constant.PROGRESSBAR_PROGRESS_KEY, 0);
                String url = intent.getStringExtra(Constant.DOWNLOAD_URL);
                String path = intent.getStringExtra(Constant.DOWNLOAD_SD_PATH);
                if (progress == 100) {
                    startDownload(url, path);
                    SharedPreferences shared = BaseApplication.getInstance().getSharedPreferences(Constant
							.ADVERT_RESULT_KEY, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = shared.edit();
                    int size = shared.getInt(Constant.ADVERT_SUM_KEY, 0);
                    for (int i = 0; i < size; i++) {
                        VideoInfo videoInfo = new VideoInfo();
                        videoInfo.id = shared.getString(Constant.ADVERT_VIDEO_ID_KEY + i, null);
                        videoInfo.name = shared.getString(Constant.ADVERT_VIDEO_NAME_KEY + i, null);
                        videoInfo.size = shared.getInt(Constant.ADVERT_VIDEO_SIZE_KEY + i, 0);
                        videoInfo.url = shared.getString(Constant.ADVERT_VIDEO_URL_KEY + i, null);
                        videoInfo.logourl = shared.getString(Constant.ADVERT_VIDEO_LOGOURL_KEY + i, null);
                        videoInfo.sdPath = shared.getString(Constant.ADVERT_VIDEO_PATH_KEY + i, null);
                        if (TextUtils.equals(url, videoInfo.url)) {
                            editor.putString(Constant.ADVERT_VIDEO_PATH_KEY + i, path);
                            editor.commit();
                            break;
                        }
                    }
                    ArrayList<VideoInfo> list = AccountEngine.getVideoArray();
                    videoArray = new ArrayList<File>();
                    if (list != null) {
                        int size1 = list.size();
                        for (int i = 0; i < size1; i++) {
                            VideoInfo videoInfo = list.get(i);
                            if (!TextUtils.isEmpty(videoInfo.sdPath)) {
                                File videoFile = new File(videoInfo.sdPath);
                                int fileSize = (int) SdUtil.getFileSize(videoFile);
                                if (videoFile.exists() && !videoFile.isDirectory() && fileSize == videoInfo.size) {
                                    videoArray.add(videoFile);
                                }
                            }
                        }
                    }

                    if (videoView.getVisibility() == View.GONE) {
                        RelativeLayout videoLayout = (RelativeLayout) findViewById(R.id.right_layout_view);
                        videoLayout.setBackgroundResource(R.drawable.btn_black_bg);
                        videoView.setVisibility(View.VISIBLE);
                        currentIndex = 0;
                        videoCurrent();
                    }
                } else {
                    startDownload(null, null);
                }

            }
        }
    };

    /**
     * 注册
     */
    private void registerVideo() {
        IntentFilter filter = new IntentFilter(Constant.VIDEO_COMPETE);
        registerReceiver(videoDownloadReceiver, filter);
    }

    /**
     * 注销
     */
    private void unregisterVideo() {
        if (videoDownloadReceiver != null) {
            try {
                unregisterReceiver(videoDownloadReceiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    /**
     * 定位信息广播接收
     */
    private BroadcastReceiver printerReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            final String action = intent.getAction();
            if (TextUtils.equals(Constant.SELECT_PRINTER_ACTION, action)) {
                startActivity(new Intent(LauncherActivity.this, SelectPrinterActivity.class));
                unregisterPrinter();
            }
        }
    };

    /**
     * 注册
     */
    private void registerPrinter() {
        IntentFilter filter = new IntentFilter(Constant.SELECT_PRINTER_ACTION);
        registerReceiver(printerReceiver, filter);
    }
    /**
     * 注销
     */
    private void unregisterPrinter() {
        if (printerReceiver != null) {
            try {
                unregisterReceiver(printerReceiver);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
   public  void   removeFile(String  filePath){
       String sDStateString = Environment.getExternalStorageState();
       if (sDStateString.equals(Environment.MEDIA_MOUNTED)) {
           File SDFile = Environment.getExternalStorageDirectory();
           File sdPath = new File(SDFile.getAbsolutePath() + "/weidi_voice");
           for (File file : sdPath.listFiles()) {
               if (filePath.equals(file.getAbsolutePath())) {
                   file.delete();
               }
           }
       }
   }
    /** 经纬度上传失败次数 */
    private int gpsReqErrorNum = 0;

    /**
     * 网络请求接口初始化
     */
    protected void initRpcExcutor() {
        logoutExcutor = new RpcExcutor<String>(this, 0, false) {
            @Override
            public void excute(Object... params) {
                ApiClient.createApi(LauncherActivity.this, RpcApi.class).logout(2, BaseApplication.getInstance()
						.getDriverId(), 1, BaseApplication.mac, this);
            }
            @Override
            public void onRpcFinish(String result, Object... params) {
                dismissAlertDialog();
                // 退出登录
                AccountEngine.clearLoginInfo();
                BaseApplication.getInstance().restart(LauncherActivity.this);

            }
            @Override
            public void onRpcException(int code, String msg, Object... params) {
                toast(msg, Toast.LENGTH_SHORT);
            }
        };
        logoutExcutor.setShowProgressDialog(true);
        logoutExcutor.setShowNetworkErrorView(false);
        valuationExcutor = new RpcExcutor<ValuationResult>(this, 0, false) {
            @Override
            public void excute(Object... params) {
                ApiClient.createApi(LauncherActivity.this, RpcApi.class).getValuation(3, BaseApplication.getInstance
                        ().getDriverId(), this);
            }
            @Override
            public void onRpcFinish(ValuationResult result, Object... params) {
                if (null == result) {
                    return;
                }
                AccountEngine.saveValuationRules(result);
                initCounterView();
                createOrderExcutor.start();
            }
            @Override
            public void onRpcException(int code, String msg, Object... params) {
                toast(msg, Toast.LENGTH_SHORT);
            }

        };
        valuationExcutor.setShowProgressDialog(true);
        valuationExcutor.setShowNetworkErrorView(false);
        gpsExcutor = new RpcExcutor<GpsResult>(this, 0, false) {
            @Override
            public void excute(Object... params) {
                double lng = (Double) params[0];
                double lat = (Double) params[1];
                int type = 0;
                if (1 == BaseApplication.locationType) {
                    type = 2;
                } else {
                    type = 1;
                }
                float speed = 0;
                float accuracy = 0;
                if (lastLocation != null) {
                    speed = lastLocation.getSpeed();
                    accuracy = lastLocation.getAccuracy();
                }
                ApiClient.createApi(LauncherActivity.this, RpcApi.class).submitGPS(5, BaseApplication.getInstance().getDriverId(), String.valueOf(lng), String.valueOf(lat), String.valueOf(speed), String.valueOf(accuracy), BaseApplication.orderId, type, this);
            }
            @Override
            public void onRpcFinish(GpsResult result, Object... params) {
                if (null == result) {
                    return;
                }
                if (result.unreadmsgnum != null && result.unreadmsgnum.sum > 0) {
                    messageNumView.setVisibility(View.VISIBLE);
                    if (result.unreadmsgnum.sum > 99) {
                        messageNumView.setText("99+");
                    } else {
                        messageNumView.setText(String.valueOf(result.unreadmsgnum.sum));
                    }
                } else {
                    messageNumView.setVisibility(View.GONE);
                }
                gpsReqErrorNum = 0;
                int bannerVersion = AccountEngine.getBannerVersion();
                if (result.padbanner != null && bannerVersion < result.padbanner.version) {
                    bannerExcutor.start();
                }
                int advertVersion = AccountEngine.getAdvertVersion();
                if (result.advertinfo != null && advertVersion < result.advertinfo.version) {
                    advertExcutor.start();
                }
            }

            @Override
            public void onRpcException(int code, String msg, Object... params) {
                // toast(msg, Toast.LENGTH_SHORT);
				/*
				gpsReqErrorNum++;
				if (gpsReqErrorNum < 3) {
					new Handler().postDelayed(new Runnable() {

						@Override
						public void run() {
							if (lastLocation != null) {
								gpsExcutor.start(lastLocation.getLongitude(),
										lastLocation.getLatitude(),
										String.valueOf(lastLocation.getTime()));
							}
						}

					}, 10000);
				}
				*/
            }
        };
        gpsExcutor.setShowProgressDialog(false);
        gpsExcutor.setShowNetworkErrorView(false);
        bannerExcutor = new RpcExcutor<BannerResult>(this, 0, false) {
            @Override
            public void excute(Object... params) {
                Log.e("getDriverId()",BaseApplication.getInstance().getDriverId()+"");
                ApiClient.createApi(LauncherActivity.this, RpcApi.class).getPadBanner(6, BaseApplication.getInstance().getDriverId(), this);
            }
            @Override
            public void onRpcFinish(BannerResult result, Object... params) {
                if (null == result) {
                    return;
                }
                AccountEngine.saveBannerInfo(result.padbanner);
                updateBanner();
            }

            @Override
            public void onRpcException(int code, String msg, Object... params) {
                toast(msg, Toast.LENGTH_SHORT);
            }

        };
        bannerExcutor.setShowProgressDialog(false);
        bannerExcutor.setShowNetworkErrorView(false);
        voiceEndExcutor=new RpcExcutor<VoiceResult>(this, 0, false) {
            @Override
            public void excute(Object... params) {
                ApiClient.createApi(LauncherActivity.this,RpcApi.class).getEndVoiceInfo(16, BaseApplication.getInstance().getDriverId(), this);
            }
            @Override
            public void onRpcFinish(VoiceResult voiceResult, Object... params) {
                if(null==voiceResult){
                    return;
                }
               if(voiceResult.audioFile.version>AccountEngine.getVoiceEndVersion()){
                    if(!TextUtils.isEmpty(endaudioFile.sdPath)){
                        removeFile(endaudioFile.sdPath);
                    }
                    AccountEngine.saveEndVoiceInfo(voiceResult.audioFile);
                    initEndVoiceData();
                    downLoadVoice(voiceResult.audioFile.fileurl, voiceResult.audioFile.name, 2);
               }
            }
            @Override
            public void onRpcException(int code, String msg, Object... params) {
                toast(msg, Toast.LENGTH_SHORT);
            }
        };
        voiceEndExcutor.setShowProgressDialog(false);
        voiceEndExcutor.setShowNetworkErrorView(false);
        voiceStartExcutor=new RpcExcutor<VoiceResult>(this, 0, false) {
            @Override
            public void excute(Object... params) {
                ApiClient.createApi(LauncherActivity.this, RpcApi.class).getStartVoiceInfo(15, BaseApplication.getInstance().getDriverId(), this);
            }
            @TargetApi(Build.VERSION_CODES.HONEYCOMB)
            @Override
            public void onRpcFinish(VoiceResult voiceResult, Object... params) {
                    if(null==voiceResult){
                      return;
                    }
                    if(voiceResult.audioFile.version>AccountEngine.getVoiceStartVersion()){
                          if(!TextUtils.isEmpty(startaudioFile.sdPath)){
                              removeFile(startaudioFile.sdPath);
                           }
                           AccountEngine.saveStartVoiceInfo(voiceResult.audioFile);
                           initStartVoiceData();
                           downLoadVoice(voiceResult.audioFile.fileurl,voiceResult.audioFile.name,1);
                   }
               }
            @Override
            public void onRpcException(int code, String msg, Object... params) {
                  toast(msg, Toast.LENGTH_SHORT);
            }
        };
        voiceStartExcutor.setShowProgressDialog(false);
        voiceStartExcutor.setShowNetworkErrorView(false);
        advertExcutor = new RpcExcutor<AdvertResult>(this, 0, false) {
            @Override
            public void excute(Object... params) {
                ApiClient.createApi(LauncherActivity.this, RpcApi.class).getAdvertInfo(7, BaseApplication.getInstance().getDriverId(), this);
            }
            @Override
            public void onRpcFinish(AdvertResult result, Object... params) {
                if (null == result) {
                    return;
                }
              if (result.version > AccountEngine.getAdvertVersion()) {
                    AccountEngine.saveAdvertInfo(result);
                    initVideoData();
               }
                currentDownloadPosition = -1;
                initVideoInfo();
                Intent  intent=new Intent(LauncherActivity.this, RemoveFileService.class);
                intent.putExtra(Constant.DOWNLOAD_FILE_TYPE,"video");
                startService(intent);
            }
            @Override
            public void onRpcException(int code, String msg, Object... params) {
                toast(msg, Toast.LENGTH_SHORT);
            }
        };
        advertExcutor.setShowProgressDialog(false);
        advertExcutor.setShowNetworkErrorView(false);
        holidayExcutor = new RpcExcutor<HolidayResult>(this, 0, false) {
            @Override
            public void excute(Object... params) {
                holidaydata = null;
                ApiClient.createApi(LauncherActivity.this, RpcApi.class).getHolidayInfo(10, BaseApplication
                        .getInstance().getDriverId(), this);
            }
            @Override
            public void onRpcFinish(HolidayResult result, Object... params) {
                if (null == result || null==result.holidaydata) {
                    stopOrderExcutor.start();
                    return;
                }
                holidaydata = result.holidaydata;
                stopOrderExcutor.start();
            }
            @Override
            public void onRpcException(int code, String msg, Object... params) {
                toast(msg, Toast.LENGTH_SHORT);
                stopOrderExcutor.start();
            }

        };
        holidayExcutor.setShowProgressDialog(false);
        holidayExcutor.setShowNetworkErrorView(false);
        createOrderExcutor = new RpcExcutor<CreateOrderResult>(this, 0, false) {
            @Override
            public void excute(Object... params) {
                int type = 0;
                if (0 == BaseApplication.valuationType) {
                    type = 1;
                } else if (1 == BaseApplication.valuationType) {
                    type = 2;
                }
                ApiClient.createApi(LauncherActivity.this, RpcApi.class).createOrder(8, BaseApplication.getInstance()
								.getDriverId(), type,// 1：按里程。2：按时间
                        this);
            }

            @Override
            public void onRpcFinish(CreateOrderResult result, Object... params) {
                if (null == result || TextUtils.isEmpty(result.distanceid)) {
                    return;
                }
                BaseApplication.orderId = result.distanceid;
                BaseApplication.locationStatus = 1;
                BaseApplication.lastDistance = 0;
                timeInit();
                startCounter();
                stopGpsTimer();
                startGpsTimer(REQ_GPS_2_TIME);
            }

            @Override
            public void onRpcException(int code, String msg, Object... params) {
                toast(msg, Toast.LENGTH_SHORT);
            }

        };
        createOrderExcutor.setShowProgressDialog(true);
        createOrderExcutor.setShowNetworkErrorView(false);
        stringSpeedRpcExcutor=new RpcExcutor<String>(this, 0, false) {
            @Override
            public void excute(Object... params) {
              String  time= (String) params[0];
              double  currentSpeedValue= (double) params[1];
              double  speedWarnValue= (double) params[2];
              ApiClient.createApi(LauncherActivity.this, RpcApi.class).postSpeedInfo(14, BaseApplication.getInstance().getDriverId(),time,currentSpeedValue,speedWarnValue,this);
            }
            @Override
            public void onRpcFinish(String s, Object... params) {
                speenNum=0;
                isSpeendWarm=false;
                playSpeenWarnAudio();
            }
            @Override
            public void onRpcException(int code, String msg, Object... params) {
                toast(msg, Toast.LENGTH_SHORT);
            }
        };
        stringSpeedRpcExcutor.setShowProgressDialog(false);
        stringSpeedRpcExcutor.setShowNetworkErrorView(false);
        speedExcutor=new RpcExcutor<SpeedWarnResult>(this, 0, false) {
            @Override
            public void excute(Object... params) {
                ApiClient.createApi(LauncherActivity.this, RpcApi.class).createSpeedWarn(13,BaseApplication.getInstance().getDriverId(), this);
            }
            @Override
            public void onRpcFinish(SpeedWarnResult speedWarnResult, Object... params) {
                 if (speedWarnResult==null){
                     return;
                 }
                 speedWarnValue=speedWarnResult.speeDwarn.value;
            }
        };
        speedExcutor.setShowProgressDialog(false);
        speedExcutor.setShowNetworkErrorView(false);
        stopOrderExcutor = new RpcExcutor<StopOrderResult>(this, 0, false) {
            @Override
            public void excute(Object... params) {
                if (startLocation == null) {
                    startLocation = lastLocation;
                }
                if (endLocation == null) {
                    endLocation = lastLocation;
                }
                if (startLocation == null || endLocation == null) {
                    stopOrderExcutor.dismiss();
                    toast("操作失败", Toast.LENGTH_SHORT);
                    return;
                }
                String slng = String.valueOf(startLocation.getLongitude());
                String slat = String.valueOf(startLocation.getLatitude());
                String elng = String.valueOf(endLocation.getLongitude());
                String elat = String.valueOf(endLocation.getLatitude());
                String miles = AccountEngine.handDistance();
                String price = AccountEngine.valuationOrderPrice(holidaydata);
                String sumtime = String.valueOf(AccountEngine.valuationTotalTime());
                String waittime = String.valueOf(AccountEngine.valuationWaitTime());
                ApiClient.createApi(LauncherActivity.this, RpcApi.class).stopOrder(9, BaseApplication.getInstance().getDriverId(), BaseApplication.orderId, slng, slat, elng, elat, price, miles, sumtime, waittime, this);
            }
            @Override
            public void onRpcFinish(StopOrderResult result, Object... params) {
                if (null == result) {
                    return;
                }
                if(endaudioFile!=null&&!TextUtils.isEmpty(endaudioFile.sdPath)) {
                    if (videomediaPlayer != null && videoView.isPlaying()) {
                        videomediaPlayer.setVolume(0.0f, 0.0f);
                    }
                    binder.play(endaudioFile.sdPath, new ICallBack() {
                        @Override
                        public void postExec() {
                            videomediaPlayer.setVolume(1.0f,1.0f);
                        }
                    });
                }
                startView.setBackgroundResource(R.drawable.counter_start);
                BaseApplication.locationStatus = 3;
                BaseApplication.orderNo = result.distanceno;
                BaseApplication.orderDate = result.sdate;
                BaseApplication.orderStartTime = result.stime;
                BaseApplication.orderEndTime = result.etime;
                BaseApplication.orderTotalPrice = "";
                BaseApplication.zfbqrcode=result.zfbqrcode;
                BaseApplication.wxqrcode=result.wxqrcode;
                initCounterView();
                initLocation(0);
                startLocation();
                stopTimer();
                showOrderInfo(result.zfbqrcode,result.wxqrcode);
            }
            @Override
            public void onRpcException(int code, String msg, Object... params) {
                toast(msg, Toast.LENGTH_SHORT);
            }
        };
        stopOrderExcutor.setShowProgressDialog(true);
        stopOrderExcutor.setShowNetworkErrorView(false);
    }
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private   void   downLoadVoice(String  url, String  fileName, final  int   witchTag){
        final DownloadManager dManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        Uri uri = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(uri);
        request.setDestinationInExternalPublicDir("weidi_voice", fileName);
        request.setMimeType("application/vnd.android.package-archive");
        // 设置为可被媒体扫描器找到
        request.allowScanningByMediaScanner();
        // 获取此次下载的ID
        final long refernece = dManager.enqueue(request);
        IntentFilter filter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        BroadcastReceiver receiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                long myDwonloadID = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
                if (refernece == myDwonloadID) {
                    DownloadManager.Query query = new DownloadManager.Query();
                    query.setFilterById(myDwonloadID);
                    Cursor cursor = dManager .query(query);
                    if (cursor.moveToFirst()) {
                        String fileName = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_FILENAME));
                        if (witchTag==1){
                            if(startaudioFile!=null&&!TextUtils.isEmpty(fileName)){
                                AudioFile  audioFile=new AudioFile();
                                audioFile.fileurl=startaudioFile.fileurl;
                                audioFile.name=startaudioFile.name;
                                audioFile.version=startaudioFile.version;
                                audioFile.id=startaudioFile.id;
                                audioFile.sdPath=fileName;
                                audioFile.size=startaudioFile.size;
                                AccountEngine.saveStartVoiceInfo(audioFile);
                                initStartVoiceData();
                            }
                        }else if( witchTag==2){
                            if(endaudioFile!=null&&!TextUtils.isEmpty(fileName)){
                                AudioFile  audioFile=new AudioFile();
                                audioFile.fileurl=endaudioFile.fileurl;
                                audioFile.name=endaudioFile.name;
                                audioFile.version=endaudioFile.version;
                                audioFile.id=endaudioFile.id;
                                audioFile.sdPath=fileName;
                                audioFile.size=endaudioFile.size;
                                AccountEngine.saveEndVoiceInfo(audioFile);
                                initEndVoiceData();
                            }
                        }
                    }
                    cursor.close();
                }
            }
        };
        registerReceiver(receiver, filter);
    }
    /**
     * 关闭下载服务
     * @param context
     */
    private void stopDownloadVideo(Context context) {
        this.stopService(new Intent(context, DownloadService.class));
    }
    /**
     * 定位服务
     */
    private AMapLocationClient locationClient = null;
    private AMapLocationClientOption locationOption = null;
    private static final long MILE_POST_DETAIL_TIME = 1 * 1000L;
    private static final long GPS_POST_DETAIL_TIME = 3 * 1000L;

    /**
     * 上一次定位成功的坐标
     */
    private AMapLocation lastLocation;
    private AMapLocation startLocation;
    private AMapLocation endLocation;

    /**
     * 单次
     */
    private float singleDistance = 0;

    /**
     * 初始化定位服务
     * @param type
     */
    private void initLocation(int type) {
        stopLocation();
        BaseApplication.locationType = type;
        locationClient = new AMapLocationClient(this.getApplicationContext());
        locationOption = new AMapLocationClientOption();
        if (0 == type) {
            // 设置定位模式为仅设备模式
            locationOption.setLocationMode(AMapLocationMode.Hight_Accuracy);
            // 设置发送定位请求的时间间隔,最小值为1000，如果小于1000，按照1000算
            locationOption.setInterval(GPS_POST_DETAIL_TIME);
        } else if (1 == type) {
            // 设置定位模式为仅设备模式
            locationOption.setLocationMode(AMapLocationMode.Device_Sensors);
            // 设置发送定位请求的时间间隔,最小值为1000，如果小于1000，按照1000算
            locationOption.setInterval(MILE_POST_DETAIL_TIME);
            //startGpsLocation();
        }

        // 设置为不是单次定位
        locationOption.setOnceLocation(false);
        // 设置定位监听
        locationClient.setLocationListener(this);
        // 设置是否需要显示地址信息
        locationOption.setNeedAddress(false);
        /**
         * 设置是否优先返回GPS定位结果，如果30秒内GPS没有返回定位结果则进行网络定位 注意：只有在高精度模式下的单次定位有效，其他方式无效
         */
        locationOption.setGpsFirst(true);

        // 设置定位参数locationClient.setLocationOption(locationOption);

    }


    private void startGpsLocation() {
        Intent intent = new Intent(LauncherActivity.this, GpsService.class);
        startService(intent);
    }

    private void stopGpsLocation() {
        Intent intent = new Intent(LauncherActivity.this, GpsService.class);
        stopService(intent);
    }

    /**
     * 开始定位
     */
    private void startLocation() {
        lastLocation = null;
        // 启动定位
        locationClient.startLocation();
    }

    /**
     * 停止定位
     */
    private void stopLocation() {
        if (null != locationClient) {
            /**
             * 如果AMapLocationClient是在当前Activity实例化的，
             * 在Activity的onDestroy中一定要执行AMapLocationClient的onDestroy
             */
            locationClient.onDestroy();
            locationClient = null;
            locationOption = null;
        }
        //stopGpsLocation();
    }
    @Override
    public void onLocationChanged(AMapLocation loc) {
        if (null != loc) {
            Message msg = mHandler.obtainMessage();
            msg.obj = loc;
            msg.what = Utils.MSG_LOCATION_FINISH;
            mHandler.sendMessage(msg);
        }
    }
    Handler mHandler = new Handler() {
        public void dispatchMessage(Message msg) {
            switch (msg.what) {
                // 开始定位
                case Utils.MSG_LOCATION_START:
                    toast("定位开始", Toast.LENGTH_LONG);
                    break;
                // 定位完成
                case Utils.MSG_LOCATION_FINISH:
                    AMapLocation loc = (AMapLocation) msg.obj;
                    if (null == loc || 0 == loc.getLatitude()) {
                        return;
                    }
                   if(loc.getSpeed()*3.6>speedWarnValue){
                           speenNum++;
                           if(speenNum>=5){
                            currentSpeedValue=loc.getSpeed()*3.6;
                            isSpeendWarm=true;
                        }
                   }
                    if (isSpeendWarm) {
                        DecimalFormat decimalFormat=new DecimalFormat(".0");//构造方法的字符格式这里如果小数不足2位,会以0补足.
                        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date curDate = new Date(System.currentTimeMillis());
                        time = formatter.format(curDate);
                        stringSpeedRpcExcutor.start(time,Double.parseDouble(decimalFormat.format(Double.valueOf(currentSpeedValue))), speedWarnValue);
                    }
                    boolean requestGps = false;
                    if (0 == BaseApplication.locationType) {
                        if(lastLocation==null){
                            requestGps = true;
                        }
                        lastLocation = loc;
                        if (requestGps && lastLocation != null && 0 != lastLocation.getLatitude()) {
                            gpsExcutor.start(lastLocation.getLongitude(), lastLocation.getLatitude(), String.valueOf(lastLocation.getTime()));
                        }
                    } else if (1 == BaseApplication.locationType) {
                        if (lastLocation == null) {
                            if(loc.getSpeed()<=0){
                                loc.setSpeed(0.1f);
                            }
                            startLocation = loc;
                            lastLocation = loc;
                        } else {
                            // 计算
                            getDistance(lastLocation, loc);
                        }
                    }
                    break;
                // 停止定位
                case Utils.MSG_LOCATION_STOP:
                    toast("定位停止", Toast.LENGTH_LONG);
                    break;
                default:
                    break;
            }

        }

        ;
    };

    private AMapLocation tempLocation;

    /**
     * 里程计算(请勿修改)
     * @param start
     * @param end
     */
    private void getDistance(AMapLocation start, AMapLocation end) {
        if (start == null || null == end) {
            return;
        }
        if (start.getLatitude() == 0 || start.getLongitude() == 0 || end.getLatitude() == 0 || end.getLongitude() == 0) {
            return;
        }
        lastSpeed = end.getSpeed();
		/*
		 * if(end.getSpeed()>0){ stopTimer(); }else{ startTimer(); }
		 */
        if (end.getAccuracy() > BaseApplication.locationAccuracy) {
            return;
        }
        LatLng startLatlng = new LatLng(start.getLatitude(), start.getLongitude());
        LatLng endLatlng = new LatLng(end.getLatitude(), end.getLongitude());
        if (start.getSpeed() > 0 && end.getSpeed() > 0) {
            singleDistance = AMapUtils.calculateLineDistance(startLatlng, endLatlng);
            lastLocation = end;
        } else {
            singleDistance = 0;
        }
        if (singleDistance > 0) {
            BaseApplication.lastDistance = BaseApplication.lastDistance + singleDistance;
        }
        endLocation = end;
    }
    // GPS上报
    private int   speenNum=0;
    private  String     time;
    private  double  currentSpeedValue;
    Handler gpsHander = new Handler() {
        public void handleMessage(Message msg) {
            if (msg.what == 1) {
                Log.e("", "需要上传GPS信息...");
                if (lastLocation != null && 0 != lastLocation.getLatitude()) {
                    gpsExcutor.start(lastLocation.getLongitude(), lastLocation.getLatitude(), String.valueOf(lastLocation.getTime()));
                }
            }
            super.handleMessage(msg);
        }

        ;
    };
    Timer gpsTimer = null;
    TimerTask gpsTask = null;

    private void stopGpsTimer() {
        if (gpsTimer != null) {
            gpsTimer.cancel();
            gpsTimer = null;
        }

        if (gpsTask != null) {
            gpsTask.cancel();
            gpsTask = null;
        }
    }

    private void startGpsTimer(long time) {
        stopGpsTimer();
        if (gpsTimer == null) {
            gpsTimer = new Timer();
        }
        if (gpsTask == null) {
            gpsTask = new TimerTask() {

                @Override
                public void run() {
                    // 需要做的事:发送消息
                    Message message = new Message();
                    message.what = 1;
                    gpsHander.sendMessage(message);
                }
            };
        }
        if (lastLocation != null && 0 != lastLocation.getLatitude()) {
            gpsExcutor.start(lastLocation.getLongitude(), lastLocation.getLatitude(), String.valueOf(lastLocation.getTime()));
        }
        gpsTimer.schedule(gpsTask, 20, time);
    }

    private void initImageLoader() {
        // 初始化imageLoader 否则会报错
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(this));

        options = new DisplayImageOptions.Builder().cacheInMemory(false) // 设置下载的图片是否缓存在内存中
                .cacheOnDisc(false) // 设置下载的图片是否缓存在SD卡中
                .showImageForEmptyUri(R.drawable.user_iamge_defaut).showImageOnFail(R.drawable.user_iamge_defaut).build();
    }

    private void createPayQRCodeWithLogo(String   zfbUrl, String   wxUrl, final ImageView    zfbIv, final ImageView   wxIv) {
        QRCodeEncoder.encodeQRCode(zfbUrl, CHZZQRCodeUtil.dp2px(LauncherActivity.this, 200), Color.parseColor("#000000"), BitmapFactory.decodeResource(LauncherActivity.this.getResources(), R.drawable.zfb), new QRCodeEncoder.Delegate() {
            @Override
            public void onEncodeQRCodeSuccess(Bitmap bitmap) {
                   zfbIv.setImageBitmap(bitmap);
            }
            @Override
            public void onEncodeQRCodeFailure() {
                Toast.makeText(LauncherActivity.this, "生成支付宝二维码失败", Toast.LENGTH_SHORT).show();
            }
        });
        QRCodeEncoder.encodeQRCode(wxUrl, CHZZQRCodeUtil.dp2px(LauncherActivity.this,200), Color.parseColor("#000000"), BitmapFactory.decodeResource(LauncherActivity.this.getResources(), R.drawable.wx), new QRCodeEncoder.Delegate() {
            @Override
            public void onEncodeQRCodeSuccess(Bitmap bitmap) {
                wxIv.setImageBitmap(bitmap);
            }
            @Override
            public void onEncodeQRCodeFailure() {
                Toast.makeText(LauncherActivity.this, "生成微信二维码失败", Toast.LENGTH_SHORT).show();
            }
        });
    }
    /** *
     *  支付完成对话框
     */
  private  AlertDialog   showPayCompDlg;
  private  void   showPayCompDlg( ){
      if(showPayCompDlg==null){
        showPayCompDlg=new AlertDialog.Builder(LauncherActivity.this).create();
      }
      showPayCompDlg.show();
      Window window=showPayCompDlg.getWindow();
      window.setContentView(R.layout.layout_complete_pay);
      TextView  cancelPaintTv= (TextView) window.findViewById(R.id.cancelOrderPrint);
      TextView  paintTv= (TextView) window.findViewById(R.id.orderPrint);
      cancelPaintTv.setOnClickListener(new OnClickListener() {
          @Override
          public void onClick(View v) {
              showPayCompDlg.dismiss();
          }
      });
      paintTv.setOnClickListener(new OnClickListener() {
          @Override
          public void onClick(View v) {
              if (AccountEngine.checkBluetoothOpen(LauncherActivity.this)) {
                    toast("正在连接打印机，此操作可能需要等待1-2分钟，等候期间请勿重复点击", Toast.LENGTH_LONG);
//                    showPayCompDlg.dismiss();
                    unregisterPrinter();
                    registerPrinter();
                    BaseApplication.getInstance().createPrinter();
                } else {
//                    showPayCompDlg.dismiss();
                    toast("正在打开蓝牙", Toast.LENGTH_LONG);
                    BaseApplication.getInstance().openBluetooth();
                }
          }
      });
  }
    /**
     * 展示订单信息
     */
    private AlertDialog  showOrderDlg;
    private void showOrderInfo(String  zfbqrcode,String    wxqrcode) {
        if(showOrderDlg==null){
            showOrderDlg=new AlertDialog.Builder(LauncherActivity.this).create();
        }
        showOrderDlg.setCancelable(false);
        showOrderDlg.show();
        Window window=showOrderDlg.getWindow();
        window.setContentView(R.layout.payorder_dialog);
        ImageView  zfbIv= (ImageView) window.findViewById(R.id.zfbIv);
        ImageView  wxIv= (ImageView) window.findViewById(R.id.wxIv);
        TextView   orderDistanceTv= (TextView) window.findViewById(R.id.orderDistanceTv);
        TextView   orderWaitTv= (TextView) window.findViewById(R.id.orderWaitTv);
        TextView   orderUnitTv= (TextView) window.findViewById(R.id.orderUnitTv);
        TextView   orderTimeTv= (TextView) window.findViewById(R.id.orderTimeTv);
        TextView   orderConfirm= (TextView) window.findViewById(R.id.orderConfirm);
        TextView   initPriceTv= (TextView) window.findViewById(R.id.initPriceTv);
        TextView   orderSumTv= (TextView) window.findViewById(R.id.orderSumTv);
        TextPaint tp = orderSumTv.getPaint();
        tp.setFakeBoldText(true);
        PayOrder    payOrder=AccountEngine.getPayOrder(holidaydata);
        orderDistanceTv.setText(payOrder.getDiatance());
        orderWaitTv.setText(payOrder.getWaitTime());
        orderTimeTv.setText(payOrder.getUseTime());
        orderUnitTv.setText(payOrder.getUnitPrice());
        orderSumTv.setText(payOrder.getSumPrice());
        initPriceTv.setText(payOrder.getInitPrice());
        createPayQRCodeWithLogo(zfbqrcode,wxqrcode,zfbIv,wxIv);
        orderConfirm.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                showOrderDlg.dismiss();
                initCounterView();
                showPayCompDlg();
            }
        });
    }
    @Override
    public void back() {
        //super.back();
        showLogout();

    }
    SoundPool soundPool;
    HashMap<Integer, Integer> soundMap ;
    MediaPlayer   mediaPlayer;
    private void initMediaPlayer(){
        soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        soundMap = new HashMap<Integer, Integer>();
        soundMap.put(1, soundPool.load(this, R.raw.graborder, 1));
        AssetFileDescriptor descriptor;
        try {
            descriptor = LauncherActivity.this.getResources().getAssets().openFd("graborder.wav");
            soundMap.put(2, soundPool.load(descriptor, 1 ));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    private void playSound(int soundID){
    int streamID =soundPool.play(soundMap.get(soundID), 1.0f, 1.0f, 1, 0, 1);
    if(streamID == 0){
        //播放失败
        Toast.makeText(this, "fail", Toast.LENGTH_SHORT).show();
    }else{
        //播放成功
        Toast.makeText(this, "ok", Toast.LENGTH_SHORT).show();
       }
    }
    private void playMedia(int soundID) {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.reset();
        }
        if (soundID == 3) {
            mediaPlayer = MediaPlayer.create(this, R.raw.graborder);
            mediaPlayer.start();
        } else if (soundID == 4) {
            try {
                AssetManager assetManager = this.getAssets();
                AssetFileDescriptor fileDesc = assetManager.openFd("graborder.wav");
                mediaPlayer = new MediaPlayer();
                mediaPlayer.setDataSource(fileDesc.getFileDescriptor(), fileDesc.getStartOffset(), fileDesc.getLength());
                mediaPlayer.prepare();
                mediaPlayer.start();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalStateException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    private  class  PlayerConn  implements ServiceConnection{
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            binder=(MusicalPlayerINterface)service;

        }
        @Override
        public void onServiceDisconnected(ComponentName name) {
            binder=null;
        }
    }
}







