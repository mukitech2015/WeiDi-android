package com.junen.vtaxi.http;

import android.content.Context;

import com.junen.vtaxi.common.BaseUrl;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

public class ApiClient {
	private static RestAdapter commonAdapter;
	private static RestAdapter adapter;
	public static <T> T createApi(Context context, Class<T> clazz) {
		if (commonAdapter == null) {
			synchronized (ApiClient.class) {
				if (commonAdapter == null) {
					RestAdapter.Builder builder = new RestAdapter.Builder();
					builder.setEndpoint(BaseUrl.IP);
					builder.setConverter(new DwdConverter());
					builder.setClient(new OkClient(AndroidOkHttpClient
							.getInstance(context)));
					commonAdapter = builder.build();
				}
			}
		}
		return commonAdapter.create(clazz);
	}

	/*
	public static <T> T createApi(Context context, Class<T> clazz) {
		if (adapter == null) {
			synchronized (ApiClient.class) {
				if (adapter == null) {
					String url = BaseApplication.getInstance().getUrl(
							context);
					if (TextUtils.isEmpty(url)) {
						return null;
					}
					RestAdapter.Builder builder = new RestAdapter.Builder();
					builder.setEndpoint(url);// 设置远程地址
					builder.setConverter(new DwdConverter());
					builder.setClient(new EncryptClient(AndroidOkHttpClient
							.getInstance(context)));
					adapter = builder.build();
				}
			}
		}
		return adapter.create(clazz);
	}
*/
	public static void clearAdapter() {
		adapter = null;
	}

}
