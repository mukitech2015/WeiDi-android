package com.junen.vtaxi.service;


import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.NotificationCompat;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationClientOption.AMapLocationMode;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.maps.AMapUtils;
import com.amap.api.maps.model.LatLng;
import com.junen.vtaxi.R;
import com.junen.vtaxi.app.BaseApplication;
import com.junen.vtaxi.common.Constant;
import com.junen.vtaxi.util.Utils;

public class LocationService extends Service implements AMapLocationListener {
	private static final String TAG = LocationService.class.getSimpleName();

	private static final long POST_DETAIL_TIME = 1 * 1000L;
	
	private AMapLocationClient locationClient = null;
	private AMapLocationClientOption locationOption = null;

	
	/*
	 * 上一次定位成功的坐标
	 */
	private AMapLocation lastLocation;
	/*
	 * 当前
	 */
	//private float lastDistance = 0;
	/*
	 * 单次
	 */
	private float singleDistance = 0;
	/*
	 * 精度要求
	 */
	private int setA = 150;
	
	
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		initLocation();
		startLocation();
	}

	@Override
	public void onDestroy() {
		stopForeground(true);
		stopLocation();
		super.onDestroy();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		NotificationCompat.Builder nb = new NotificationCompat.Builder(this);
		nb.setOngoing(true);
		nb.setContentTitle(getString(R.string.app_name));
		String countentText ="运行中...";

		StringBuffer sbf = new StringBuffer();

		

		sbf.append(" ");
		sbf.append(countentText);
		nb.setContentText(sbf.toString());

		nb.setSmallIcon(R.drawable.ic_launcher);
		Intent intents = new Intent(Constant.NOTICATION_CLICK_ACTION);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0,
				intents, PendingIntent.FLAG_UPDATE_CURRENT);
		nb.setContentIntent(pendingIntent);
		startForeground(1423, nb.build());
				
		//flags = START_STICKY;  
		//return super.onStartCommand(intent, flags, startId);
		return START_NOT_STICKY;
	}

	@Override
	public boolean stopService(Intent name) {
		return super.stopService(name);
	}

	private void initLocation() {
		locationClient = new AMapLocationClient(this.getApplicationContext());
		locationOption = new AMapLocationClientOption();

		// 设置定位模式为仅设备模式
		locationOption.setLocationMode(AMapLocationMode.Device_Sensors);
		// 设置为不是单次定位
		locationOption.setOnceLocation(false);

		// 设置定位监听
		locationClient.setLocationListener(this);
		// 设置是否需要显示地址信息
		locationOption.setNeedAddress(false);
		/**
		 * 设置是否优先返回GPS定位结果，如果30秒内GPS没有返回定位结果则进行网络定位 注意：只有在高精度模式下的单次定位有效，其他方式无效
		 */
		locationOption.setGpsFirst(true);
		// 设置发送定位请求的时间间隔,最小值为1000，如果小于1000，按照1000算
		locationOption.setInterval(POST_DETAIL_TIME);
		// 设置定位参数
		locationClient.setLocationOption(locationOption);
	}

	private void startLocation() {
		// 启动定位
		locationClient.startLocation();
	}

	private void stopLocation() {
		if (null != locationClient) {
			/**
			 * 如果AMapLocationClient是在当前Activity实例化的，
			 * 在Activity的onDestroy中一定要执行AMapLocationClient的onDestroy
			 */
			locationClient.onDestroy();
			locationClient = null;
			locationOption = null;
		}
	}

	@Override
	public void onLocationChanged(AMapLocation loc) {
		if (null != loc) {
			Message msg = mHandler.obtainMessage();
			msg.obj = loc;
			msg.what = Utils.MSG_LOCATION_FINISH;
			mHandler.sendMessage(msg);
		}
	}

	Handler mHandler = new Handler() {
		public void dispatchMessage(Message msg) {
			Intent data = new Intent(Constant.UPDATE_LOCATION_ACTION);
			switch (msg.what) {
			// 开始定位
			case Utils.MSG_LOCATION_START:
				data.putExtra(Constant.LOCATION_STATUS, Constant.LOCATION_START);
				break;
			// 定位完成
			case Utils.MSG_LOCATION_FINISH:
				AMapLocation loc = (AMapLocation) msg.obj;
				if (loc != null) {
					if (lastLocation == null) {
						lastLocation = loc;
					} else {
						// 计算
						getDistance(lastLocation,loc);
					}
					data.putExtra(Constant.LOCATION_STATUS, Constant.LOCATION_SUCESS);
					Bundle bundle = new Bundle();
					//String result = Utils.getLocationStr(loc);
					//bundle.putParcelable("AMapLocation", loc);
					//bundle.putString("result", result);
					bundle.putFloat("lastDistance", BaseApplication.lastDistance);
					bundle.putFloat("singleDistance", singleDistance);
					bundle.putFloat("Speed", loc.getSpeed());
					data.putExtra(Constant.LOCATION_DATA, bundle);
				}
				break;
			// 停止定位
			case Utils.MSG_LOCATION_STOP:
				data.putExtra(Constant.LOCATION_STATUS, Constant.LOCATION_END);
				break;
			default:
				break;
			}
			
			sendBroadcast(data);
		};
	};
	
	
	
	
	
	
	private void getDistance(AMapLocation start,AMapLocation end){
		if(start==null || null==end){
			return;
		}
		
		if(start.getLatitude()==0 || start.getLongitude()==0 ||
				end.getLatitude()==0 || end.getLongitude()==0){
			return;
		}
		
		if(end.getAccuracy()>BaseApplication.locationAccuracy){
			return;
		}
		
		LatLng startLatlng = new LatLng(start.getLatitude(),start.getLongitude());
		LatLng endLatlng = new LatLng(end.getLatitude(),end.getLongitude());
		
		if(start.getSpeed()>0 && end.getSpeed()>0){
			singleDistance = AMapUtils.calculateLineDistance(startLatlng, endLatlng);
		}else{
			singleDistance = 0;
		}
		
		if(singleDistance>0){
			BaseApplication.lastDistance = BaseApplication.lastDistance+singleDistance;
		}
		lastLocation = end;
	}
	
}
