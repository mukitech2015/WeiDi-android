package com.junen.vtaxi.database;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DBManager {
	private DBHelper helper;
	private SQLiteDatabase db;

	public DBManager(Context context) {
		helper = new DBHelper(context);
		// 因为getWritableDatabase内部调用了mContext.openOrCreateDatabase(mName, 0,
		// mFactory);
		// 所以要确保context已初始化,我们可以把实例化DBManager的步骤放在Activity的onCreate里
		db = helper.getWritableDatabase();
	}

	/**
	 * add persons
	 * 
	 * @param persons
	 */
	public void add(List<WdLocation> locations) {
		db.beginTransaction(); // 开始事务
		try {
			for (WdLocation loc : locations) {
				db.execSQL("INSERT INTO loc VALUES(null, ?,?, ?, ?, ?, ?, ?, ?,?)",
						new Object[] { loc.type,loc.lat, loc.lng, loc.accuracy, loc.speed, loc.distance, loc.wait, loc.total,loc.orderId });
			}
			db.setTransactionSuccessful(); // 设置事务成功完成
		} finally {
			db.endTransaction(); // 结束事务
		}
	}

	/**
	 * update person's age
	 * 
	 * @param person
	 */
	public void updateAge(WdLocation loc) {
		/*
		ContentValues cv = new ContentValues();
		cv.put("age", person.age);
		db.update("person", cv, "name = ?", new String[] { person.name });
		*/
	}

	/**
	 * delete old person
	 * 
	 * @param person
	 */
	public void deleteOldPerson(WdLocation loc) {
		/*
		db.delete("person", "age >= ?",
				new String[] { String.valueOf(person.age) });
				*/
	}
	
	public void clearLocation(){
		db.beginTransaction(); // 开始事务
		try {
			db.execSQL("delete from loc");
			db.setTransactionSuccessful(); // 设置事务成功完成
		} finally {
			db.endTransaction(); // 结束事务
		}
	}

	/**
	 * query all persons, return list
	 * 
	 * @return List<Person>
	 */
	public List<WdLocation> query() {
		ArrayList<WdLocation> persons = new ArrayList<WdLocation>();
		Cursor c = queryTheCursor();
		while (c.moveToNext()) {
			WdLocation loc = new WdLocation();
			loc._id = c.getInt(c.getColumnIndex("_id"));
			loc.type = c.getInt(c.getColumnIndex("type"));
			loc.lat = c.getInt(c.getColumnIndex("lat"));
			loc.lng = c.getInt(c.getColumnIndex("lng"));
			loc.accuracy = c.getInt(c.getColumnIndex("accuracy"));
			loc.speed = c.getInt(c.getColumnIndex("speed"));
			loc.distance = c.getInt(c.getColumnIndex("distance"));
			loc.wait = c.getInt(c.getColumnIndex("wait"));
			loc.total = c.getInt(c.getColumnIndex("total"));
			loc.orderId = c.getString(c.getColumnIndex("orderid"));
			persons.add(loc);
		}
		c.close();
		return persons;
	}

	public WdLocation queryStartLocation(String orderId){
		String sql = "select * from loc where orderId=? and type=0";  
	    Cursor c = db.rawQuery(sql,new String[]{orderId});
	    WdLocation loc = new WdLocation();
	    if(c.moveToNext()){
	    	loc._id = c.getInt(c.getColumnIndex("_id"));
			loc.type = c.getInt(c.getColumnIndex("type"));
			loc.lat = c.getInt(c.getColumnIndex("lat"));
			loc.lng = c.getInt(c.getColumnIndex("lng"));
			loc.accuracy = c.getInt(c.getColumnIndex("accuracy"));
			loc.speed = c.getInt(c.getColumnIndex("speed"));
			loc.distance = c.getInt(c.getColumnIndex("distance"));
			loc.wait = c.getInt(c.getColumnIndex("wait"));
			loc.total = c.getInt(c.getColumnIndex("total"));
			loc.orderId = c.getString(c.getColumnIndex("orderid"));
	    }
	    c.close();
	    
	    return loc;
	}
	
	public void queryEndLocation(){
		
	}
	
	/**
	 * query all persons, return cursor
	 * 
	 * @return Cursor
	 */
	public Cursor queryTheCursor() {
		Cursor c = db.rawQuery("SELECT * FROM loc", null);
		return c;
	}

	/**
	 * close database
	 */
	public void closeDB() {
		db.close();
	}
}
