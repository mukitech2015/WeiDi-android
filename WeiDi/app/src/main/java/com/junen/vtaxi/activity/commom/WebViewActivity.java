package com.junen.vtaxi.activity.commom;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.junen.vtaxi.R;
import com.junen.vtaxi.activity.base.BaseActivity;
import com.junen.vtaxi.app.BaseApplication;
import com.junen.vtaxi.common.Constant;
import com.junen.vtaxi.http.ApiClient;
import com.junen.vtaxi.model.MessageRusult;
import com.junen.vtaxi.rpc.RpcExcutor;
import com.junen.vtaxi.rpc.api.RpcApi;

/**
 * Created by new on 16/7/8.
 */
public class WebViewActivity extends BaseActivity {
    private String title;
    private String url;


    private TextView titleView,errorView;
    private WebView webView;
    private ProgressBar progressBar;

    private RpcExcutor<String> readExcutor;
    private String messageId;
    private boolean isSuccess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        title = getIntent().getStringExtra(Constant.WEBVIEW_TITLE_KEY);
        url = getIntent().getStringExtra(Constant.WEBVIEW_URL_KEY);
        messageId = getIntent().getStringExtra(Constant.MESSAGE_ID_KEY);
        BaseApplication.readMessageId = messageId;
        initView();
        initData();
    }

    @Override
    public void initView() {
        super.initView();
        Button backView = (Button)findViewById(R.id.close_view);
        backView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                back();
            }
        });

        titleView = (TextView)findViewById(R.id.top_layout_view);
        errorView = (TextView)findViewById(R.id.error_view);
        webView = (WebView)findViewById(R.id.webview);
        progressBar = (ProgressBar) findViewById(R.id.pb_veiw);
        progressBar.setMax(100);
    }

    @Override
    public void initData() {
        super.initData();
        initRpcExcutor();
        if(TextUtils.isEmpty(title)){
            title = "微的";
        }
        titleView.setText("加载中");

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        webSettings.setBuiltInZoomControls(false);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setSupportZoom(true);
        webSettings.setSupportMultipleWindows(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setPluginState(WebSettings.PluginState.ON);

        webView.setWebViewClient(new WebViewClient() {

            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                super.onReceivedError(view, errorCode, description, failingUrl);
                errorView.setVisibility(View.VISIBLE);
                webView.setVisibility(View.GONE);
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                setUrl(url);
                if (!TextUtils.isEmpty(url)) {
                    if (url.startsWith("http://") || url.startsWith("https://")) {
                        errorView.setVisibility(View.GONE);
                        webView.setVisibility(View.VISIBLE);
                        view.loadUrl(url);
                    } else {
                        titleView.setText(title);
                        errorView.setVisibility(View.VISIBLE);
                        webView.setVisibility(View.GONE);
                    }
                    return true;
                }

                return false;
            }

        });

        webView.setWebChromeClient(new WebChromeClient() {

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                progressBar.setProgress(newProgress);
                if (newProgress == 100 || newProgress == 0) {
                    setTitle(view);
                    progressBar.setVisibility(View.GONE);
                    if(newProgress==100){
                        isSuccess = true;
                        if(!TextUtils.isEmpty(messageId)){
                            readExcutor.start();
                        }
                    }
                } else {
                    titleView.setText("加载中");
                    progressBar.setVisibility(View.VISIBLE);
                }
                super.onProgressChanged(view, newProgress);
            }

        });

        errorView.setVisibility(View.GONE);
        webView.setVisibility(View.VISIBLE);
        webView.loadUrl(url);
    }


    @Override
    public void back() {
        super.back();
        if(webView!=null && webView.canGoBack()){
            webView.goBack();
            return;
        }
        BaseApplication.readMessageId = messageId;
        setResult(RESULT_OK);
        finish();
    }

    /*
         * 设置Title
         */
    private void setTitle(WebView webview){
        if(webview!=null && webview.canGoBack()){
            titleView.setText(webview.getTitle());
        }else{
            titleView.setText(title);
        }
    }

    private void setUrl(String url){
        this.url = url;
    }


    protected void initRpcExcutor() {
        readExcutor = new RpcExcutor<String>(this, 0, false) {
            @Override
            public void excute(Object... params) {
                ApiClient.createApi(WebViewActivity.this, RpcApi.class).readMessage(12,
                        BaseApplication.getInstance().getDriverId(),messageId,this);
            }

            @Override
            public void onRpcFinish(String result, Object... params) {

            }

            @Override
            public void onRpcException(int code, String msg, Object... params) {
                toast(msg, Toast.LENGTH_SHORT);
            }

        };
        readExcutor.setShowProgressDialog(true);
        readExcutor.setShowNetworkErrorView(false);
    }
}
