package com.junen.vtaxi.activity.commom;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.igexin.sdk.PushManager;
import com.junen.vtaxi.R;
import com.junen.vtaxi.activity.account.ContactUsActivity;
import com.junen.vtaxi.activity.base.BaseActivity;
import com.junen.vtaxi.app.BaseApplication;
import com.junen.vtaxi.engine.AccountEngine;
import com.junen.vtaxi.http.ApiClient;
import com.junen.vtaxi.model.ApkInfo;
import com.junen.vtaxi.model.LoginResult;
import com.junen.vtaxi.rpc.RpcExcutor;
import com.junen.vtaxi.rpc.api.RpcApi;

public class LoginActivity extends BaseActivity implements View.OnClickListener{
	EditText nameView;
	EditText pwdView;
	
	private String userName;
	
	/** 查询城市 */
	private RpcExcutor<LoginResult> loginExcutor;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		initPush();
		initView();
		initData();
	}
	private void initPush(){
		PushManager.getInstance().initialize(this.getApplicationContext());
		String cid = PushManager.getInstance().getClientid(this);
		Log.e("","cid:"+cid);
		System.out.println("cid:"+cid);
	}
	@Override
	public void initView() {
		super.initView();
		Button loginView = (Button)findViewById(R.id.login_view);
		loginView.setOnClickListener(this);
		TextView contactUsView = (TextView)findViewById(R.id.contact_us_view);
		contactUsView.setOnClickListener(this);
		nameView = (EditText)findViewById(R.id.user_name_view);
		pwdView = (EditText)findViewById(R.id.user_password_view);
	}

	@Override
	public void initData() {
		super.initData();
		initRpcExcutor();
		AccountEngine.clearLoginInfo();
		BaseApplication.getInstance().setDriverId(null);
		userName = AccountEngine.getLoginAccount();
		if(!TextUtils.isEmpty(userName)){
			nameView.setText(userName);
		}
	}

	protected void initRpcExcutor() {
		loginExcutor = new RpcExcutor<LoginResult>(this, 0, false) {
			@Override
			public void excute(Object... params) {
				userName = (String) params[0];
				String userPwd = (String) params[1];
				ApiClient.createApi(LoginActivity.this, RpcApi.class)
						.login(1,userName,userPwd,1,
								BaseApplication.mac,
								BaseApplication.appVersion,this);
			}

			@Override
			public void onRpcFinish(LoginResult result, Object... params) {
				if(result==null || null==result.info){
					toast("网络异常，请重试", Toast.LENGTH_LONG);
					return;
				}
				AccountEngine.saveLoginInfo(result);
				AccountEngine.saveLoginAccount(userName);
				if(AccountEngine.checkAppUpdate(LoginActivity.this, result.apkinfo)){
					final ApkInfo apkInfo = result.apkinfo;
					customAlert("检测到新版本，请更新", 
							"更新", new View.OnClickListener() {
								
								@Override
								public void onClick(View v) {
									dismissAlertDialog();
									AccountEngine.updateApp(LoginActivity.this, apkInfo);
									launcher();
								}
							},
							"取消", new View.OnClickListener() {
								
								@Override
								public void onClick(View v) {
									dismissAlertDialog();
									launcher();
								}
							}, false);
				}else{
					launcher();
				}
				
			}

			@Override
			public void onRpcException(int code, String msg, Object... params) {
				toast(msg, Toast.LENGTH_SHORT);
			}

		};
		loginExcutor.setShowProgressDialog(true);
		loginExcutor.setShowNetworkErrorView(false);
	}

	@Override
	public void onClick(View v) {
		int id = v.getId();
		switch(id){
		case R.id.login_view:
			login();
			break;
		case R.id.contact_us_view:
			startActivity(new Intent(LoginActivity.this, ContactUsActivity.class));
			break;
		}
	}
	
	private void launcher(){
		startActivity(new Intent(LoginActivity.this, LauncherActivity.class));
		finish();
	}
	
	private void login(){
		String userName = nameView.getText().toString();
		if(TextUtils.isEmpty(userName)){
			toast("手机号不能为空", Toast.LENGTH_LONG);
			return;
		}
		if(11!=userName.length()){
			toast("手机号格式不正确", Toast.LENGTH_LONG);
			return;
		}
		String userPwd = pwdView.getText().toString();
		if(TextUtils.isEmpty(userPwd)){
			toast("密码不能为空", Toast.LENGTH_LONG);
			return;
		}
		
		loginExcutor.start(userName,userPwd);
	}
}
