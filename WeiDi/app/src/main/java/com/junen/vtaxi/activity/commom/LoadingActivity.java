package com.junen.vtaxi.activity.commom;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import com.junen.vtaxi.R;
import com.junen.vtaxi.activity.base.BaseActivity;
import com.junen.vtaxi.app.BaseApplication;
import com.junen.vtaxi.common.Constant;
import com.junen.vtaxi.service.DownloadService;

public class LoadingActivity extends BaseActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_loading);
		if (!isTaskRoot()) {
			finish();
			return;
		}
		initView();
		initData();
	}

	@Override
	public void initView() {
		super.initView();
		LinearLayout rootView = (LinearLayout)findViewById(R.id.root_layout_view);
		rootView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				//startActivity(new Intent(LoadingActivity.this,LoginActivity.class));
				//finish();
			}
		});
	}

	@Override
	public void initData() {
		super.initData();
		new Handler().postDelayed(new Runnable(){
			@Override
			public void run() {
				BaseApplication.getInstance().destroyActivitys();
				startActivity(new Intent(LoadingActivity.this,LoginActivity.class));
				finish();
			}
			
		}, 3000);
	}

	public void updateApp(Context context, String url) {
		String systems = context.getResources().getString(R.string.app_name);
		String apkName = "";
		if (null != url && url.contains(".apk")) {
			int start = url.lastIndexOf("/");
			int length = url.length();
			apkName = url.substring(start + 1, length);
		}
		Intent intent = new Intent(context, DownloadService.class);
		intent.setAction("com.zxw.weidi.download.service");
		intent.setPackage(context.getPackageName());
		intent.putExtra(Constant.DOWNLOAD_TYPE, 0);
		intent.putExtra(Constant.DOWNLOAD_TIP, systems);
		intent.putExtra(Constant.DOWNLOAD_FILE, apkName);
		url = url.replace(" ", "");
		intent.putExtra(Constant.DOWNLOAD_URL, url);
		intent.putExtra(Constant.DOWNLOAD_PACKAGE, "dwb");
		context.startService(intent);
	}
	
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

}
