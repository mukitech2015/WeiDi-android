package com.junen.vtaxi.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.widget.RemoteViews;

import com.junen.vtaxi.R;
import com.junen.vtaxi.common.Constant;
import com.junen.vtaxi.util.SdUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;

public class DownloadService extends Service {
	private final static int mNotificationId = 5;
	private String downloadTip;//下载提示
	private String downloadFile;// 下载文件名称，例如：boxpay.apk
	private String downloadUrl;// 下载地址，例如：http://115.238.178.70:8023/appupdate/
	private boolean isSdCard = true;// 是否存储在SD卡
	// 文件存储
	private String packageName;// 存储文件夹路径，例如："/hefutong"
	private File downloadPackage = null;// 存储路径
	// 通知栏
	private NotificationManager downloadNotifManager = null;
	private Notification downloadNotification = null;
	// 通知栏跳转Intent
	private Intent downloadIntent = null;
	private PendingIntent downloadPendingIntent = null;
	// 下载状态
	private final static int DOWNLOAD_COMPLETE = 0;
	private final static int DOWNLOAD_FAIL = 1;

	//下载总长度
	private long totalSize = 0;
	//已下载长度
	private int downloadSize = 0;
	//进度百分比
	private int progressNum = 0;
	private FileOutputStream outputStream = null;

	private boolean isStarted = false;//是否已经启动
	//0-无下载进度，无安装提示，1－下载进度，2-安装提示，3-安装提示＋下载进度
	private int downloadType = 0;
	private  String   downFileType;
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if(intent==null){
			return 0;
		}
		if(isStarted){
			return 0;
		}
		isStarted = true;
		downloadType = intent.getIntExtra(Constant.DOWNLOAD_TYPE, 0);
		downloadTip = intent.getStringExtra(Constant.DOWNLOAD_TIP);
		downloadFile = intent.getStringExtra(Constant.DOWNLOAD_FILE);
		downloadUrl = intent.getStringExtra(Constant.DOWNLOAD_URL);
		packageName = intent.getStringExtra(Constant.DOWNLOAD_PACKAGE);
		downFileType=intent.getStringExtra(Constant.DOWNLOAD_FILE_TYPE);
		Log.e("downFileType===========",downFileType);
		initData();
		initNotification();
		// 开启一个新的线程下载，如果使用Service同步下载，会导致ANR问题，Service本身也会阻塞
		new Thread(new DownloadRunnable()).start();// 这个是下载的重点，是下载的过程
		//Toast.makeText(getApplicationContext(), ""+downloadFile+"开始下载", Toast.LENGTH_LONG).show();
		return super.onStartCommand(intent, flags, startId);
	}
	/*
	 * 初始化数据
	 */
	private void initData(){
		if(!TextUtils.isEmpty(downloadUrl)){
			int length = downloadUrl.length();
			int sLength = downloadUrl.lastIndexOf(".");
			downloadFile = downloadFile+"."+downloadUrl.substring(sLength+1, length);
			Log.e("downloadFile", downloadFile);
		}
		totalSize = 0;
		downloadSize = 0;
		progressNum = 0;
		isSdCard = SdUtil.getStatusSD();
	}
	/*
	 * 初始化通知栏下载进度
	 */
	private void initNotification(){
		if(0==downloadType || 2==downloadType){
			return;
		}
		this.downloadNotifManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		this.downloadNotification = new Notification();
		downloadNotification.flags=Notification.FLAG_AUTO_CANCEL;
		// 设置下载过程中，点击通知栏，回到主界面
		//updateIntent = new Intent(this, LoginActivity.class);
		downloadIntent = new Intent();
		downloadPendingIntent = PendingIntent.getActivity(this, 0, downloadIntent,0);
		// 设置通知栏显示内容
		downloadNotification.icon = R.drawable.ic_launcher;
		downloadNotification.tickerText = downloadTip;
		downloadNotification.contentView = new RemoteViews(this.getPackageName(),
				R.layout.update_notification);
		downloadNotification.contentIntent = downloadPendingIntent;
		downloadNotification.contentView.setTextViewText(R.id.notification_text,
				downloadTip + "升级通知");
		downloadNotification.contentView.setProgressBar(
				R.id.notification_progress, 100, progressNum, false);
		// 发出通知
		downloadNotifManager.notify(mNotificationId, downloadNotification);
	}
	
	/*
	 * 初始化，点击安装apk
	 */
	private void initInstall(){
		if(0==downloadType || 1==downloadType){
			return;
		}
		// 点击安装PendingIntent
		Uri uri = Uri.fromFile(downloadPackage);
		Intent installIntent = new Intent(Intent.ACTION_VIEW);
		installIntent.setDataAndType(uri,"application/vnd.android.package-archive");
		if(downloadPendingIntent!=null){
			downloadPendingIntent = PendingIntent.getActivity(
					DownloadService.this, 0, installIntent, 0);
			downloadNotification.contentView.setTextViewText(
					R.id.notification_text, downloadTip + ",下载完成,点击安装");
			downloadNotification.contentView.setProgressBar(
					R.id.notification_progress, 100, progressNum, false);
			downloadNotification.contentIntent = downloadPendingIntent;

			downloadNotifManager.notify(mNotificationId,
					downloadNotification);
		}
		//安装apk
		installIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(installIntent);
	}
	
	/*
	 * 关闭下载服务
	 */
	private void stopDownloadService(){
		try {
			stopService(downloadIntent);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * 下载失败通知栏
	 */
	private void downloadFial(){
		if(0==downloadType || 2==downloadType ||downloadNotification==null){
			return;
		}
		// 下载失败
		downloadNotification.defaults = Notification.DEFAULT_VIBRATE;
		downloadNotification.contentView.setTextViewText(
				R.id.notification_text, downloadTip + ",下载失败");
		downloadNotification.contentView.setProgressBar(
				R.id.notification_progress, 100, progressNum, false);
		downloadNotifManager.notify(mNotificationId,
				downloadNotification);
	}
	
	/*
	 * 广播下载进度
	 * status,0-下载中，1-下载失败,2-下载成功
	 */
	private void broadcastDownloadProgress(int status){
		Intent intent = new Intent();
		intent.setAction(Constant.PROGRESS_ACTION);
		intent.putExtra(Constant.PROGRESS_STATUS_KEY, status);
		intent.putExtra(Constant.PROGRESSBAR_PROGRESS_KEY, progressNum);
		intent.putExtra(Constant.DOWNLOAD_URL, downloadUrl);
		sendBroadcast(intent);
	}
	/*
	 * 更新通知栏进度条
	 */
	private void updateNotification(){
		if(0==downloadType || 2==downloadType ||downloadNotification==null){
			return;
		}
		DecimalFormat format = (DecimalFormat) NumberFormat.getInstance();
		format.applyPattern("0.00");
		String totleS = format.format(totalSize / 1024.0 / 1024.0);
		String updateS = format
				.format(downloadSize / 1024.0 / 1024.0);
		downloadNotification.contentView.setTextViewText(
				R.id.notification_text, downloadFile + ", "
						+ (int) totalSize * 100 / downloadSize
						+ "%" + "(" + totleS + "M/" + updateS
						+ "M)");
		downloadNotification.contentView
				.setProgressBar(R.id.notification_progress, 100,
						progressNum, false);
		downloadNotifManager.notify(mNotificationId,
				downloadNotification);
	}
	
	private Handler downloadHandler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case DOWNLOAD_COMPLETE:
				isStarted = false;
				initInstall();
				openNotification();
				setDownloadVideoMessage();
			    stopDownloadService();
				break;
			case DOWNLOAD_FAIL:
				isStarted = false;
				downloadFial();
				openNotification();
				Log.e("downFileType",downFileType);
				setDownloadVideoMessage();
				stopDownloadService();
				broadcastDownloadProgress(1);
				break;
			default:
				isStarted = false;
				stopDownloadService();
			}
		}

	};
	class DownloadRunnable implements Runnable {
		Message message = downloadHandler.obtainMessage();
		@Override
		public void run() {
			message.what = DOWNLOAD_COMPLETE;
			try {
				if (isSdCard) {
					// SD卡存储
					File downloadDir = new File(Environment.getExternalStorageDirectory(), packageName);
					downloadPackage = new File(downloadDir.getPath(), downloadFile);
					// 增加权限;
					if (!downloadDir.exists()) {
						downloadDir.mkdirs();
					}
					if (!downloadPackage.exists()) {
						downloadPackage.createNewFile();
					}
					outputStream = new FileOutputStream(downloadPackage, false);
				} else {
					// Data存储
					outputStream = openFileOutput(downloadFile,
							Context.MODE_WORLD_READABLE);
					downloadPackage = getDir(downloadFile, Context.MODE_WORLD_READABLE);
				}
				// 下载函数
				// 增加权限;
				long downloadSize = downloadUpdateFile(downloadUrl,downloadPackage, outputStream);
				Log.e("downloadSize",downloadSize+"");
				if (downloadSize > 0 && (totalSize == downloadSize)) {
					// 下载成功
					downloadHandler.sendMessage(message);
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (Exception ex) {
				ex.printStackTrace();
				message.what = DOWNLOAD_FAIL;
				// 下载失败
				downloadHandler.sendMessage(message);
			}
		}
	}

	/*
	 * 下载
	 */
	@SuppressWarnings("resource")
	public long downloadUpdateFile(String downloadUrl, File saveFile,
			FileOutputStream fileOutputStream) throws Exception {
		int downloadCount = 0;
		int currentSize = 0;
		HttpURLConnection httpConnection = null;
		InputStream is = null;
		FileOutputStream fos = null;
		try {
			URL url = new URL(downloadUrl);
			httpConnection = (HttpURLConnection) url.openConnection();
			httpConnection.setRequestProperty("User-Agent", "PacificHttpClient");
			if (currentSize > 0) {
				httpConnection.setRequestProperty("RANGE", "bytes="
						+ currentSize + "-");
			}
			httpConnection.setConnectTimeout(10000);
			httpConnection.setReadTimeout(20000);
			downloadSize = httpConnection.getContentLength();
			if (httpConnection.getResponseCode() == 404) {
				throw new Exception("fail!");
			}
			is = httpConnection.getInputStream();
			if (fileOutputStream != null) {
				outputStream = fileOutputStream;
				fos = outputStream;
			} else {
				fos = new FileOutputStream(saveFile, false);
			}
			byte buffer[] = new byte[4096];
			int readsize = 0;
			while ((readsize = is.read(buffer)) > 0) {
				fos.write(buffer, 0, readsize);
				totalSize += readsize;
				// 为了防止频繁的通知导致应用吃紧，百分比增加10才通知一次
				double d = (totalSize*0.1)/(downloadSize*0.1);
				if ((downloadCount == 0)
						|| (int) (d*100) - 1 > downloadCount) {
					downloadCount += 1;
					progressNum = (int)(d*100);
					broadcastDownloadProgress(0);
					updateNotification();
				}
			}
		} finally {
			if (httpConnection != null) {
				httpConnection.disconnect();
			}
			if (is != null) {
				is.close();
			}

			if (fos != null) {
				fos.close();
			}
		}

		return totalSize;
	}

	public void cleanUpdateFile() {
		if(isSdCard){
			if(downloadPackage.exists()){
				downloadPackage.delete();
			}
		}else{
			deleteFile(downloadFile);
		}
	}

	@Override
	public void onDestroy() {
		isStarted = false;
		cleanNotification();
		super.onDestroy();
	}

	/*
	 * 打开通知栏
	 */
	public void openNotification() {
		if(0==downloadType || 2==downloadType ||downloadNotification==null){
			return;
		}
		try {
			int currentApiVersion = android.os.Build.VERSION.SDK_INT;  
			Object service = getSystemService("statusbar");
			Class<?> statusBarManager = Class
					.forName("android.app.StatusBarManager");
			Method expand = null;  
            if (service != null) {  
                if (currentApiVersion <= 16) {  
                    expand = statusBarManager.getMethod("expand");  
                } else {  
                    expand = statusBarManager  
                            .getMethod("expandNotificationsPanel");  
                }  
                expand.setAccessible(true);  
                expand.invoke(service);  
            }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * 清除通知
	 */
	public void cleanNotification() {
		if(0==downloadType || 2==downloadType ||downloadNotification==null){
			return;
		}
		downloadNotifManager.cancel(mNotificationId);
	}
	/*
	 * 下载成功提示
	 */
	public void setDownloadVideoMessage(){
		Intent intent = new Intent();
		intent.setAction(Constant.VIDEO_COMPETE);
		intent.putExtra(Constant.PROGRESSBAR_PROGRESS_KEY, progressNum);
		intent.putExtra(Constant.DOWNLOAD_URL, downloadUrl);
		intent.putExtra(Constant.DOWNLOAD_SD_PATH, downloadPackage.getAbsolutePath());
		sendBroadcast(intent);
	}
}




