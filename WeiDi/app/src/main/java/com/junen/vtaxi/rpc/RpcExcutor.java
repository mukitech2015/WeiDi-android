package com.junen.vtaxi.rpc;

import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import org.androidannotations.api.BackgroundExecutor;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.HttpHostConnectException;

import retrofit.Callback;
import retrofit.RetrofitError;
import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ScrollView;

import com.junen.vtaxi.R;
import com.junen.vtaxi.activity.base.ActivityResponsable;
import com.junen.vtaxi.common.Constant;
import com.junen.vtaxi.common.Strings;
import com.junen.vtaxi.http.ApiClient;
import com.junen.vtaxi.http.HttpException;
import com.junen.vtaxi.rpc.RpcException.ErrorCode;
import com.junen.vtaxi.rpc.api.RpcApi;
import com.junen.vtaxi.widget.FlowTipView;

/**
 * 扩展RPC服务，提供RPC代理，统一处理网络异常显示提示，菊花统一处理 默认显示菊花和网络错误页面 <br />
 * 不需要菊花可以设置 showProgressDialog=false <br />
 * 不需要网络错误页面可以设置 showNetworkErrorView=false
 * 
 * 通过RpcExcutor自带start方法可以支持增加启动参数 通过Thread或线程池启动RpcExcutor将无法传参数
 * 
 * 
 */
public abstract class RpcExcutor<Result> implements Runnable, Callback<Result> {

	private static final String TAG = "RpcExcutor";

	/**
	 * 主线程Looper
	 */
	private Handler handler = new Handler(Looper.getMainLooper());

	/**
	 * Rpc请求的BaseActivity
	 */
	private Activity context;

	/**
	 * 转菊花界面
	 */
	private ActivityResponsable activityResponsable;

	/**
	 * 转菊花时显示的文案
	 */
	private String progressText = null;

	/**
	 * 是否转菊花，默认转菊花
	 */
	private boolean showProgressDialog = true;

	/**
	 * 是否显示网络错误重试视图，默认关闭
	 */
	private boolean showNetworkErrorView = false;

	/**
	 * 显示网络错误页面时 View与顶部的距离
	 */
	private int marginTop = 0;

	/**
	 * 显示网络错误页面时 View与顶底部的距离
	 */
	private int marginBottom = 0;

	/**
	 * 显示网络错误页面时 左边距
	 */
	private int marginLeft = 0;

	/**
	 * 显示网络错误页面时 右边距
	 */
	private int marginRight = 0;

	/**
	 * 界面RootView，网络错误页面放到RootView
	 */
	private FrameLayout rootView;

	/**
	 * 错误显示页面
	 */
	private FlowTipView flowTipView;

	/**
	 * 错误显示页面容器
	 */
	private ScrollView scrollView;

	private boolean running = false;

	public RpcApi rpcApi;

	/**
	 * 无Activity界面时无法显示网络错误和进度条
	 */
	public RpcExcutor(Context c) {
		this.showNetworkErrorView = false;
		Activity activity = context;
		if (activity != null && activity instanceof ActivityResponsable) {
			activityResponsable = (ActivityResponsable) activity;
		}
		if (rpcApi == null) {
			rpcApi = ApiClient.createApi(c, RpcApi.class);
		}
	}

	/**
	 * 在Activity内部，可以显示网络错误控件和进度条
	 * 
	 * @param context
	 * @param tipTopMargin
	 */
	public RpcExcutor(Activity context, int tipTopMargin) {
		init(context, tipTopMargin, true);
	}

	/**
	 * 在Activity内部，可以显示网络错误控件和进度条
	 * 
	 * @param context
	 * @param tipTopMargin
	 */
	public RpcExcutor(Activity context, int tipTopMargin, boolean initRpcApi) {
		init(context, tipTopMargin, initRpcApi);
	}

	/**
	 * 在Activity内部，可以显示网络错误控件和进度条
	 * 
	 * @param context
	 * @param titleBar
	 */
	public RpcExcutor(Activity context, View titleBar) {
		int measuredHeight = titleBar.getMeasuredHeight();
		if (measuredHeight <= 0) {
			// DeviceInfo deviceInfo = DeviceInfo.getInstance();
			// titleBar.measure(deviceInfo.getmScreenWidth(),
			// deviceInfo.getScreenHeight());
			measuredHeight = titleBar.getMeasuredHeight();
		}
		init(context, measuredHeight, true);
	}

	/**
	 * 初始化RpcExcutor
	 * 
	 * @param context
	 * @param tipTopMargin
	 */
	private void init(Activity context, int tipTopMargin, boolean initRpcApi) {
		this.context = context;
		this.marginTop = tipTopMargin;
		if (context instanceof ActivityResponsable) {
			activityResponsable = (ActivityResponsable) context;
		}
		if (rpcApi == null && initRpcApi) {
			rpcApi = ApiClient.createApi(context, RpcApi.class);
		}
	}

	/**
	 * 初始化视图对象
	 */
	private void initView() {
		if (context == null) {
			return;
		}
		if (flowTipView == null) {
			rootView = (FrameLayout) context.findViewById(android.R.id.content);
			if (rootView.getChildCount() > 0) {
				for (int i = 0; i < rootView.getChildCount(); i++) {
					View child = rootView.getChildAt(i);
					if (child instanceof ScrollView
							&& ((ViewGroup) child).getChildAt(0) instanceof FlowTipView) {
						scrollView = (ScrollView) child;
						break;
					}
				}
			}
			if (scrollView == null) {
				scrollView = (ScrollView) LayoutInflater.from(context).inflate(
						R.layout.ext_flow_tip_view, null);
				hideTipView();
				LayoutParams params = new LayoutParams(
						LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
				params.topMargin = marginTop;
				params.bottomMargin = marginBottom;
				params.leftMargin = marginLeft;
				params.rightMargin = marginRight;
				rootView.addView(scrollView, params);
			}
			flowTipView = (FlowTipView) scrollView.getChildAt(0);
		}
	}

	/**
	 * 隐藏TipView
	 */
	public void hideTipView() {
		if (scrollView != null) {
			scrollView.setVisibility(View.GONE);
		}
	}

	/**
	 * 启动RPC任务
	 * 
	 * @param params
	 *            启动参数
	 */
	public void start(final Object... params) {
		BackgroundExecutor.execute(new Runnable() {
			@Override
			public void run() {
				excuteRpcTask(params);
			}
		});
	}

	/**
	 * 执行RPC请求任务
	 * 
	 * @param action
	 */
	private void excuteRpcTask(Object... params) {
		try {
			running = true;
			if (showProgressDialog && activityResponsable != null) {
				activityResponsable.showProgressDialog(getProgressText());
			}
			excute(params);
		} catch (Exception e) {
			dismiss();
			try {
				onRpcException(HttpException.NETWORK_UNKNOWN_ERROR, e + "",
						params);
				//MobclickAgent.reportError(context, e);
			} catch (Exception e1) {
				Log.e(TAG, String.valueOf(e1));
			}
		}
	}

	/**
	 * 显示网络错误页面
	 */
	public void showNetworkErrorTip(final Object... params) {
		if (context == null) {
			return;
		}
		context.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				initView();
				flowTipView.resetFlowTipType(FlowTipView.TYPE_NETWORK_ERROR);
				flowTipView.setTips(context
						.getString(R.string.flow_network_error));
				flowTipView.setAction(context.getString(R.string.try_again),
						new OnClickListener() {

							@Override
							public void onClick(View v) {
								hideTipView();
								start(params);
							}
						});
				scrollView.setVisibility(View.VISIBLE);
			}
		});
	}

	/**
	 * 数据为空时提示界面
	 * 
	 * @param emptyTipStr
	 *            数据为空提示文案
	 * @param actionText
	 *            数据为时需要重试等操作时的按钮文案
	 * @param clickAction
	 *            需要重试等操作时按钮触发的事件
	 */
	public void showEmptyTip(final String emptyTipStr, final String actionText,
			final OnClickListener clickAction) {
		if (context == null) {
			return;
		}
		context.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				initView();
				flowTipView.resetFlowTipType(FlowTipView.TYPE_EMPTY);
				flowTipView.setTips(emptyTipStr);
				if (clickAction != null) {
					flowTipView.setAction(actionText, new OnClickListener() {

						@Override
						public void onClick(View v) {
							hideTipView();
							clickAction.onClick(v);
						}
					});
				} else {
					flowTipView.getActionButton().setVisibility(View.INVISIBLE);
				}
				scrollView.setVisibility(View.VISIBLE);
			}
		});
	}

	/**
	 * 显示警告文案和图标
	 * 
	 * @param warningTipStr
	 *            警告文案内容
	 * @param actionText
	 *            数据为时需要重试等操作时的按钮文案
	 * @param clickAction
	 *            需要重试等操作时按钮触发的事件
	 */
	public void showWarningTip(final String warningTipStr,
			final String actionText, final OnClickListener clickAction) {
		if (context == null) {
			return;
		}
		context.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				initView();
				flowTipView.resetFlowTipType(FlowTipView.TYPE_WARNING);
				flowTipView.setTips(warningTipStr);
				if (clickAction != null) {
					flowTipView.setAction(actionText, new OnClickListener() {

						@Override
						public void onClick(View v) {
							hideTipView();
							clickAction.onClick(v);
						}
					});
				} else {
					flowTipView.getActionButton().setVisibility(View.INVISIBLE);
				}
				scrollView.setVisibility(View.VISIBLE);
			}
		});
	}

	/**
	 * 执行RPC方法，如果有在excute方法里面有弹出对话框的操作需要延迟弹出
	 * 
	 * @param params
	 * @return RPC拿到的结果
	 */
	public abstract void excute(Object... params);

	/**
	 * RPC请求完成后执行,还是在工作线程里执行
	 * 
	 * @param result
	 * @param params
	 *            启动参数
	 */
	public void onRpcFinish(Result result, Object... params) {
		Log.d("onRpcFinish", "rpc request finish.");
	}

	/**
	 * 发生网络错误时回调
	 * 
	 * @param ex
	 */
	public void onNetworkError(int code, String msg, Object... params) {
	}

	/**
	 * 发生RPC异常时回调
	 * 
	 * @param ex
	 */
	public void onRpcException(int code, String msg, Object... params) {
		Log.w("ExtRpcAction", msg);
	}

	/**
	 * 执行Rpc任务 发生异常，可能是代码逻辑错误，需要关注
	 * 
	 * @param ex
	 */
	public void onException(Exception ex, Object... params) {
		Log.e(TAG, "ExtRpcAction", ex);
	}

	@Override
	public void run() {
		excuteRpcTask();
	}

	public String getProgressText() {
		return progressText;
	}

	public void setProgressText(String progressText) {
		this.progressText = progressText;
	}

	public boolean isShowProgressDialog() {
		return showProgressDialog;
	}

	public void setShowProgressDialog(boolean showProgressDialog) {
		this.showProgressDialog = showProgressDialog;
	}

	public boolean isShowNetworkErrorView() {
		return showNetworkErrorView;
	}

	public void setShowNetworkErrorView(boolean showNetworkErrorView) {
		this.showNetworkErrorView = showNetworkErrorView;
	}

	public void runOnUiThread(Runnable action) {
		runOnUiThreadDelay(action, -1);
	}

	public void runOnUiThreadDelay(Runnable action, long delayMillis) {
		if (action != null) {
			if (delayMillis > 0) {
				handler.postDelayed(action, delayMillis);
			} else {
				handler.post(action);
			}
		}
	}

	public boolean isRunning() {
		return running;
	}

	/**
	 * 设置提示页面上下左右边距
	 * 
	 * @param topMargin
	 * @param leftMargin
	 * @param bottomMargin
	 * @param rightMargin
	 */
	public void setTipMargin(int topMargin, int leftMargin, int bottomMargin,
			int rightMargin) {
		this.marginBottom = bottomMargin;
		this.marginLeft = leftMargin;
		this.marginRight = rightMargin;
		this.marginTop = topMargin;
	}

	/**
	 * 不显示进度条
	 */
	public void dismiss() {
		if (activityResponsable != null) {
			activityResponsable.dismissProgressDialog();
		}
		running = false;
	}

	@Override
	public void success(Result result, retrofit.client.Response response) {
		dismiss();
		if (result != null) {
			onRpcFinish(result);
		} else {
			onRpcException(Constant.SYSTEM_ERROR_CODE, Strings.DADA_ERROR_MSG);
		}
	}

	@Override
	public void failure(RetrofitError error) {
		dismiss();
		if (error == null || error.getCause() == null) {
			onRpcException(ErrorCode.CLIENT_UNKNOWN_ERROR,
					Strings.NETWORK_UNKNOWN_ERROR_MSG);
			return;
		}
		Throwable throwable = error.getCause();
		if (throwable instanceof ConnectTimeoutException
				|| throwable instanceof SocketTimeoutException
				|| throwable instanceof HttpHostConnectException
				|| throwable instanceof UnknownHostException
				|| throwable instanceof ConnectException
				|| throwable instanceof ConnectTimeoutException
				|| throwable instanceof SocketTimeoutException
				|| throwable instanceof UnknownHostException
				|| throwable instanceof SocketException) {
			onRpcException(ErrorCode.CLIENT_NETWORK_UNAVAILABLE_ERROR,
					Strings.NETWORK_ERROR_MSG);
			return;
		}
		if (throwable instanceof RpcException) {
			onRpcException(((RpcException) throwable).getCode(),
					((RpcException) throwable).getMsg());
			return;
		}
		onRpcException(ErrorCode.CLIENT_UNKNOWN_ERROR, Strings.DADA_ERROR_MSG);
	}

	/**
	 * 执行RPC请求任务
	 * 
	 * @param action
	 */
	private void excuteRpcTaskSync(Object... params) {
		Result result = null;
		try {
			running = true;
			if (showProgressDialog && activityResponsable != null) {
				activityResponsable.showProgressDialog(getProgressText());
			}
			result = excuteSync(params);
		} catch (RetrofitError error) {
			dismiss();
			if (error == null || error.getCause() == null) {
				onRpcException(ErrorCode.CLIENT_UNKNOWN_ERROR,
						Strings.NETWORK_UNKNOWN_ERROR_MSG, params);
				return;
			}
			Throwable throwable = error.getCause();
			if (throwable instanceof ConnectTimeoutException
					|| throwable instanceof SocketTimeoutException
					|| throwable instanceof HttpHostConnectException
					|| throwable instanceof UnknownHostException
					|| throwable instanceof ConnectException
					|| throwable instanceof ConnectTimeoutException
					|| throwable instanceof SocketTimeoutException
					|| throwable instanceof UnknownHostException
					|| throwable instanceof SocketException) {
				onRpcException(ErrorCode.CLIENT_NETWORK_UNAVAILABLE_ERROR,
						Strings.NETWORK_ERROR_MSG, params);
				return;
			}
			if (throwable instanceof RpcException) {
				onRpcException(((RpcException) throwable).getCode(),
						((RpcException) throwable).getMsg(), params);
				return;
			}
			onRpcException(ErrorCode.CLIENT_UNKNOWN_ERROR,
					Strings.DADA_ERROR_MSG, params);
			return;
		} catch (Exception e) {
			dismiss();
			try {
				onRpcException(HttpException.NETWORK_UNKNOWN_ERROR, e + "",
						params);
				//MobclickAgent.reportError(context, e);
			} catch (Exception e1) {
				Log.e(TAG, String.valueOf(e1));
			}
			return;

		}
		dismiss();
		running = false;
		try {
			if (result != null) {
				onRpcFinish(result, params);
			} else {
				onRpcException(Constant.SYSTEM_ERROR_CODE,
						Strings.DADA_ERROR_MSG, params);
			}
		} catch (Exception e2) {
		}
	}

	/**
	 * 启动RPC任务
	 * 
	 * @param params
	 *            启动参数
	 */
	public void startSync(final Object... params) {
		BackgroundExecutor.execute(new Runnable() {
			@Override
			public void run() {
				excuteRpcTaskSync(params);
			}
		});
	}

	/**
	 * 执行RPC方法，同步返回结果
	 * 
	 * @param params
	 * @return RPC拿到的结果
	 */
	public Result excuteSync(Object... params) {
		return null;
	};

}
