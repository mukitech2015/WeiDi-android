package com.junen.vtaxi.model;

import java.util.List;

/**
 * Created by root on 16-12-16.
 */

public class RecordResult {
    public int tag;
    public int sum;
    public List<Record> list;
}
