package com.junen.vtaxi.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.junen.vtaxi.R;

/**
 * 浮动信息提示界面
 * 
 * @author menglin.li
 * 
 */
public class FlowTipView extends LinearLayout {
	public static final int TYPE_NETWORK_ERROR = 0x10;
	public static final int TYPE_EMPTY = 0x11;
	public static final int TYPE_WARNING = 0x12;

	private Button mAction;
	private TextView mTips;
	private ImageView mIcon;

	private int mType;

	public FlowTipView(Context context) {
		super(context);
	}

	public FlowTipView(Context context, AttributeSet set) {
		super(context, set);

		LayoutInflater.from(context)
				.inflate(R.layout.flow_tip_view, this, true);

		TypedArray a = context.obtainStyledAttributes(set,
				R.styleable.FlowTipView);
		mType = a.getInt(R.styleable.FlowTipView_flow_tip_view_type,
				TYPE_NETWORK_ERROR);
		a.recycle();

		setOrientation(VERTICAL);
	}

	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		mAction = (Button) findViewById(R.id.action);
		mTips = (TextView) findViewById(R.id.tips);
		mIcon = (ImageView) findViewById(R.id.icon);
		resetFlowTipType(mType);
	}

	public void resetFlowTipType(int type) {
		mType = type;
		switch (mType) {
		case TYPE_NETWORK_ERROR:
			mIcon.setImageResource(R.drawable.flow_network_signals);
			break;
		case TYPE_EMPTY:
			mIcon.setImageResource(R.drawable.flow_empty);
			break;
		case TYPE_WARNING:
			mIcon.setImageResource(R.drawable.flow_warning);
			break;

		default:
			break;
		}
	}

	/**
	 * 设置按钮的属性
	 * 
	 * @param text
	 * @param clickListener
	 */
	public void setAction(String text, OnClickListener clickListener) {
		mAction.setText(text);
		mAction.setOnClickListener(clickListener);
		mAction.setVisibility(View.VISIBLE);
	}

	/**
	 * 取消按钮
	 */
	public void setNoAction() {
		mAction.setVisibility(View.GONE);
	}

	/**
	 * 设置提示信息
	 * 
	 * @param text
	 */
	public void setTips(String text) {
		mTips.setText(Html.fromHtml(text));
		mTips.setVisibility(View.VISIBLE);
	}

	public Button getActionButton() {
		return mAction;
	}
}
