package com.junen.vtaxi.activity.account;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.junen.vtaxi.R;
import com.junen.vtaxi.activity.base.BaseActivity;
import com.junen.vtaxi.activity.commom.RecordActivity;
import com.junen.vtaxi.app.BaseApplication;
import com.junen.vtaxi.common.Constant;
import com.junen.vtaxi.http.ApiClient;
import com.junen.vtaxi.model.Drivermoney;
import com.junen.vtaxi.rpc.RpcExcutor;
import com.junen.vtaxi.rpc.api.RpcApi;
import com.junen.vtaxi.widget.CustomDialog;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.json.JSONException;
import org.json.JSONObject;

public class UserInfoActivity extends BaseActivity {
	//图片下载
	private ImageLoader imageLoader;
	private DisplayImageOptions options;
	private RpcExcutor<String> drivermoneyRpcExcutor;
	private RpcExcutor<String>  stringRpcExcutor;
	TextView  sumView;
	TextView  ktxView;
	TextView  shView;
	Drivermoney  drivermoney;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_info);
		initView();
		initData();
	}

	@Override
	public void initView() {
		super.initView();
		Button backView = (Button)findViewById(R.id.close_view);
		backView.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		TextView  withdrawBtn= (TextView) findViewById(R.id.withdrawBtn);
		withdrawBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
              showWidthDrawDlg(drivermoney);
			}
		});
		TextView  recordBtn= (TextView) findViewById(R.id.recordBtn);
		recordBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
               goToActivity();
			}
		});
	}
	public  void  goToActivity(  ){
		Intent  intent=new Intent(this, RecordActivity.class);
		startActivity(intent);
	}
	public  void   initRpcExcutor(){
		stringRpcExcutor=new RpcExcutor<String>(this, 0, false) {
			@Override
			public void excute(Object... params) {
				double   tx= (double) params[0];
				ApiClient.createApi(UserInfoActivity.this, RpcApi.class).submitWithdraw(19, BaseApplication.getInstance().getDriverId(),tx, this);
			}
			@Override
			public void onRpcFinish(String s, Object... params) {
				toast("提现申请成功，请等待审核结算！", Toast.LENGTH_SHORT);
			}

			@Override
			public void onRpcException(int code, String msg, Object... params) {
				toast(msg, Toast.LENGTH_SHORT);
			}
		};

		drivermoneyRpcExcutor=new RpcExcutor<String>(this, 0, false) {
			@Override
			public void excute(Object... params) {
				ApiClient.createApi(UserInfoActivity.this, RpcApi.class).getUserMoney(18, BaseApplication
						.getInstance().getDriverId(), this);
			}
			@Override
			public void onRpcFinish(String string, Object... params) {
				try {
					JSONObject   jsonObject=new JSONObject(string);
					JSONObject   childObj=jsonObject.getJSONObject("drivermoney");
					Drivermoney   drivermoney=new Drivermoney();
					drivermoney.ktx=Double.parseDouble(childObj.getString("ktx"));
					drivermoney.sum=Double.parseDouble(childObj.getString("sum"));
					drivermoney.sh=Double.parseDouble(childObj.getString("sh"));
					UserInfoActivity.this.drivermoney=drivermoney;
					sumView.setText(drivermoney.sum+"");
					ktxView.setText(drivermoney.ktx+"");
					shView.setText(drivermoney.sh+"");
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			@Override
			public void onRpcException(int code, String msg, Object... params) {
				toast(msg, Toast.LENGTH_SHORT);
			}
		};
		drivermoneyRpcExcutor.setShowNetworkErrorView(false);
		drivermoneyRpcExcutor.setShowProgressDialog(false);
	}
	@Override
	public void initData() {
		super.initData();
		initImageLoader();
		ImageView headView = (ImageView)findViewById(R.id.user_head_view);
		TextView nameView = (TextView)findViewById(R.id.user_name_veiw);
		TextView sexView = (TextView)findViewById(R.id.sex_show_veiw);
		TextView phoneView = (TextView)findViewById(R.id.phone_show_veiw);
		TextView plateView = (TextView)findViewById(R.id.plate_number_show_veiw);
		TextView addressView = (TextView)findViewById(R.id.home_address_show_veiw);
		sumView= (TextView) findViewById(R.id.sum_show_veiw);
		ktxView= (TextView) findViewById(R.id.ktx_show_veiw);
		shView= (TextView) findViewById(R.id.sh_show_veiw);
		SharedPreferences shared = BaseApplication.getInstance().getSharedPreferences(
				Constant.LOGIN_RESULT_KEY, Context.MODE_PRIVATE);
		String headUrl = shared.getString(Constant.USER_INFO_LOGOURL_KEY, null);
		imageLoader.displayImage(headUrl, headView, options);
		nameView.setText(shared.getString(Constant.USER_INFO_NAME_KEY, ""));
		if(1==shared.getInt(Constant.USER_INFO_SEX_KEY, 0)){
			sexView.setText("女");
		}else{
			sexView.setText("男");
		}
		phoneView.setText(shared.getString(Constant.USER_INFO_PHONE_KEY, ""));
		plateView.setText(shared.getString(Constant.USER_INFO_CARNO_KEY, ""));
		addressView.setText(shared.getString(Constant.USER_INFO_ADDRESS_KEY, ""));
		initRpcExcutor();
		drivermoneyRpcExcutor.start();
	}

	private void initImageLoader(){
		// 初始化imageLoader 否则会报错
		imageLoader = ImageLoader.getInstance();
		imageLoader.init(ImageLoaderConfiguration.createDefault(this));
		options = new DisplayImageOptions.Builder()
		.cacheInMemory(false) // 设置下载的图片是否缓存在内存中
		.cacheOnDisc(false) // 设置下载的图片是否缓存在SD卡中
		.build();
	}

	public  void   showWidthDrawDlg(final Drivermoney  drivermoney){
		final CustomDialog dialog = new CustomDialog(this);
		dialog.show();
		dialog.setYeTV(drivermoney.ktx+"");
		dialog.setCancelBtn(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				dialog.dismiss();
			}
		});
		dialog.setConfirmBtn(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(TextUtils.isEmpty(dialog.getSqEt())){
					toast("请输入提现金额",Toast.LENGTH_SHORT);
					return;
			 	 }
				double tx=Double.parseDouble(dialog.getSqEt());
				if(tx==0){
				 toast("提现金额必须大于0",Toast.LENGTH_SHORT);
				 return;
			    }
			    if(tx>drivermoney.ktx){
				   toast("可提现余额不足，无法提现",Toast.LENGTH_SHORT);
				   return;
			    }
				stringRpcExcutor.start(tx);
				dialog.dismiss();
			}
		});
	}

}









