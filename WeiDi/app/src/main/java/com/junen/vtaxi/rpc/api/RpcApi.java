package com.junen.vtaxi.rpc.api;

import com.junen.vtaxi.model.AdvertResult;
import com.junen.vtaxi.model.BannerResult;
import com.junen.vtaxi.model.CreateOrderResult;
import com.junen.vtaxi.model.GpsResult;
import com.junen.vtaxi.model.HolidayResult;
import com.junen.vtaxi.model.LoginResult;
import com.junen.vtaxi.model.MessageRusult;
import com.junen.vtaxi.model.QueryCityResult;
import com.junen.vtaxi.model.RecordResult;
import com.junen.vtaxi.model.ShopResult;
import com.junen.vtaxi.model.SpeedWarnResult;
import com.junen.vtaxi.model.StopOrderResult;
import com.junen.vtaxi.model.ValuationResult;
import com.junen.vtaxi.model.VoiceResult;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

public interface RpcApi {
	@FormUrlEncoded
	@POST("/apis/common/get-city.json")
	public void getCity(@Field("lat") int lat, @Field("lng") int lng, @Field("systemCode") String systemCode, @Field
			("mobile") String mobile, @Field("locationType") int locationType, @Field("remoteSupported") boolean
			remoteSupported, Callback<QueryCityResult> callBack);
	
	//登录
	@FormUrlEncoded
	@POST("/vtaxi/InterfaceService?")
	public void login(@Field("tag") int tag, @Field("name") String name, @Field("pwd") String pwd, @Field("way") int
			way, @Field("mac") String mac, @Field("apkversion") String apkversion, Callback<LoginResult> callBack);
	
	//登出
	@FormUrlEncoded
	@POST("/vtaxi/InterfaceService?")
	public void logout(@Field("tag") int tag, @Field("id") String id, @Field("way") int way, @Field("mac") String mac,
					   Callback<String> callBack);
	
	//获取计价规则
	@FormUrlEncoded
	@POST("/vtaxi/InterfaceService?")
	public void getValuation(@Field("tag") int tag, @Field("id") String id, Callback<ValuationResult> callBack);

	//上报超速
	@FormUrlEncoded
	@POST("/vtaxi/InterfaceService?")
	public void postSpeedInfo(@Field("tag") int tag, @Field("id") String id, @Field("time") String time, @Field("speed") double speed, @Field("speedwarn") double speedwarn, Callback<String> callBack);
	//获取公司信息
	@FormUrlEncoded
	@POST("/vtaxi/InterfaceService?")
	public void getShopInfo(@Field("tag") int tag, Callback<ShopResult> callBack);
	
	//上报GPS
	@FormUrlEncoded
	@POST("/vtaxi/InterfaceService?")
	public void submitGPS(@Field("tag") int tag, @Field("id") String id, @Field("lng") String lng, @Field("lat")
	String lat, @Field("speed") String speed, @Field("precision") String precision, @Field("distanceid") String
			distanceid, @Field("type") int type,//1：正常状态上报 2：行程中上报
						  Callback<GpsResult> callBack);
	
	
	//获取滚动字幕-6
	@FormUrlEncoded
	@POST("/vtaxi/InterfaceService?")
	public void getPadBanner(@Field("tag") int tag, @Field("id") String id, Callback<BannerResult> callBack);
	
	//获取广告
	@FormUrlEncoded
	@POST("/vtaxi/InterfaceService?")
	public void getAdvertInfo(@Field("tag") int tag, @Field("id") String id, Callback<AdvertResult> callBack);

	//获取开始语音
	@FormUrlEncoded
	@POST("/vtaxi/InterfaceService?")
	public void getStartVoiceInfo(@Field("tag") int tag, @Field("id") String id, Callback<VoiceResult> callBack);

	@FormUrlEncoded
	@POST("/vtaxi/InterfaceService?")
	public void getEndVoiceInfo(@Field("tag") int tag, @Field("id") String id, Callback<VoiceResult> callBack);
	//创建行程
	@FormUrlEncoded
	@POST("/vtaxi/InterfaceService?")
	public void createOrder(@Field("tag") int tag, @Field("id") String id, @Field("type") int type,//1：按里程。2：按时间
							Callback<CreateOrderResult> callBack);

	//获取超速信息
	@FormUrlEncoded
	@POST("/vtaxi/InterfaceService?")
	public void createSpeedWarn(@Field("tag") int tag, @Field("id") String id,Callback<SpeedWarnResult> callBack);
	//上报行程
	@FormUrlEncoded
	@POST("/vtaxi/InterfaceService?")
	public void stopOrder(@Field("tag") int tag, @Field("id") String id, @Field("distanceid") String distanceid,
						  @Field("slng") String slng, @Field("slat") String slat, @Field("elng") String elng, @Field
									  ("elat") String elat, @Field("price") String price, @Field("miles") String
									  miles, @Field("sumtime") String sumtime, @Field("waittime") String waittime,
						  Callback<StopOrderResult> callBack);

	//获取节假日信息-10
	@FormUrlEncoded
	@POST("/vtaxi/InterfaceService?")
	public void getHolidayInfo(@Field("tag") int tag, @Field("id") String id,
							Callback<HolidayResult> callBack);

	//获取未读消息-11
	@FormUrlEncoded
	@POST("/vtaxi/InterfaceService?")
	public void getMessageList(@Field("tag") int tag, @Field("id") String id,
			@Field("page") int page,
			Callback<MessageRusult> callBack);

	@FormUrlEncoded
	@POST("/vtaxi/InterfaceService?")
	public void getUserMoney(@Field("tag") int tag, @Field("id") String id,Callback<String> callBack);


	@FormUrlEncoded
	@POST("/vtaxi/InterfaceService?")
	public void submitWithdraw(@Field("tag") int tag, @Field("id") String id,@Field("tx") double tx,Callback<String> callBack);


	//获取未读消息-11

	@FormUrlEncoded
	@POST("/vtaxi/InterfaceService?")
	public void getRecordList(@Field("tag") int tag, @Field("id") String id,
							   @Field("page") int page,
							   Callback<RecordResult> callBack);
	//登出-12
	@FormUrlEncoded
	@POST("/vtaxi/InterfaceService?")
	public void readMessage(@Field("tag") int tag, @Field("id") String id, @Field("msgid") String msgid,
					   Callback<String> callBack);
}
