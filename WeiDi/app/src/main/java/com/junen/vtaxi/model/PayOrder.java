package com.junen.vtaxi.model;

/**
 * Created by root on 16-12-17.
 */

public class PayOrder  {
   String  diatance;
   String  waitTime;
   String  useTime;
   String  initPrice;

    public String getInitPrice() {
        return initPrice;
    }

    public void setInitPrice(String initPrice) {
        this.initPrice = initPrice;
    }

    public String getDiatance() {
        return diatance;
    }

    public void setDiatance(String diatance) {
        this.diatance = diatance;
    }

    public String getWaitTime() {
        return waitTime;
    }

    public void setWaitTime(String waitTime) {
        this.waitTime = waitTime;
    }

    public String getUseTime() {
        return useTime;
    }

    public void setUseTime(String useTime) {
        this.useTime = useTime;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getSumPrice() {
        return sumPrice;
    }

    public void setSumPrice(String sumPrice) {
        this.sumPrice = sumPrice;
    }

    public String getPremiumTimePrice() {
        return PremiumTimePrice;
    }

    public void setPremiumTimePrice(String premiumTimePrice) {
        PremiumTimePrice = premiumTimePrice;
    }

    public String getReducetimepricepercent() {
        return Reducetimepricepercent;
    }

    public void setReducetimepricepercent(String reducetimepricepercent) {
        Reducetimepricepercent = reducetimepricepercent;
    }

    public String getPremiumMilesPrice() {
        return PremiumMilesPrice;
    }

    public void setPremiumMilesPrice(String premiumMilesPrice) {
        PremiumMilesPrice = premiumMilesPrice;
    }

    String  unitPrice;
   String  sumPrice;
   String  PremiumTimePrice;
   String  Reducetimepricepercent;
   String  PremiumMilesPrice;
}
