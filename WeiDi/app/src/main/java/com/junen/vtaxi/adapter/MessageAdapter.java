package com.junen.vtaxi.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.junen.vtaxi.R;
import com.junen.vtaxi.model.MessageItem;
import com.junen.vtaxi.model.VideoInfo;

import java.util.ArrayList;

/**
 * Created by new on 16/7/8.
 */
public class MessageAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater mInflater;
    private ArrayList<MessageItem> messageItemArrayList;

    public MessageAdapter(Context context,ArrayList<MessageItem> messageItemArrayList){
        super();
        this.context = context;
        this.messageItemArrayList = messageItemArrayList;
        mInflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return messageItemArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return messageItemArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView==null){
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.list_message_item, null);
            holder.readView = (TextView)convertView.findViewById(R.id.read_view);
            holder.titleView = (TextView)convertView.findViewById(R.id.title_view);
            holder.timeView = (TextView)convertView.findViewById(R.id.time_view);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        MessageItem messageItem = messageItemArrayList.get(position);
        if(1==messageItem.state){
            holder.readView.setVisibility(View.GONE);
        }else{
            holder.readView.setVisibility(View.VISIBLE);
        }
        holder.titleView.setText(messageItem.title);
        holder.timeView.setText(messageItem.time);
        return convertView;
    }

    public class ViewHolder{
        public TextView readView;
        public TextView titleView;
        public TextView timeView;
    }
}
