package com.junen.vtaxi.activity.base;

import android.app.Activity;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;

import com.huawei.android.app.admin.HwDevicePolicyManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Activity封装类
 * 
 * 封装生命周期监控，对话框等
 * 
 * @author menglin.li
 * 
 */
public abstract class BaseActivity extends Activity implements
		ActivityResponsable {
	final static String TAG = BaseActivity.class.getSimpleName();
	/**
	 * Activity辅助类
	 */
	private ActivityHelper mActivityHelper;
	public static final int FLAG_HOMEKEY_DISPATCHED = 0x80000000;//定义屏蔽参数
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mActivityHelper = new ActivityHelper(this);
		this.getWindow().setFlags(FLAG_HOMEKEY_DISPATCHED, FLAG_HOMEKEY_DISPATCHED);
		ComponentName cn = new ComponentName("com.junen.vtaxi","BaseActivity");
      //创建HwDevicePolicyManager类的对象
		HwDevicePolicyManager mHwPDM = new HwDevicePolicyManager(this);
		List<String>   packageNames=new ArrayList<>();
		packageNames.add("com.junen.vtaxi");
		mHwPDM.addPersistentApp(cn,packageNames);
		mHwPDM.setBackButtonDisabled(cn,true);
        mHwPDM.setRecentTaskButtonDisabled(cn,true);
		mHwPDM.setHomeButtonDisabled(cn,true);
		mHwPDM.setUninstallDisabled(cn,true);
	}
	@Override
	public void finish() {
		if (mActivityHelper != null) {
			mActivityHelper.finish();
		}
		super.finish();
	}
	/**
	 * 弹对话框
	 * 
	 * @param title
	 *            标题
	 * @param msg
	 *            消息
	 * @param positive
	 *            确定
	 * @param positiveListener
	 *            确定回调
	 * @param negative
	 *            否定
	 * @param negativeListener
	 *            否定回调
	 */
	@Override
	public void alert(String title, String msg, String positive,
			DialogInterface.OnClickListener positiveListener, String negative,
			DialogInterface.OnClickListener negativeListener) {
		mActivityHelper.alert(title, msg, positive, positiveListener, negative,
				negativeListener);
	}

	/**
	 * 弹对话框
	 * 
	 * @param title
	 *            标题
	 * @param msg
	 *            消息
	 * @param positive
	 *            确定
	 * @param positiveListener
	 *            确定回调
	 * @param negative
	 *            否定
	 * @param negativeListener
	 *            否定回调
	 * @param isCanceledOnTouchOutside
	 *            外部点是否可以取消对话框
	 */
	@Override
	public void alert(String title, String msg, String positive,
			DialogInterface.OnClickListener positiveListener, String negative,
			DialogInterface.OnClickListener negativeListener,
			Boolean isCanceledOnTouchOutside) {
		mActivityHelper.alert(title, msg, positive, positiveListener, negative,
				negativeListener, isCanceledOnTouchOutside);
	}

	/**
	 * 弹对话框
	 * 
	 * @param title
	 *            标题
	 * @param msg
	 *            消息
	 * @param positive
	 *            确定
	 * @param positiveListener
	 *            确定回调
	 * @param negative
	 *            否定
	 * @param negativeListener
	 *            否定回调
	 */
	@Override
	public void customAlert(String title, String msg, String positive,
			View.OnClickListener positiveListener, String negative,
			View.OnClickListener negativeListener, Boolean isBackClickCanceled) {
		mActivityHelper.customAlert(title, msg, positive, positiveListener,
				negative, negativeListener, false, isBackClickCanceled);
	}

	/**
	 * 弹对话框
	 * 
	 * @param msg
	 *            消息
	 * @param positive
	 *            确定
	 * @param positiveListener
	 *            确定回调
	 * @param negative
	 *            否定
	 * @param negativeListener
	 *            否定回调
	 */
	@Override
	public void customAlert(String msg, String positive,
			View.OnClickListener positiveListener, String negative,
			View.OnClickListener negativeListener, Boolean isCanceledBackClick) {
		mActivityHelper.customAlert(null, msg, positive, positiveListener,
				negative, negativeListener, false, isCanceledBackClick);
	}

	/**
	 * TOAST
	 * 
	 * @param msg
	 *            消息
	 * @param period
	 *            时长
	 */
	@Override
	public void toast(String msg, int period) {
		mActivityHelper.toast(msg, period);
	}

	/**
	 * 显示进度对话框
	 * 
	 * @param msg
	 *            消息
	 */
	@Override
	public void showProgressDialog(String msg) {
		mActivityHelper.showProgressDialog(msg);
	}

	/**
	 * 显示可取消的进度对话框
	 * 
	 * @param msg
	 *            消息
	 */
	public void showProgressDialog(final String msg, final boolean cancelable,
			final OnCancelListener cancelListener) {
		mActivityHelper.showProgressDialog(msg, cancelable, cancelListener);
	}

	@Override
	public void dismissProgressDialog() {
		mActivityHelper.dismissProgressDialog();
	}
	
	@Override
	public void dismissAlertDialog() {
		mActivityHelper.dismissAlertDialog();
	}

	@Override
	protected void onDestroy() {
		dismissProgressDialog();
		super.onDestroy();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		return super.dispatchTouchEvent(event);
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
// TODO Auto-generated method stub
		switch(keyCode){
			case KeyEvent.KEYCODE_BACK:
			case KeyEvent.KEYCODE_HOME:
			case KeyEvent.KEYCODE_MENU:
				return true;
			default:
				return false;
		}
	}
	public void back(){
		finish();
	}
	
	public void initView(){
		
	}
	
	public void initData(){
		
	}
}
