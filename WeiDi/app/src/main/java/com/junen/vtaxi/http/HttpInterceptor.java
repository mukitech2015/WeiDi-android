package com.junen.vtaxi.http;
//package com.dwd.rider.http;
//
//import android.content.Context;
//import android.content.SharedPreferences;
//import android.content.SharedPreferences.Editor;
//import android.text.TextUtils;
//import com.dada.mobile.library.pojo.PhoneInfo;
//import com.tomkey.commons.tools.Container;
//import com.tomkey.commons.tools.DevUtil;
//import com.tomkey.commons.tools.FileUtil;
//import com.tomkey.commons.tools.LocationUtil;
//import com.tomkey.commons.tools.MD5;
//import com.tomkey.commons.tools.NetworkUtil;
//import com.tomkey.commons.tools.Strings;
//import java.io.File;
//import java.io.IOException;
//import java.util.HashMap;
//import java.util.Map;
//import retrofit.RequestInterceptor;
//import retrofit.RequestInterceptor.RequestFacade;
//import retrofit.client.Request;
//
//public class HttpInterceptor
//  implements RequestInterceptor
//{
//  private static String a = "1";
//  private static int b = 0;
//  private static SharedPreferences c = Container.getPreference();
//  private static String d = "";
//
//  static
//  {
//    System.loadLibrary("sign");
//  }
//
//  public static String a(Context paramContext)
//  {
//    File localFile;
//    if (TextUtils.isEmpty(PhoneInfo.sdcardId))
//      if (FileUtil.hasOneSDCardMounted())
//      {
//        localFile = new File(FileUtil.getExternalSdCardPath(), ".dadatmp");
//        if (!localFile.exists())
//          break label111;
//        PhoneInfo.sdcardId = FileUtil.file2String(localFile.getAbsolutePath());
//      }
//    while (true)
//    {
//      Container.getPreference().edit().putString("sdcard_id", PhoneInfo.sdcardId).apply();
//      if (TextUtils.isEmpty(PhoneInfo.sdcardId))
//      {
//        PhoneInfo.sdcardId = Container.getPreference().getString("sdcard_id", "");
//        if (TextUtils.isEmpty(PhoneInfo.sdcardId))
//          PhoneInfo.sdcardId = PhoneInfo.memoryId;
//      }
//      return PhoneInfo.sdcardId;
//      label111: PhoneInfo.sdcardId = Strings.uniqueID();
//      try
//      {
//        FileUtil.store(localFile, PhoneInfo.sdcardId.getBytes());
//      }
//      catch (IOException localIOException)
//      {
//        localIOException.printStackTrace();
//      }
//    }
//  }
//
//  public static Map<String, String> a()
//  {
//    HashMap localHashMap = new HashMap();
//    localHashMap.put("User-Token", c());
//    localHashMap.put("User-Id", b() + "");
//    localHashMap.put("App-Version", PhoneInfo.versionName);
//    localHashMap.put("App-Name", PhoneInfo.appName);
//    localHashMap.put("Platform", "Android");
//    localHashMap.put("Channel-ID", PhoneInfo.umengChannel);
//    localHashMap.put("OS-Version", PhoneInfo.osVersion);
//    localHashMap.put("UUID", PhoneInfo.uuid);
//    localHashMap.put("Unique-Id", PhoneInfo.uniqueId);
//    localHashMap.put("Sdcard-Id", PhoneInfo.sdcardId);
//    localHashMap.put("Lat", PhoneInfo.lat + "");
//    localHashMap.put("Lng", PhoneInfo.lng + "");
//    localHashMap.put("Location-Time", PhoneInfo.locateTime + "");
//    localHashMap.put("City-Code", PhoneInfo.cityCode + "");
//    localHashMap.put("City-Id", PhoneInfo.cityId + "");
//    localHashMap.put("Model", PhoneInfo.model);
//    localHashMap.put("Location-Provider", PhoneInfo.locationProvider);
//    localHashMap.put("Network", NetworkUtil.getNetWorkTypeStr(Container.getContext()));
//    localHashMap.put("Enable-Gps", LocationUtil.isGPSEnableValue(Container.getContext()));
//    localHashMap.put("Accuracy", PhoneInfo.accuracy);
//    return localHashMap;
//  }
//
//  public static void a(int paramInt)
//  {
//    b = paramInt;
//    c.edit().putInt("header_user_id", paramInt).apply();
//  }
//
//  public static void a(String paramString)
//  {
//    a = paramString;
//    c.edit().putString("header_access_token", paramString).apply();
//  }
//
//  public static int b()
//  {
//    if (b == 0)
//      b = c.getInt("header_user_id", 0);
//    return b;
//  }
//
//  public static void b(String paramString)
//  {
//    d = paramString;
//  }
//
//  private void b(RequestInterceptor.RequestFacade paramRequestFacade)
//  {
//    paramRequestFacade.addHeader("User-Token", c());
//    paramRequestFacade.addHeader("User-Id", b() + "");
//    paramRequestFacade.addHeader("App-Version", PhoneInfo.versionName);
//    paramRequestFacade.addHeader("App-Name", PhoneInfo.appName);
//    paramRequestFacade.addHeader("Platform", PhoneInfo.platform);
//    paramRequestFacade.addHeader("Channel-ID", PhoneInfo.umengChannel);
//    paramRequestFacade.addHeader("OS-Version", PhoneInfo.osVersion);
//    paramRequestFacade.addHeader("UUID", PhoneInfo.uuid);
//    paramRequestFacade.addHeader("Sdcard-Id", PhoneInfo.sdcardId);
//    paramRequestFacade.addHeader("Lat", PhoneInfo.lat + "");
//    paramRequestFacade.addHeader("Lng", PhoneInfo.lng + "");
//    paramRequestFacade.addHeader("Location-Time", PhoneInfo.locateTime + "");
//    paramRequestFacade.addHeader("City-Code", PhoneInfo.cityCode + "");
//    paramRequestFacade.addHeader("City-Id", PhoneInfo.cityId + "");
//    paramRequestFacade.addHeader("Model", PhoneInfo.model);
//    paramRequestFacade.addHeader("Location-Provider", PhoneInfo.locationProvider);
//    paramRequestFacade.addHeader("Network", NetworkUtil.getNetWorkTypeStr(Container.getContext()));
//    paramRequestFacade.addHeader("Enable-Gps", LocationUtil.isGPSEnableValue(Container.getContext()));
//    paramRequestFacade.addHeader("Accuracy", PhoneInfo.accuracy);
//    paramRequestFacade.addHeader("Verification-Hash", a(paramRequestFacade));
//  }
//
//  public static String c()
//  {
//    if (("1".equals(a)) || (TextUtils.isEmpty(a)))
//      a = c.getString("header_access_token", "1");
//    return a;
//  }
//
//  protected String a(RequestInterceptor.RequestFacade paramRequestFacade)
//  {
//    Request localRequest = paramRequestFacade.getRequest();
//    String str1 = localRequest.getMethod();
//    String str2 = "";
//    if ("GET".equals(str1))
//      str2 = localRequest.getQueryUrl();
//    while (true)
//    {
//      DevUtil.d("zqt", "hash:" + str2);
//      if ((!DevUtil.isDebug()) || (TextUtils.isEmpty(d)))
//        break;
//      return d;
//      if ("POST".equals(str1))
//        str2 = localRequest.getBodyAsString();
//    }
//    return MD5.getMD5(getHash(str2, Container.getContext()).getBytes());
//  }
//
//  public native String getHash(String paramString, Context paramContext);
//
//  public void intercept(RequestInterceptor.RequestFacade paramRequestFacade)
//  {
//    b(paramRequestFacade);
//  }
//}
//
///* Location:           /Users/limenglin/Documents/tools/dex2jar/classes_dex2jar.jar
// * Qualified Name:     com.dada.mobile.library.http.HttpInterceptor
// * JD-Core Version:    0.6.2
// */