package com.junen.vtaxi.database;

public class WdLocation {
	public int _id;
	public int type;//0-普通，1-开始，2-结束
	public int lat;
	public int lng;
	public int accuracy;//精度
	public int speed;//速度
    public int distance;//里程(m)
    public int wait;//等待时间(s)
    public int total;//总时间(s)
    public String orderId;//订单ID
    
    public WdLocation() { 
    	
    }

	public WdLocation(int type,int lat, int lng, int accuracy, int speed, int distance,
			int wait, int total,String orderId) {
		super();
		this.type = type;
		this.lat = lat;
		this.lng = lng;
		this.accuracy = accuracy;
		this.speed = speed;
		this.distance = distance;
		this.wait = wait;
		this.total = total;
		this.orderId = orderId;
	}

	@Override
	public String toString() {
		StringBuffer sbf = new StringBuffer();
		sbf.append("lat");
		sbf.append(lat);
		sbf.append(",lng=");
		sbf.append(lng);
		sbf.append(",accuracy=");
		sbf.append(accuracy);
		sbf.append(",speed=");
		sbf.append(speed);
		sbf.append(",distance=");
		sbf.append(distance);
		sbf.append(",wait=");
		sbf.append(wait);
		sbf.append(",total=");
		sbf.append(total);
		
		return sbf.toString();
	}
	
	
}
