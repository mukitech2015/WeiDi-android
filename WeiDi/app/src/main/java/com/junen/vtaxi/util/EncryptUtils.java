package com.junen.vtaxi.util;

import java.net.URLDecoder;
import java.util.Arrays;

/**
 * 排序加密
 * 
 * @author menglin.li
 * 
 */
public class EncryptUtils {

	/**
	 * 排序并加密
	 * 
	 * @param params
	 * @param token
	 * @return
	 */
	public static String sign(String params, String token) {
		String[] paramsArray = params.split("&");
		Arrays.sort(paramsArray);
		String sortParams = join(paramsArray, "&");
		try {
			sortParams = URLDecoder.decode(sortParams, "UTF-8");
		} catch (Exception e) {
		}
		StringBuffer paramsBuffer = new StringBuffer();
		paramsBuffer.append(EncrytionKey.getEncrytKey()).append(sortParams)
				.append(token);
		String encryptParams = DigestUtils.MD5(DigestUtils.SHA(paramsBuffer
				.toString()));
		StringBuffer newParamsBuffer = new StringBuffer(params);
		newParamsBuffer.append("&sign=").append(encryptParams);
		return newParamsBuffer.toString();
	}

	/**
	 * 参数数组拼接
	 * 
	 * @param array
	 * @param separator
	 * @return
	 */
	public static String join(Object array[], String separator) {
		if (array == null) {
			return null;
		}
		if (separator == null) {
			separator = "";
		}
		int startIndex = 0;
		int endIndex = array.length;
		int bufSize = endIndex - startIndex;
		if (bufSize <= 0) {
			return "";
		}
		bufSize *= (array[startIndex] != null ? array[startIndex].toString()
				.length() : 16) + separator.length();
		StringBuffer buf = new StringBuffer(bufSize);
		for (int i = startIndex; i < endIndex; i++) {
			if (i > startIndex)
				buf.append(separator);
			if (array[i] != null)
				buf.append(array[i]);
		}
		return buf.toString();
	}

}
