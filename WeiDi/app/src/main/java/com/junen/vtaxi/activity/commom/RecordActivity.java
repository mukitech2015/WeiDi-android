package com.junen.vtaxi.activity.commom;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.junen.vtaxi.R;
import com.junen.vtaxi.activity.account.MessageActivity;
import com.junen.vtaxi.activity.base.BaseActivity;
import com.junen.vtaxi.adapter.MessageAdapter;
import com.junen.vtaxi.adapter.RecordAdapter;
import com.junen.vtaxi.app.BaseApplication;
import com.junen.vtaxi.common.Constant;
import com.junen.vtaxi.http.ApiClient;
import com.junen.vtaxi.model.MessageItem;
import com.junen.vtaxi.model.MessageRusult;
import com.junen.vtaxi.model.Record;
import com.junen.vtaxi.model.RecordResult;
import com.junen.vtaxi.rpc.RpcExcutor;
import com.junen.vtaxi.rpc.api.RpcApi;
import com.junen.vtaxi.widget.XListView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by root on 16-12-16.
 */

public class RecordActivity extends BaseActivity {
    private XListView listView;
    private TextView emptyView;
    private ArrayList<Record> recordItemArray = new ArrayList<Record>();
    private RecordAdapter recordAdapter;
    private RpcExcutor<RecordResult> recordResultRpcExcutor;
    private int page = 1;
    private int tempPage = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_record);
        initView();
        initData();
    }
    @Override
    public void initView() {
        super.initView();
        Button backView = (Button) findViewById(R.id.close_view);
        backView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                back();
            }
        });

        listView = (XListView) findViewById(R.id.list_view);
        emptyView = (TextView) findViewById(R.id.empty_view);

        listView.setEmptyView(emptyView);
        listView.setVisibility(View.VISIBLE);
        listView.setPullRefreshEnable(true);
        listView.setPullLoadEnable(true);
        listView.setXListViewListener(listViewListener);
    }

    @Override
    public void initData() {
        super.initData();
        initRpcExcutor();
        //performData();
        recordItemArray  = new ArrayList<Record>();
        recordAdapter = new RecordAdapter(RecordActivity.this, recordItemArray);
        listView.setAdapter(recordAdapter);
        tempPage = 0;
        page = 0;
        recordResultRpcExcutor.start();
    }

    private void onLoad() {
        listView.stopRefresh();
        listView.stopLoadMore();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date curDate = new Date(System.currentTimeMillis());// 获取当前时间
        String str = formatter.format(curDate);
        listView.setRefreshTime(str);
    }

    /**
     * ListView上拉下拉监听
     */
    XListView.IXListViewListener listViewListener = new XListView.IXListViewListener() {

        @Override
        public void onRefresh() {
            tempPage = 1;
            page = 1;
            recordResultRpcExcutor.start();
        }

        @Override
        public void onLoadMore() {
            tempPage = page + 1;
            recordResultRpcExcutor.start();
        }

    };


    protected void initRpcExcutor() {
        recordResultRpcExcutor = new RpcExcutor<RecordResult>(this, 0, false) {
            @Override
            public void excute(Object... params) {
                ApiClient.createApi(RecordActivity.this, RpcApi.class).getRecordList(20, BaseApplication.getInstance().getDriverId(), tempPage, this);
            }

            @Override
            public void onRpcFinish(RecordResult  result, Object... params) {
                doResult(result);
            }

            @Override
            public void onRpcException(int code, String msg, Object... params) {
                toast(msg, Toast.LENGTH_SHORT);
            }

        };
        recordResultRpcExcutor.setShowProgressDialog(false);
        recordResultRpcExcutor.setShowNetworkErrorView(false);
    }

    private void doResult(RecordResult  result) {
        if (result == null) {
            onLoad();
            return;
        }
        page = tempPage;
        if(recordItemArray==null){
            recordItemArray = new ArrayList<Record>();
        }
        if(1==page){
            recordItemArray.clear();
        }
        ArrayList<Record> recordArray = (ArrayList<Record>)result.list;
        if(recordArray!=null){
            int size = recordArray.size();
            for(int i=0;i<size;i++){
                recordItemArray.add(recordArray.get(i));
            }
        }
        if (recordAdapter == null) {
            recordAdapter = new RecordAdapter(RecordActivity.this,recordItemArray);
            listView.setAdapter(recordAdapter);
        } else {
            recordAdapter.notifyDataSetChanged();
        }
        onLoad();
        if(recordArray==null ||recordArray.size()<10){
            //不可加载更多
            listView.setPullLoadEnable(false);
        }else{
            listView.setPullLoadEnable(true);
        }
    }

}
