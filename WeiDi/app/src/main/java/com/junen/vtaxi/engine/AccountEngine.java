package com.junen.vtaxi.engine;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;

import com.junen.vtaxi.R;
import com.junen.vtaxi.app.BaseApplication;
import com.junen.vtaxi.common.Constant;
import com.junen.vtaxi.model.AdvertResult;
import com.junen.vtaxi.model.ApkInfo;
import com.junen.vtaxi.model.AudioFile;
import com.junen.vtaxi.model.HolidayData;
import com.junen.vtaxi.model.LoginResult;
import com.junen.vtaxi.model.PadBanner;
import com.junen.vtaxi.model.PayOrder;
import com.junen.vtaxi.model.ValuationResult;
import com.junen.vtaxi.model.VideoInfo;
import com.junen.vtaxi.service.UpdateService;
import com.junen.vtaxi.util.AppUtils;
import com.junen.vtaxi.util.DateUtil;

import java.math.BigDecimal;
import java.util.ArrayList;

public class AccountEngine {
	private static float initprice;//起步价/元
	private static float initmiles;//起步价内的公里数
	private static float everymilesprice;//每公里价格/元
	private static String premiumstime;//溢价开始时间
	private static String premiumetime;//溢价结束时间
	private static float premiumtimepricepercent;//溢价百分比
	private static String reducestime;//减价开始时间
	private static String reduceetime;//减价结束时间
	private static float reducepricepercent;//减价百分比
	private static float premiummiles;//溢价里程
	private static float premiummilespricepercent;//溢价里程的溢价百分比
	private static int waittime;//等待时间折合1公里
	private static float intervaltime;//间隔跳表价
	private static float everyhourprice;//每小时/元
	
	/*
	 * 保存登录账号
	 */
	public static void saveLoginAccount(String account){
		SharedPreferences shared = BaseApplication.getInstance().getSharedPreferences(
				Constant.LOGIN_ACCOUNT_KEY, Context.MODE_PRIVATE);
		Editor editor = shared.edit();
		editor.putString(Constant.LOGIN_USER_NAME_KEY, account);
		editor.commit();
	}
	
	public static String getLoginAccount(){
		SharedPreferences shared = BaseApplication.getInstance().getSharedPreferences(
				Constant.LOGIN_ACCOUNT_KEY, Context.MODE_PRIVATE);
		return shared.getString(Constant.LOGIN_USER_NAME_KEY, null);
	}
	
	public static SharedPreferences getSharedPreferences(){
		return BaseApplication.getInstance().getSharedPreferences(Constant.LOGIN_RESULT_KEY, Context.MODE_PRIVATE);
	}
	
	public static int getInt(String name){
		return getSharedPreferences().getInt(name, 0);
	}
	
	public static float getFloat(String name){
		return getSharedPreferences().getFloat(name, 0);
	}
	
	public static String getString(String name){
		return getSharedPreferences().getString(name, "");
	}
	
	/*
	 * 保存登录信息
	 */
	public static void saveLoginInfo(LoginResult result){
		if(null==result || null==result.info){
			return;
		}
		SharedPreferences shared = BaseApplication.getInstance().getSharedPreferences(
				Constant.LOGIN_RESULT_KEY, Context.MODE_PRIVATE);
		Editor editor = shared.edit();
		editor.putString(Constant.USER_INFO_ID_KEY, result.info.id);
		editor.putString(Constant.USER_INFO_NAME_KEY, result.info.name);
		editor.putString(Constant.USER_INFO_PHONE_KEY, result.info.phone);
		editor.putString(Constant.USER_INFO_CARNO_KEY, result.info.carno);
		editor.putString(Constant.USER_INFO_SGCARDNO_KEY, result.info.sgcardno);
		editor.putString(Constant.USER_INFO_ADDRESS_KEY, result.info.address);
		editor.putString(Constant.USER_INFO_LOGOURL_KEY, result.info.logourl);
		editor.putString(Constant.USER_INFO_MONEY_KEY, result.info.money);
		editor.putString(Constant.USER_INFO_SCORE_KEY, result.info.score);
		editor.putString(Constant.USER_INFO_ORDERNUM_KEY, result.info.ordernum);
		editor.putString(Constant.USER_INFO_GNAME_KEY, result.info.gname);
		editor.putInt(Constant.USER_INFO_SEX_KEY, result.info.sex);
		if(null!=result.padbanner){
			editor.putInt(Constant.BANNER_VERSION_KEY, result.padbanner.version);
			editor.putString(Constant.BANNER_CONTENT_KEY, result.padbanner.content);
		}
		if(null!=result.milesvaluation){
			initprice = result.milesvaluation.initprice;
			initmiles = result.milesvaluation.initmiles;
			everymilesprice = result.milesvaluation.everymilesprice;
			premiumstime = result.milesvaluation.premiumstime;
			premiumetime = result.milesvaluation.premiumetime;
			premiumtimepricepercent = result.milesvaluation.premiumtimepricepercent;
			//减价
			reducestime = result.milesvaluation.reducestime;
			reduceetime = result.milesvaluation.reduceetime;
			reducepricepercent = result.milesvaluation.reducepricepercent;

			premiummiles = result.milesvaluation.premiummiles;
			premiummilespricepercent = result.milesvaluation.premiummilespricepercent;
			waittime = result.milesvaluation.waittime;
			intervaltime = result.milesvaluation.intervaltime;

			editor.putInt(Constant.MILE_VERSION_KEY, result.milesvaluation.version);
			editor.putFloat(Constant.MILE_INIT_PRICE_KEY, result.milesvaluation.initprice);
			editor.putFloat(Constant.MILE_INIT_MILES_KEY, result.milesvaluation.initmiles);
			editor.putFloat(Constant.MILE_PER_KM_PRICE_KEY, result.milesvaluation.everymilesprice);
			editor.putString(Constant.MILE_PREMIUM_START_TIME_KEY, result.milesvaluation.premiumstime);
			editor.putString(Constant.MILE_PREMIUM_END_TIME_KEY, result.milesvaluation.premiumetime);
			editor.putFloat(Constant.MILE_PREMIUM_TIME_PRICE_KEY, result.milesvaluation.premiumtimepricepercent);
			//减价
			editor.putString(Constant.MILE_REDUCE_START_TIME_KEY, result.milesvaluation.reducestime);
			editor.putString(Constant.MILE_REDUCE_END_TIME_KEY, result.milesvaluation.reduceetime);
			editor.putFloat(Constant.MILE_REDUCE_TIME_PRICE_KEY, result.milesvaluation.reducepricepercent);

			editor.putFloat(Constant.MILE_PREMIUM_KM_KEY, result.milesvaluation.premiummiles);
			editor.putFloat(Constant.MILE_PREMIUM_KM_PRICE_KEY, result.milesvaluation.premiummilespricepercent);
			editor.putInt(Constant.MILE_WAIT_TIME_KM_KEY, result.milesvaluation.waittime);
			editor.putFloat(Constant.MILE_INTERVAL_PRICE_KEY, result.milesvaluation.intervaltime);
		}
		if(null!=result.timevaluation){
			everyhourprice = result.timevaluation.everyhourprice;
			editor.putInt(Constant.TIME_VERSION_KEY, result.timevaluation.version);
			editor.putFloat(Constant.TIME_PER_PRICE_KEY, result.timevaluation.everyhourprice);
		}
		
		
		if(null!=result.padbanner){
			editor.putInt(Constant.BANNER_VERSION_KEY, result.padbanner.version);
			editor.putString(Constant.BANNER_CONTENT_KEY, result.padbanner.content);
		}
		
		editor.commit();
		
		
	}
	 
	
	public static void saveBannerInfo(PadBanner padBanner){
		if(padBanner==null){
			return;
		}
		SharedPreferences shared = BaseApplication.getInstance().getSharedPreferences(
				Constant.LOGIN_RESULT_KEY, Context.MODE_PRIVATE);
		Editor editor = shared.edit();
		editor.putInt(Constant.BANNER_VERSION_KEY, padBanner.version);
		editor.putString(Constant.BANNER_CONTENT_KEY, padBanner.content);
		
		editor.commit();
	}


	public  static void  saveEndVoiceInfo(AudioFile audioFile){
		if(audioFile==null){
			return;
		}
		SharedPreferences shared = BaseApplication.getInstance().getSharedPreferences(Constant.VOICE_END_RESULT_KEY, Context.MODE_PRIVATE);
		Editor editor = shared.edit();
		editor.putInt(Constant.VOICE_END_VERSION_KEY, audioFile.version);
		editor.putString(Constant.VOICE_END_URL_KEY, audioFile.fileurl);
		editor.putString(Constant.VOICE_END_PATH_KEY, audioFile.sdPath);
		editor.putString(Constant.VOICE_END_ID_KEY, audioFile.id);
		editor.putString(Constant.VOICE_END_NAME_KEY,audioFile.name);
		editor.putInt(Constant.VOICE_END_SIZE_KEY,audioFile.size);
		editor.commit();
	}
	private  static  int   voiceEndVersion=0;
	public   static   int  getVoiceEndVersion( ) {
		if (voiceEndVersion== 0) {
			SharedPreferences shared = BaseApplication.getInstance().getSharedPreferences(Constant.VOICE_END_RESULT_KEY, Context.MODE_PRIVATE);
			voiceEndVersion=shared.getInt(Constant.VOICE_END_VERSION_KEY, 0);
		}
		return   voiceEndVersion;
	}
	public  static  AudioFile   getEndAudioFile(  ){
		AudioFile   audioFile=new AudioFile();
		SharedPreferences shared = BaseApplication.getInstance().getSharedPreferences(Constant.VOICE_END_RESULT_KEY, Context.MODE_PRIVATE);
		audioFile.version=shared.getInt(Constant.VOICE_END_VERSION_KEY,0);
		voiceEndVersion=audioFile.version;
		audioFile.fileurl=shared.getString(Constant.VOICE_END_URL_KEY, null);
		audioFile.sdPath=shared.getString(Constant.VOICE_END_PATH_KEY,null);
		audioFile.name=shared.getString(Constant.VOICE_END_NAME_KEY,null);
		audioFile.size=shared.getInt(Constant.VOICE_END_SIZE_KEY,0);
		audioFile.id=shared.getString(Constant.VOICE_END_ID_KEY,null);
		return audioFile;
	}








	public  static void  saveStartVoiceInfo(AudioFile audioFile){
		if(audioFile==null){
			return;
		}
		SharedPreferences shared = BaseApplication.getInstance().getSharedPreferences(Constant.VOICE_START_RESULT_KEY, Context.MODE_PRIVATE);
		Editor editor = shared.edit();
		editor.putInt(Constant.VOICE_START_VERSION_KEY, audioFile.version);
		editor.putString(Constant.VOICE_START_URL_KEY, audioFile.fileurl);
		editor.putString(Constant.VOICE_START_PATH_KEY, audioFile.sdPath);
		editor.putString(Constant.VOICE_START_ID_KEY, audioFile.id);
		editor.putString(Constant.VOICE_START_NAME_KEY,audioFile.name);
		editor.putInt(Constant.VOICE_START_SIZE_KEY,audioFile.size);
		editor.commit();
	}
	private static int bannerVersion = 0;
	public static int getBannerVersion(){
		if(bannerVersion==0){
			SharedPreferences shared = BaseApplication.getInstance().getSharedPreferences(
					Constant.LOGIN_RESULT_KEY, Context.MODE_PRIVATE);
			bannerVersion=shared.getInt(Constant.BANNER_VERSION_KEY, 0);
		}
		
		return bannerVersion;
	}
	
	public static String getBannerText(){
		SharedPreferences shared = BaseApplication.getInstance().getSharedPreferences(
				Constant.LOGIN_RESULT_KEY, Context.MODE_PRIVATE);
		return shared.getString(Constant.BANNER_CONTENT_KEY, "");
	}
	public static void saveAdvertInfo(AdvertResult result){
		if(result==null || result.list==null){
			return;
		}
		ArrayList<VideoInfo> list = getVideoArray();
		SharedPreferences shared = BaseApplication.getInstance().getSharedPreferences(
				Constant.ADVERT_RESULT_KEY, Context.MODE_PRIVATE);
		Editor editor = shared.edit();
		editor.clear();
		editor.commit();
		editor = shared.edit();
		int size = result.list.size();
		advertVersion = result.version;
		editor.putInt(Constant.ADVERT_VERSION_KEY, result.version);
		editor.putInt(Constant.ADVERT_SUM_KEY, size);
		for(int i=0;i<size;i++){
			VideoInfo videoInfo = result.list.get(i);
			editor.putString(Constant.ADVERT_VIDEO_ID_KEY+i, videoInfo.id);
			editor.putString(Constant.ADVERT_VIDEO_NAME_KEY+i, videoInfo.name);
			editor.putInt(Constant.ADVERT_VIDEO_SIZE_KEY+i, videoInfo.size);
			editor.putString(Constant.ADVERT_VIDEO_URL_KEY+i, videoInfo.url);
			editor.putString(Constant.ADVERT_VIDEO_LOGOURL_KEY+i, videoInfo.logourl);
			editor.putString(Constant.ADVERT_VIDEO_PATH_KEY+i, videoInfo.sdPath);
			for(int j=0;j<list.size();j++){
				   VideoInfo videoInfo1 = list.get(j);
				if(TextUtils.equals(videoInfo1.url, videoInfo.url)){
					editor.putString(Constant.ADVERT_VIDEO_PATH_KEY+i, videoInfo1.sdPath);
					break;
				}
			}
			
		}
		editor.commit();
	}

	private static int advertVersion = 0;
	public static int getAdvertVersion(){
		if(advertVersion==0){
			SharedPreferences shared = BaseApplication.getInstance().getSharedPreferences(
					Constant.ADVERT_RESULT_KEY, Context.MODE_PRIVATE);
			advertVersion=shared.getInt(Constant.ADVERT_VERSION_KEY, 0);
		}
		return advertVersion;
	}

	private  static  int   voiceStartVersion=0;
	public   static   int  getVoiceStartVersion( ) {
		if (voiceStartVersion == 0) {
			SharedPreferences shared = BaseApplication.getInstance().getSharedPreferences(Constant.VOICE_START_RESULT_KEY, Context.MODE_PRIVATE);
			voiceStartVersion=shared.getInt(Constant.VOICE_START_VERSION_KEY, 0);
		}
       return   voiceStartVersion;
	}
	public  static  AudioFile   getStartAudioFile(  ){
		AudioFile   audioFile=new AudioFile();
		SharedPreferences shared = BaseApplication.getInstance().getSharedPreferences(Constant.VOICE_START_RESULT_KEY, Context.MODE_PRIVATE);
		audioFile.version=shared.getInt(Constant.VOICE_START_VERSION_KEY,0);
		voiceStartVersion=audioFile.version;
		audioFile.fileurl=shared.getString(Constant.VOICE_START_URL_KEY, null);
		audioFile.sdPath=shared.getString(Constant.VOICE_START_PATH_KEY,null);
        audioFile.name=shared.getString(Constant.VOICE_START_NAME_KEY,null);
		audioFile.size=shared.getInt(Constant.VOICE_START_SIZE_KEY,0);
		audioFile.id=shared.getString(Constant.VOICE_START_ID_KEY,null);
		return audioFile;
	}


	public static ArrayList<VideoInfo> getVideoArray(){
		ArrayList<VideoInfo> list = new ArrayList<VideoInfo>();
		SharedPreferences shared = BaseApplication.getInstance().getSharedPreferences(Constant.ADVERT_RESULT_KEY, Context.MODE_PRIVATE);
		int size = shared.getInt(Constant.ADVERT_SUM_KEY, 0);
		for(int i=0;i<size;i++){
			VideoInfo videoInfo = new VideoInfo();
			videoInfo.id = shared.getString(Constant.ADVERT_VIDEO_ID_KEY+i, null);
			videoInfo.name = shared.getString(Constant.ADVERT_VIDEO_NAME_KEY+i, null);
			videoInfo.size = shared.getInt(Constant.ADVERT_VIDEO_SIZE_KEY+i, 0);
			videoInfo.url = shared.getString(Constant.ADVERT_VIDEO_URL_KEY+i, null);
			videoInfo.logourl = shared.getString(Constant.ADVERT_VIDEO_LOGOURL_KEY+i, null);
			videoInfo.sdPath = shared.getString(Constant.ADVERT_VIDEO_PATH_KEY+i, null);
			list.add(videoInfo);
		}
		return list;
	}
	
	
	public static String getDriverId(){
		SharedPreferences shared = BaseApplication.getInstance().getSharedPreferences(Constant.LOGIN_RESULT_KEY,
				Context.MODE_PRIVATE);
		return shared.getString(Constant.USER_INFO_ID_KEY, null);
	}
	
	public static void clearLoginInfo(){
		SharedPreferences shared = BaseApplication.getInstance().getSharedPreferences(
				Constant.LOGIN_RESULT_KEY, Context.MODE_PRIVATE);
		Editor editor = shared.edit();
		editor.clear();
		editor.commit();
	}
	
	public static void savePrinterMac(String macAddress){
		SharedPreferences shared = BaseApplication.getInstance().getSharedPreferences(
				Constant.LOGIN_RESULT_KEY, Context.MODE_PRIVATE);
		Editor editor = shared.edit();
		editor.putString(Constant.PRINTER_MAC_ADDRESS_KEY, macAddress);
		editor.commit();
	}
	
	public static String getPrinterMac(){
		SharedPreferences shared = BaseApplication.getInstance().getSharedPreferences(
				Constant.LOGIN_RESULT_KEY, Context.MODE_PRIVATE);
		return shared.getString(Constant.PRINTER_MAC_ADDRESS_KEY, null);
	}
	
	/*
	 * 保存计价规则
	 */
	public static void saveValuationRules(ValuationResult result){
		if(null==result || null==result.milesvaluation || null==result.timevaluation){
			return;
		}
		SharedPreferences shared = BaseApplication.getInstance().getSharedPreferences(
				Constant.LOGIN_RESULT_KEY, Context.MODE_PRIVATE);
		Editor editor = shared.edit();
		
		if(null!=result.milesvaluation){
			initprice = result.milesvaluation.initprice;
			initmiles = result.milesvaluation.initmiles;
			everymilesprice = result.milesvaluation.everymilesprice;
			premiumstime = result.milesvaluation.premiumstime;
			premiumetime = result.milesvaluation.premiumetime;
			premiumtimepricepercent = result.milesvaluation.premiumtimepricepercent;
			//减价
			reducestime = result.milesvaluation.reducestime;
			reduceetime = result.milesvaluation.reduceetime;
			reducepricepercent = result.milesvaluation.reducepricepercent;

			premiummiles = result.milesvaluation.premiummiles;
			premiummilespricepercent = result.milesvaluation.premiummilespricepercent;
			waittime = result.milesvaluation.waittime;
			intervaltime = result.milesvaluation.intervaltime;
			
			editor.putInt(Constant.MILE_VERSION_KEY, result.milesvaluation.version);
			editor.putFloat(Constant.MILE_INIT_PRICE_KEY, result.milesvaluation.initprice);
			editor.putFloat(Constant.MILE_INIT_MILES_KEY, result.milesvaluation.initmiles);
			editor.putFloat(Constant.MILE_PER_KM_PRICE_KEY, result.milesvaluation.everymilesprice);
			editor.putString(Constant.MILE_PREMIUM_START_TIME_KEY, result.milesvaluation.premiumstime);
			editor.putString(Constant.MILE_PREMIUM_END_TIME_KEY, result.milesvaluation.premiumetime);
			editor.putFloat(Constant.MILE_PREMIUM_TIME_PRICE_KEY, result.milesvaluation.premiumtimepricepercent);
			//减价
			editor.putString(Constant.MILE_REDUCE_START_TIME_KEY, result.milesvaluation.reducestime);
			editor.putString(Constant.MILE_REDUCE_END_TIME_KEY, result.milesvaluation.reduceetime);
			editor.putFloat(Constant.MILE_REDUCE_TIME_PRICE_KEY, result.milesvaluation.reducepricepercent);

			editor.putFloat(Constant.MILE_PREMIUM_KM_KEY, result.milesvaluation.premiummiles);
			editor.putFloat(Constant.MILE_PREMIUM_KM_PRICE_KEY, result.milesvaluation.premiummilespricepercent);
			editor.putInt(Constant.MILE_WAIT_TIME_KM_KEY, result.milesvaluation.waittime);
			editor.putFloat(Constant.MILE_INTERVAL_PRICE_KEY, result.milesvaluation.intervaltime);
		}
		if(null!=result.timevaluation){
			everyhourprice = result.timevaluation.everyhourprice;
			editor.putInt(Constant.TIME_VERSION_KEY, result.timevaluation.version);
			editor.putFloat(Constant.TIME_PER_PRICE_KEY, result.timevaluation.everyhourprice);
		}
		
		editor.commit();
	}
	
	public static String[] getValuationText(){
		String[] valuationRules = new String[2];
		StringBuffer sbf = new StringBuffer();
		SharedPreferences shared = BaseApplication.getInstance().getSharedPreferences(
				Constant.LOGIN_RESULT_KEY, Context.MODE_PRIVATE);
		
		//起步价?元，含?公里，每公里?元\n时间??:??至??:??加价?%\n超过?公里加价?%\n每等待?分钟后折合1公里\n每?角/?
		sbf.append("起步价");
		sbf.append(shared.getFloat(Constant.MILE_INIT_PRICE_KEY, 0)+"元，含");
		sbf.append(shared.getFloat(Constant.MILE_INIT_MILES_KEY, 0)+"公里，每公里");
		sbf.append(shared.getFloat(Constant.MILE_PER_KM_PRICE_KEY, 0)+"元\n");
		if(!TextUtils.isEmpty(shared.getString(Constant.MILE_PREMIUM_START_TIME_KEY, ""))){
			sbf.append("时间");
			sbf.append(shared.getString(Constant.MILE_PREMIUM_START_TIME_KEY, "")+"至");
			sbf.append(shared.getString(Constant.MILE_PREMIUM_END_TIME_KEY, "")+"加价");
			sbf.append(shared.getFloat(Constant.MILE_PREMIUM_TIME_PRICE_KEY, 0)+"%\n");
		}
		if(!TextUtils.isEmpty(shared.getString(Constant.MILE_REDUCE_START_TIME_KEY, ""))){
			sbf.append("时间");
			sbf.append(shared.getString(Constant.MILE_REDUCE_START_TIME_KEY, "")+"至");
			sbf.append(shared.getString(Constant.MILE_REDUCE_END_TIME_KEY, "")+"打折");
			sbf.append(shared.getFloat(Constant.MILE_REDUCE_TIME_PRICE_KEY, 0)+"%\n");
		}
		if(0!=shared.getFloat(Constant.MILE_PREMIUM_KM_KEY, 0)){
			sbf.append("超过");
			sbf.append(shared.getFloat(Constant.MILE_PREMIUM_KM_KEY, 0)+"公里加价");
			sbf.append(shared.getFloat(Constant.MILE_PREMIUM_KM_PRICE_KEY, 0)+"%\n");
		}
		sbf.append("每等待");
		sbf.append(shared.getInt(Constant.MILE_WAIT_TIME_KM_KEY, 0) + "分钟后折合1公里\n每");
		sbf.append(shared.getFloat(Constant.MILE_INTERVAL_PRICE_KEY, 0) + "元跳表（刷新）");
		valuationRules[0] = sbf.toString();
		
		sbf = new StringBuffer();
		if(0!=shared.getFloat(Constant.TIME_PER_PRICE_KEY, 0)){
			sbf.append("每分钟");
			sbf.append(shared.getFloat(Constant.TIME_PER_PRICE_KEY, 0));
			sbf.append("元（不足一分钟，则除以60，按秒计算，总价精确到0.1元）");
		}
		valuationRules[1] = sbf.toString();
		return valuationRules;
	}
	
	
	public static float valuationPrice(){
		getInitPrice();
		getInitMiles();
		getEveryMilesPrice();
		getWaitTime();
		getIntervalTime();
		getPremiumMiles();
		getPremiumMilesPrice();
		float totalPrice = 0;
		if(0==BaseApplication.valuationType){
			int iDistance = (int) (BaseApplication.lastDistance/10);
			float distance = iDistance/100.0f;
			float premium = 0;

			if(1==BaseApplication.isPremium){
				premium = getPremiumTimePrice();
			}else if(2==BaseApplication.isPremium){
				premium = - getReducetimepricepercent();
			}
			distance = distance+(int)(valuationWaitTime()/(waittime*60f));
			if(distance>initmiles){
				totalPrice = initprice+(distance-initmiles)*everymilesprice;
				if(distance>premiummiles && premiummiles>0){
					premium = premium+premiummilespricepercent;
				}
				totalPrice = (totalPrice*(1+premium*0.01f));
				int num = (int)(totalPrice/intervaltime);
				totalPrice = intervaltime*num;
			}else{
				totalPrice = initprice;
			}
		}else if(1==BaseApplication.valuationType){
			totalPrice = getEveryHourPrice()*valuationTotalTime()/3600;
			int num = (int)(totalPrice/intervaltime);
			totalPrice = intervaltime*num;
		}
		
		return totalPrice;
	}

	/*
	public static String valuationTotalPrice(){
		getInitPrice();
		getInitMiles();
		getEveryMilesPrice();
		getWaitTime();
		getIntervalTime();
		getPremiumMiles();
		getPremiumMilesPrice();
		float totalPrice = 0;
		int price = 0;
		if(0==BaseApplication.valuationType){
			int iDistance = (int) (BaseApplication.lastDistance/10);
			float distance = iDistance/100.0f;
			float premium = 0;
			if(1==BaseApplication.isPremium){
				premium = getPremiumTimePrice();
			}else if(2==BaseApplication.isPremium){
				premium = -getReducetimepricepercent();
			}
			distance = distance+(int)(valuationWaitTime()/(waittime*60f));
			if(distance>initmiles){
				totalPrice = initprice+(distance-initmiles)*everymilesprice;
				if(distance>premiummiles){
					premium = premium+premiummilespricepercent;
				}
				getIntervalTime();
				totalPrice = (totalPrice*(1+premium*0.01f));
				int num = (int)(totalPrice/intervaltime);
				totalPrice = intervaltime*num;
				price = (int) (100*totalPrice);
				
			}else{
				totalPrice = initprice;
				price = (int) (100*totalPrice);
			}
		}else if(1==BaseApplication.valuationType){
			totalPrice = getEveryHourPrice()*valuationTotalTime()/3600;
			getIntervalTime();
			int num = (int)(totalPrice/intervaltime);
			totalPrice = intervaltime*num;
			price = (int) (100*totalPrice);
		}
		
		if(price!=0){
			BigDecimal a = new BigDecimal(String.valueOf(price));
			return String.valueOf(a.divide(new BigDecimal(100), 1, BigDecimal.ROUND_HALF_UP).doubleValue());
		}else{
			return "0.0";
		}
	}
*/

	/*
	最终价格计算（包括节假日）
	 */
	public static String valuationOrderPrice(HolidayData holidayData){
		getInitPrice();
		getInitMiles();
		getEveryMilesPrice();
		getWaitTime();
		getPremiumMiles();
		getPremiumMilesPrice();
		float totalPrice = 0;
		int price = 0;

		//节假日信息
		int type = 0;
		int value = 0;
		if(holidayData!=null){
			type = holidayData.type;
			value = holidayData.value;
		}
		if(0==BaseApplication.valuationType){
			int iDistance = (int) (BaseApplication.lastDistance/10);
			float distance = iDistance/100.0f;
			float premium = 0;
			if(1==BaseApplication.isPremium){
				premium = getPremiumTimePrice();
			}else if(2==BaseApplication.isPremium){
				premium = -getReducetimepricepercent();
			}
			distance = distance+(int)(valuationWaitTime()/(waittime*60f));
			if(distance>initmiles){
				totalPrice = initprice+(distance-initmiles)*everymilesprice;
				if(distance>premiummiles && premiummiles>0){
					premium = premium+premiummilespricepercent;
				}
				totalPrice = (totalPrice*(1+premium*0.01f));
				int num = (int)(totalPrice/intervaltime);
				totalPrice = intervaltime*num;
			}else{
				totalPrice = initprice;
			}
		}else if(1==BaseApplication.valuationType){
			totalPrice = getEveryHourPrice()*valuationTotalTime()/3600;
			int num = (int)(totalPrice/intervaltime);
			totalPrice = intervaltime*num;
		}

		float premium = 0;
		if(1==type){
			premium = -value;
		}else if(2==type){
			premium = value;
		}
		totalPrice = (totalPrice*(1+premium*0.01f));
		int num = (int)(totalPrice/intervaltime);
		totalPrice = intervaltime*num;
		price = (int) (100*totalPrice);

		BaseApplication.orderTotalPrice = "";
		if(price!=0){
			BigDecimal a = new BigDecimal(String.valueOf(price));
			BaseApplication.orderTotalPrice = String.valueOf(a.divide(new BigDecimal(100), 1, BigDecimal.ROUND_HALF_UP).doubleValue());
		}else{
			BaseApplication.orderTotalPrice = "0.0";
		}

		return BaseApplication.orderTotalPrice;
	}
	
	public static long valuationWaitTime(){
		long waitTime = 0;
		waitTime = BaseApplication.waitTimeS+BaseApplication.waitTimeM*60+BaseApplication.waitTimeH*3600;
		return waitTime;
	}
	
	public static long valuationTotalTime(){
		long totalTime = 0;
		totalTime = BaseApplication.usedTimeS+BaseApplication.usedTimeM*60+BaseApplication.usedTimeH*3600;
		return totalTime;
	}
	
	/*
	 * 更新时间溢价值
	 */
	public static void updatePremium(){
		if(1==BaseApplication.valuationType){
			return;
		}
		getPremiumStartTime();
		getPremiumEndTime();
		getPremiumTimePrice();
		getReduceStartTime();
		getReduceEndTime();
		getReducetimepricepercent();
		BaseApplication.isPremium = 0;
		String nowTimeS = DateUtil.getNowDate("HH:mm");
		System.out.println("isPremium:"+nowTimeS);
		try {
			if(!TextUtils.isEmpty(premiumstime) && !TextUtils.isEmpty(premiumetime)){
				String[] startTimes = premiumstime.split(":");
				String[] endTimes = premiumetime.split(":");
				String[] nowTimes = nowTimeS.split(":");
				int startTime = 60*Integer.valueOf(startTimes[0])+Integer.valueOf(startTimes[1]);
				int endTime = 60*Integer.valueOf(endTimes[0])+Integer.valueOf(endTimes[1]);
				int nowTime = 60*Integer.valueOf(nowTimes[0])+Integer.valueOf(nowTimes[1]);
				if(nowTime>=startTime && nowTime<endTime){
					BaseApplication.isPremium = 1;
				}else{
					BaseApplication.isPremium = 0;
				}
				return;
			}else if(!TextUtils.isEmpty(reducestime) && !TextUtils.isEmpty(reduceetime)){
				String[] startTimes = reducestime.split(":");
				String[] endTimes = reduceetime.split(":");
				String[] nowTimes = nowTimeS.split(":");
				int startTime = 60*Integer.valueOf(startTimes[0])+Integer.valueOf(startTimes[1]);
				int endTime = 60*Integer.valueOf(endTimes[0])+Integer.valueOf(endTimes[1]);
				int nowTime = 60*Integer.valueOf(nowTimes[0])+Integer.valueOf(nowTimes[1]);
				if(nowTime>=startTime && nowTime<endTime){
					BaseApplication.isPremium = 2;
				}else{
					BaseApplication.isPremium = 0;
				}
				return;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * 检测App更新
	 */
	public static boolean checkAppUpdate(Context context,ApkInfo apkInfo){
		boolean update = false;
		if(null!=apkInfo && !TextUtils.isEmpty(apkInfo.apkurl)){
			if(apkInfo.version>AppUtils.getVersionNumber(context)){
				update = true;
			}
		}
		
		return update;
	}
	
	/*
	 * 更新App
	 */
	public static void updateApp(Context context,ApkInfo apkInfo){
		String systems = context.getResources().getString(R.string.app_name);

		Intent intent = new Intent(context, UpdateService.class);
		intent.setAction("com.zxw.weidi.update.service");
		intent.setPackage(context.getPackageName());
		intent.putExtra(Constant.DOWNLOAD_TYPE, 3);
		intent.putExtra(Constant.DOWNLOAD_TIP, systems);
		intent.putExtra(Constant.DOWNLOAD_FILE, "weidi_android");
		apkInfo.apkurl = apkInfo.apkurl.replace(" ", "");
		intent.putExtra(Constant.DOWNLOAD_URL, apkInfo.apkurl);
		intent.putExtra(Constant.DOWNLOAD_PACKAGE, "weidi_app");
		context.startService(intent);
	}


	public   static PayOrder getPayOrder( HolidayData holidayData  ){
		String space = "  ";
		PayOrder  payOrder=new PayOrder();
		if(0==BaseApplication.valuationType){
           payOrder.setDiatance("里程："+space+handDistance()+"km");
		   payOrder.setWaitTime("等候："+space+BaseApplication.waitTimeH+"时"+BaseApplication.waitTimeM+"分"+BaseApplication.waitTimeS+"秒");
           payOrder.setUseTime("用时："+space+BaseApplication.usedTimeH+"时"+BaseApplication.usedTimeM+"分"+BaseApplication.usedTimeS+"秒");
		   payOrder.setUnitPrice("单价："+space+getEveryMilesPrice()+"元/公里");
		   payOrder.setSumPrice("总价:  "+valuationOrderPrice(holidayData)+"元");
		   payOrder.setInitPrice("起步价"+getInitPrice()+"元，包含"+getInitMiles()+"公里");
			if(!TextUtils.isEmpty(getPremiumStartTime()) && getPremiumTimePrice()>0){
				payOrder.setPremiumTimePrice(getPremiumStartTime()+"到"+getPremiumEndTime()+"溢价百分"+getPremiumTimePrice());
			}
			if(!TextUtils.isEmpty(getReduceStartTime()) && getReducetimepricepercent()>0){
				payOrder.setReducetimepricepercent(getReduceStartTime()+"到"+getReduceEndTime()+"打折百分"+getReducetimepricepercent());
			}
			if(getPremiumMiles()>0 && getPremiumMilesPrice()>0){
				payOrder.setPremiumMilesPrice("超出"+getPremiumMiles()+"公里，总里程溢价百分"+getPremiumMilesPrice());
			}
	}else if(1==BaseApplication.valuationType){
		getEveryHourPrice();
		//时间
	    payOrder.setUseTime("用时："+space+BaseApplication.usedTimeH+"时"+BaseApplication.usedTimeM+"分"+BaseApplication.usedTimeS+"秒");
	    payOrder.setUnitPrice("单价："+space+getEveryHourPrice()+"元/小时");
        payOrder.setSumPrice("总价：" + space+valuationOrderPrice(holidayData)+"元");
	}
        return  payOrder;
	}
	public static String getOrderInfo(HolidayData holidayData){
		StringBuffer sbf = new StringBuffer();
		String space = "      ";
		if(null!=holidayData && !TextUtils.isEmpty(holidayData.des)){
			sbf.append(holidayData.des);
			sbf.append("\n\n");
		}
		if(0==BaseApplication.valuationType){
			//里程
			sbf.append("里程："+space);
			sbf.append(handDistance()+"km");
			sbf.append("\n");
			sbf.append("等候："+space);
			sbf.append(BaseApplication.waitTimeH+"时"+BaseApplication.waitTimeM+"分"+BaseApplication.waitTimeS+"秒");
			sbf.append("\n");
			sbf.append("用时："+space);
			sbf.append(BaseApplication.usedTimeH+"时"+BaseApplication.usedTimeM+"分"+BaseApplication.usedTimeS+"秒");
			sbf.append("\n");
			sbf.append("单价："+space);
			sbf.append(getEveryMilesPrice()+"元/公里");
			sbf.append("\n");
			sbf.append("总价："+space);
			sbf.append(valuationOrderPrice(holidayData)+"元");
			sbf.append("\n");
			sbf.append("\n");
			sbf.append("起步价"+getInitPrice()+"元，包含"+getInitMiles()+"公里");
			if(!TextUtils.isEmpty(getPremiumStartTime()) && getPremiumTimePrice()>0){
				sbf.append("\n");
				sbf.append(getPremiumStartTime()+"到"+getPremiumEndTime()+"溢价百分"+getPremiumTimePrice());
			}
			if(!TextUtils.isEmpty(getReduceStartTime()) && getReducetimepricepercent()>0){
				sbf.append("\n");
				sbf.append(getReduceStartTime()+"到"+getReduceEndTime()+"打折百分"+getReducetimepricepercent());
			}
			sbf.append("\n");
			if(getPremiumMiles()>0 && getPremiumMilesPrice()>0){
				sbf.append("超出"+getPremiumMiles()+"公里，总里程溢价百分"+getPremiumMilesPrice());
			}
		}else if(1==BaseApplication.valuationType){
			getEveryHourPrice();
			//时间
			sbf.append("用时："+space);
			sbf.append(BaseApplication.usedTimeH+"时"+BaseApplication.usedTimeM+"分"+BaseApplication.usedTimeS+"秒");
			sbf.append("\n");
			sbf.append("单价："+space);
			sbf.append(getEveryHourPrice()+"元/小时");
			sbf.append("\n");
			sbf.append("总价：" + space);
			sbf.append(valuationOrderPrice(holidayData)+"元");
		}
		return sbf.toString();
	}
	public static String handPrice(){
		int price = (int) (100*BaseApplication.totalPrice);
		if(price!=0){
			BigDecimal a = new BigDecimal(String.valueOf(price));
			return String.valueOf(a.divide(new BigDecimal(100), 1, BigDecimal.ROUND_HALF_UP).doubleValue());
		}else{
			return "0.0";
		}
	}
	
	
	public static String handDistance(){
		int price = (int)(BaseApplication.lastDistance/10);
		if(price!=0){
			BigDecimal a = new BigDecimal(String.valueOf(price));
			return String.valueOf(a.divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP).doubleValue());
		}else{
			return "0.00";
		}
	}
	
	
	public static String getPrinterOrderInfo(){
		StringBuffer sbf = new StringBuffer();
		SharedPreferences shared = BaseApplication.getInstance().getSharedPreferences(
				Constant.LOGIN_RESULT_KEY, Context.MODE_PRIVATE);
		int length = 26;
		String area = shared.getString(Constant.USER_INFO_GNAME_KEY, "");
		String userName = shared.getString(Constant.USER_INFO_NAME_KEY, "");
		String carNo = shared.getString(Constant.USER_INFO_CARNO_KEY, "");
		String sgCarNo = shared.getString(Constant.USER_INFO_SGCARDNO_KEY, "");
		String date = BaseApplication.orderDate;
		if(date==null){
			date = "";
		}
		String startTime = BaseApplication.orderStartTime;
		if(startTime==null){
			startTime = "";
		}
		String endTime = BaseApplication.orderEndTime;
		if(endTime==null){
			endTime = "";
		}
		String distance = "";
		String orderId = BaseApplication.orderNo;
		
		if(orderId==null){
			orderId = "";
		}
		String usedTimeH = "00";
		String usedTimeM = "00";
		String usedTimeS = "00";
		if(BaseApplication.usedTimeH>9){
			usedTimeH = String.valueOf(BaseApplication.usedTimeH);
		}else{
			usedTimeH = "0"+String.valueOf(BaseApplication.usedTimeH);
		}
		if(BaseApplication.usedTimeM>9){
			usedTimeM = String.valueOf(BaseApplication.usedTimeM);
		}else{
			usedTimeM = "0"+String.valueOf(BaseApplication.usedTimeM);
		}
		if(BaseApplication.usedTimeS>9){
			usedTimeS = String.valueOf(BaseApplication.usedTimeS);
		}else{
			usedTimeS = "0"+String.valueOf(BaseApplication.usedTimeS);
		}
		String usedTime = usedTimeH+":"+usedTimeM+":"+usedTimeS;
		String price = BaseApplication.orderTotalPrice+"元";
		String unitPrice = "";
		String counterType = "按里程";
		if(0==BaseApplication.valuationType){
			//里程
			counterType = "按里程";
			distance = handDistance()+"km";
			unitPrice = getEveryMilesPrice()+"元／公里";
		}else if(1==BaseApplication.valuationType){
			//时间
			counterType = "按时间";
			unitPrice = getEveryMilesPrice()+"元／小时";
		}
		
		int size = 0;
		int spaceCount = 0;
		
		sbf.append("－－－－－－－－－－－－－－－－");
		sbf.append("\r\n");
		sbf.append("区域：");
		size = area.length()+counterType.length();
		spaceCount = (length/2-size)/2;
		for(int i=0;i<spaceCount;i++){
			sbf.append("  ");
		}
		sbf.append(area);
		spaceCount = (length/2-size)/2+(length/2-size)%2;
		for(int i=0;i<spaceCount;i++){
			sbf.append("  ");
		}
		sbf.append(counterType);
		sbf.append("\r\n");
		sbf.append("司机：");
		spaceCount = length/2-userName.length();
		for(int i=0;i<spaceCount;i++){
			sbf.append("  ");
		}
		sbf.append(userName);
		sbf.append("\r\n");
		sbf.append("车号：");
		int num = (carNo.getBytes().length-carNo.length())/2;
		
		spaceCount = length-(carNo.length()+num);
		for(int i=0;i<spaceCount;i++){
			sbf.append(" ");
		}
		sbf.append(carNo);
		sbf.append("\r\n");
		sbf.append("证号：");
		spaceCount = length-sgCarNo.getBytes().length;
		for(int i=0;i<spaceCount;i++){
			sbf.append(" ");
		}
		sbf.append(sgCarNo);
		sbf.append("\r\n");
		sbf.append("单号：");
		spaceCount = length-orderId.getBytes().length;
		for(int i=0;i<spaceCount;i++){
			sbf.append(" ");
		}
		sbf.append(orderId);
		sbf.append("\r\n");
		sbf.append("日期：");
		spaceCount = length-date.getBytes().length;
		for(int i=0;i<spaceCount;i++){
			sbf.append(" ");
		}
		sbf.append(date);
		sbf.append("\r\n");
		sbf.append("上车：");
		spaceCount = length-startTime.getBytes().length;
		for(int i=0;i<spaceCount;i++){
			sbf.append(" ");
		}
		sbf.append(startTime);
		sbf.append("\r\n");
		sbf.append("下车：");
		spaceCount = length-endTime.getBytes().length;
		for(int i=0;i<spaceCount;i++){
			sbf.append(" ");
		}
		sbf.append(endTime);
		sbf.append("\r\n");
		sbf.append("单价：");
		spaceCount = length/2-unitPrice.length();
		for(int i=0;i<spaceCount;i++){
			sbf.append("  ");
		}
		sbf.append(unitPrice);
		sbf.append("\r\n");
		if(!TextUtils.isEmpty(distance)){
			sbf.append("里程：");
			spaceCount = length-distance.getBytes().length;
			for(int i=0;i<spaceCount;i++){
				sbf.append(" ");
			}
			sbf.append(distance);
			sbf.append("\r\n");
		}
		sbf.append("时间：");
		spaceCount = length-usedTime.getBytes().length;
		for(int i=0;i<spaceCount;i++){
			sbf.append(" ");
		}
		sbf.append(usedTime);
		sbf.append("\r\n");
		sbf.append("金额：");
		spaceCount = length-price.getBytes().length;
		for(int i=0;i<spaceCount;i++){
			sbf.append(" ");
		}
		sbf.append(" ");
		sbf.append(price);
		sbf.append("\r\n");
		sbf.append("－－－－－－－－－－－－－－－－");
		sbf.append("服务单位：温州微的品牌运营管理有限公司");
		sbf.append("\r\n");
		sbf.append("客户电话：40067-40089");
		sbf.append("\r\n");
		sbf.append("微的网站：www.vtaxi.cn");
		sbf.append("\r\n");
		sbf.append("请核对找零，欢迎再次乘坐。");
		sbf.append("\r\n");
		sbf.append("微的，为“短距离”绿色出行服务");
		sbf.append("\r\n");
		sbf.append("\r\n");
		sbf.append("\r\n");
		return sbf.toString();
	}
	
	
	public static float getInitPrice(){
		if(0==initprice){
			initprice = getFloat(Constant.MILE_INIT_PRICE_KEY);
		}
		return initprice;
	}
	
	public static float getInitMiles(){
		if(0==initmiles){
			initmiles = getFloat(Constant.MILE_INIT_MILES_KEY);
		}
		return initmiles;
	}
	
	public static String getPremiumStartTime(){
		if(TextUtils.isEmpty(premiumstime)){
			premiumstime = getString(Constant.MILE_PREMIUM_START_TIME_KEY);
		}
		return premiumstime;
	}
	
	public static String getPremiumEndTime(){
		if(TextUtils.isEmpty(premiumetime)){
			premiumetime = getString(Constant.MILE_PREMIUM_END_TIME_KEY);
		}
		return premiumetime;
	}
	
	public static float getPremiumTimePrice(){
		if(0==premiumtimepricepercent){
			premiumtimepricepercent = getFloat(Constant.MILE_PREMIUM_TIME_PRICE_KEY);
		}
		return premiumtimepricepercent;
	}

	//减价
	public static String getReduceStartTime(){
		if(TextUtils.isEmpty(reducestime)){
			reducestime = getString(Constant.MILE_REDUCE_START_TIME_KEY);
		}
		return reducestime;
	}

	public static String getReduceEndTime(){
		if(TextUtils.isEmpty(reducestime)){
			reduceetime = getString(Constant.MILE_REDUCE_END_TIME_KEY);
		}
		return reduceetime;
	}

	public static float getReducetimepricepercent(){
		if(0==reducepricepercent){
			reducepricepercent = getFloat(Constant.MILE_REDUCE_TIME_PRICE_KEY);
		}
		return reducepricepercent;
	}
	
	public static float getPremiumMiles(){
		if(0==premiummiles){
			premiummiles = getFloat(Constant.MILE_PREMIUM_KM_KEY);
		}
		return premiummiles;
	}
	
	public static float getPremiumMilesPrice(){
		if(0==premiummilespricepercent){
			premiummilespricepercent = getFloat(Constant.MILE_PREMIUM_KM_PRICE_KEY);
		}
		return premiummilespricepercent;
	}
	
	public static int getWaitTime(){
		if(0==waittime){
			waittime = getInt(Constant.MILE_WAIT_TIME_KM_KEY);
		}
		if(waittime==0){
			waittime = 600;
		}
		return waittime;
	}
	
	
	public static float getIntervalTime(){
		if(0==intervaltime){
			intervaltime = getFloat(Constant.MILE_INTERVAL_PRICE_KEY);
		}
		return intervaltime;
	}
	
	public static float getEveryMilesPrice(){
		if(0==everymilesprice){
			everymilesprice = getFloat(Constant.MILE_PER_KM_PRICE_KEY);
		}
		return everymilesprice;
	}
	
	public static float getEveryHourPrice(){
		if(0==everyhourprice){
			everyhourprice = getFloat(Constant.TIME_PER_PRICE_KEY);
		}
		return everyhourprice;
	}
	
	
	public static boolean checkBluetoothOpen(Context context){
		BluetoothAdapter blueAdapter=BluetoothAdapter.getDefaultAdapter(); 
		if(blueAdapter==null || !blueAdapter.isEnabled()){
			return false;
		}
		
		return true;
	}
	
	public static void openBluetooth(Context context){
		Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		// 请求开启蓝牙设备
		context.startActivity(intent);
	}
}
