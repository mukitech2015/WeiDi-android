package com.junen.vtaxi.model;

/**
 * Created by new on 16/7/8.
 */
public class HolidayData {
    public String id;//节假日id
    public String name;//节假日名称
    public int type;//加减类型，1：减价。2：加价
    public int value;
    public String des;//描述
}
