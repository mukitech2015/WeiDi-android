package com.junen.vtaxi.http;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import retrofit.converter.Converter;
import retrofit.mime.FormUrlEncodedTypedOutput;
import retrofit.mime.TypedFile;
import retrofit.mime.TypedInput;
import retrofit.mime.TypedOutput;
import android.text.TextUtils;
import android.util.Log;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.junen.vtaxi.common.Strings;
import com.junen.vtaxi.rpc.RpcException;
import com.junen.vtaxi.rpc.RpcException.ErrorCode;
import com.junen.vtaxi.util.IOUtil;
import com.junen.vtaxi.util.JsonUtils;

public class DwdConverter implements Converter {

	public Object fromBody(TypedInput typedInput, Type type) {
		if (type == TypedInput.class)
			return typedInput;
		String response = "";
		try {
			response = IOUtil.convertStreamToString(typedInput.in());
			JSONObject jsonObject = JsonUtils.parseObject(response);
			int resultCode = jsonObject.getIntValue("status");
			String memo = jsonObject.getString("msg");
			if (resultCode != ErrorCode.OK) {
				String errorCodeStr = jsonObject.getString("errorCode");
				int errorCode = 0;
				if (!TextUtils.isEmpty(errorCodeStr)
						&& (errorCode = Integer.parseInt(errorCodeStr)) != ErrorCode.CLIENT_UNKNOWN_ERROR) {
					resultCode = errorCode;
				}
				throw new RpcException(resultCode, memo);
			}
			if(!jsonObject.containsKey("data")){
				return "";
			}
			Object value = JSON.parseObject(jsonObject.getString("data"), type);
			return value;
		} catch (JSONException e) {
			throw new RpcException(ErrorCode.CLIENT_DESERIALIZER_ERROR,
					Strings.DADA_ERROR_MSG);
		} catch (IOException e) {
			throw new RpcException(ErrorCode.CLIENT_DESERIALIZER_ERROR,
					Strings.DADA_ERROR_MSG);
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public TypedOutput toBody(Object object) {
		if ((object instanceof File))
			return new TypedFile("*/*", (File) object);
		if (object instanceof Map) {
			if (object != null) {
				FormUrlEncodedTypedOutput outPut = new FormUrlEncodedTypedOutput();
				Map map = (Map) object;
				Set<String> keys = map.keySet();
				for (Iterator it = keys.iterator(); it.hasNext();) {
					String key = (String) it.next();
					outPut.addField(key, (String) map.get(key));
				}
				return outPut;
			}
		}
		return null;
	}

	public static TypedFile toTypedFile(String filePath) {
		return new TypedFile("image/png", new File(filePath));
	}

}
