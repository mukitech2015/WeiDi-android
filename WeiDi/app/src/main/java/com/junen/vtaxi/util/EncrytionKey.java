package com.junen.vtaxi.util;

public class EncrytionKey {
	private static EncrytionKey key;
	static {
		System.loadLibrary("encrytkey");
	}

	public native String getEncrytionKey();

	public static String getEncrytKey() {
		if (key == null) {
			synchronized (EncrytionKey.class) {
				key = new EncrytionKey();
			}

		}

		return key.getEncrytionKey();

	}
}
