package com.junen.vtaxi.service;

import android.app.Service;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;

import com.junen.vtaxi.common.Constant;
import com.junen.vtaxi.engine.AccountEngine;
import com.junen.vtaxi.model.AudioFile;
import com.junen.vtaxi.model.VideoInfo;

import java.io.File;
import java.util.ArrayList;

public class RemoveFileService extends Service {
	private  String   downFileType;
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}
	@Override
	public void onCreate() {
		super.onCreate();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
	       if(intent==null){
		     return 0;
    	   }
		downFileType=intent.getStringExtra(Constant.DOWNLOAD_FILE_TYPE);
		removeOtherFile();
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	private void removeOtherFile() {
		try {
			String sDStateString = Environment.getExternalStorageState();
			if (sDStateString.equals(Environment.MEDIA_MOUNTED)) {
				File SDFile = Environment.getExternalStorageDirectory();
					File sdPath = new File(SDFile.getAbsolutePath()+"/weidi");
					ArrayList<VideoInfo> list = AccountEngine.getVideoArray();
					if(sdPath.listFiles().length > 0 && list!=null){
						for(File file : sdPath.listFiles()){
							boolean canRemove = true;
							for(int i=0;i<list.size();i++){
								VideoInfo videoInfo = list.get(i);
								if(file.getName().startsWith(videoInfo.id+".")){
									canRemove = false;
									break;
								}
							}
							if(canRemove){
								file.delete();
							}
						}
				}
			}
			this.stopSelf();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
