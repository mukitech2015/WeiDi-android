package com.junen.vtaxi.activity.base;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.view.View;

import com.junen.vtaxi.app.BaseApplication;
import com.junen.vtaxi.widget.DialogHelper;

/**
 * Activity辅助类
 * 
 * @author menglin.li
 * 
 */
public class ActivityHelper {
	final static String TAG = ActivityHelper.class.getSimpleName();

	/**
	 * 对应的Activity
	 */
	private Activity mActivity;

	/**
	 * 对应的App
	 */
	protected BaseApplication mApp;

	/**
	 * 对话框帮助类
	 */
	private DialogHelper mDialogHelper;

	public ActivityHelper(Activity activity) {
		mActivity = activity;
		mApp = BaseApplication.getInstance();
		mDialogHelper = new DialogHelper(mActivity);
		mApp.pushActivity(mActivity);
	}

	public void finish() {
		if (mApp != null)
			mApp.removeActivity(mActivity);
		mDialogHelper.dismissProgressDialog();
		mDialogHelper.dismissAlertDialog();
	}

	/**
	 * 弹对话框
	 * 
	 * @param title
	 *            标题
	 * @param msg
	 *            消息
	 * @param positive
	 *            确定
	 * @param positiveListener
	 *            确定回调
	 * @param negative
	 *            否定
	 * @param negativeListener
	 *            否定回调
	 */
	public void alert(String title, String msg, String positive,
			DialogInterface.OnClickListener positiveListener, String negative,
			DialogInterface.OnClickListener negativeListener) {
		mDialogHelper.alert(title, msg, positive, positiveListener, negative,
				negativeListener);
	}

	/**
	 * 
	 * 弹对话框
	 * 
	 * @param title
	 *            标题
	 * @param msg
	 *            消息
	 * @param positive
	 *            确定
	 * @param positiveListener
	 *            确定回调
	 * @param negative
	 *            否定
	 * @param negativeListener
	 *            否定回调
	 * @param isCanceledOnTouchOutside
	 *            外部是否可点取消
	 */
	public void alert(String title, String msg, String positive,
			DialogInterface.OnClickListener positiveListener, String negative,
			DialogInterface.OnClickListener negativeListener,
			Boolean isCanceledOnTouchOutside) {
		mDialogHelper.alert(title, msg, positive, positiveListener, negative,
				negativeListener, isCanceledOnTouchOutside);
	}

	/**
	 * 
	 * 弹对话框
	 * 
	 * @param title
	 *            标题
	 * @param msg
	 *            消息
	 * @param positive
	 *            确定
	 * @param positiveListener
	 *            确定回调
	 * @param negative
	 *            否定
	 * @param negativeListener
	 *            否定回调
	 * @param isCanceledOnTouchOutside
	 *            外部是否可点取消
	 */
	public void customAlert(String title, String msg, String positive,
			View.OnClickListener positiveListener, String negative,
			View.OnClickListener negativeListener,
			Boolean isCanceledOnTouchOutside, Boolean isCanceledBackClick) {
		mDialogHelper.customAlert(title, msg, positive, positiveListener,
				negative, negativeListener, isCanceledOnTouchOutside,
				isCanceledBackClick);
	}

	/**
	 * TOAST
	 * 
	 * @param msg
	 *            消息
	 * @param period
	 *            时长
	 */
	public void toast(String msg, int period) {
		mDialogHelper.toast(msg, period);
	}

	/**
	 * 显示进度对话框
	 * 
	 * @param msg
	 *            消息
	 */
	public void showProgressDialog(String msg) {
		mDialogHelper.showProgressDialog(msg);
	}

	/**
	 * 显示可取消的进度对话框
	 * 
	 * @param msg
	 *            消息
	 */
	public void showProgressDialog(final String msg, final boolean cancelable,
			final OnCancelListener cancelListener) {
		mDialogHelper.showProgressDialog(msg, cancelable, cancelListener, true);
	}

	public void dismissProgressDialog() {
		mDialogHelper.dismissProgressDialog();
	}
	
	public void dismissAlertDialog() {
		mDialogHelper.dismissAlertDialog();
	}
}
