package com.junen.vtaxi.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.junen.vtaxi.app.BaseApplication;
import com.junen.vtaxi.common.Constant;

public class PrinterUtils {
	public static String getPrinterText(){
		SharedPreferences shared = BaseApplication.getInstance().getSharedPreferences(
				Constant.LOGIN_RESULT_KEY, Context.MODE_PRIVATE);
		
		
		int length = 26;
		String area = "龙港";
		String counterType = "按时间";
		String userName = shared.getString(Constant.USER_INFO_NAME_KEY, "");
		String carNo = shared.getString(Constant.USER_INFO_CARNO_KEY, "");
		String sgCarNo = shared.getString(Constant.USER_INFO_SGCARDNO_KEY, "");
		String orderId = "567899766";
		String date = "2016-04-20";
		String startTime = "17:25";
		String endTime = "17:25";
		String distance = "10.2km";
		String usedTime = "00:20:08";
		String price = "20.0元";
		
		int size = 0;
		int spaceCount = 0;
		StringBuffer sbf = new StringBuffer();
		sbf.append("－－－－－－－－－－－－－－－－");
		sbf.append("\r\n");
		sbf.append("区域：");
		size = area.length()+counterType.length();
		spaceCount = (length/2-size)/2;
		for(int i=0;i<spaceCount;i++){
			sbf.append("  ");
		}
		sbf.append(area);
		for(int i=0;i<spaceCount;i++){
			sbf.append("  ");
		}
		sbf.append(counterType);
		sbf.append("\r\n");
		sbf.append("司机：");
		spaceCount = length/2-userName.length();
		for(int i=0;i<spaceCount;i++){
			sbf.append("  ");
		}
		sbf.append(userName);
		sbf.append("\r\n");
		sbf.append("车号：");
		spaceCount = length-carNo.getBytes().length;
		for(int i=0;i<spaceCount;i++){
			sbf.append(" ");
		}
		sbf.append(carNo);
		sbf.append("\r\n");
		sbf.append("证号：");
		spaceCount = length-sgCarNo.getBytes().length;
		for(int i=0;i<spaceCount;i++){
			sbf.append(" ");
		}
		sbf.append(sgCarNo);
		sbf.append("\r\n");
		sbf.append("单号：");
		spaceCount = length-orderId.getBytes().length;
		for(int i=0;i<spaceCount;i++){
			sbf.append(" ");
		}
		sbf.append(orderId);
		sbf.append("\r\n");
		sbf.append("日期：");
		spaceCount = length-date.getBytes().length;
		for(int i=0;i<spaceCount;i++){
			sbf.append(" ");
		}
		sbf.append(date);
		sbf.append("\r\n");
		sbf.append("上车：");
		spaceCount = length-startTime.getBytes().length;
		for(int i=0;i<spaceCount;i++){
			sbf.append(" ");
		}
		sbf.append(startTime);
		sbf.append("\r\n");
		sbf.append("下车：");
		spaceCount = length-endTime.getBytes().length;
		for(int i=0;i<spaceCount;i++){
			sbf.append(" ");
		}
		sbf.append(endTime);
		sbf.append("\r\n");
		sbf.append("单价：");
		sbf.append("\r\n");
		sbf.append("里程：");
		spaceCount = length-distance.getBytes().length;
		for(int i=0;i<spaceCount;i++){
			sbf.append(" ");
		}
		sbf.append(distance);
		sbf.append("\r\n");
		sbf.append("时间：");
		spaceCount = length-usedTime.getBytes().length;
		for(int i=0;i<spaceCount;i++){
			sbf.append(" ");
		}
		sbf.append(usedTime);
		sbf.append("\r\n");
		sbf.append("金额：");
		spaceCount = length-price.getBytes().length;
		for(int i=0;i<spaceCount;i++){
			sbf.append(" ");
		}
		sbf.append(" ");
		sbf.append(price);
		sbf.append("\r\n");
		sbf.append("－－－－－－－－－－－－－－－－");
		sbf.append("服务单位：温州微的品牌运营管理有限公司");
		sbf.append("\r\n");
		sbf.append("客户电话：40067-40089");
		sbf.append("\r\n");
		sbf.append("微的网站：www.vtaxi.cn");
		sbf.append("\r\n");
		sbf.append("请核对找零，欢迎再次乘坐。");
		sbf.append("\r\n");
		sbf.append("微的，为“短距离”绿色出行服务");
		sbf.append("\r\n");
		sbf.append("\r\n");
		sbf.append("\r\n");
		return sbf.toString();
	}
	
	
}
