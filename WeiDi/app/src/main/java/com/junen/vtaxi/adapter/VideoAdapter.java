package com.junen.vtaxi.adapter;

import java.io.File;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.junen.vtaxi.R;
import com.junen.vtaxi.model.VideoInfo;
import com.junen.vtaxi.util.SdUtil;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class VideoAdapter extends BaseAdapter {
	private Context context;
	private LayoutInflater mInflater;
	private ArrayList<VideoInfo> videoArray;
	private boolean isSdPath = false;
	
	//图片下载
	private ImageLoader imageLoader;
	private DisplayImageOptions options;
		
	public VideoAdapter(Context context,
			ArrayList<VideoInfo> videoArray,boolean isSdPath) {
		super();
		this.context = context;
		this.isSdPath = isSdPath;
		mInflater = LayoutInflater.from(this.context);
		this.videoArray = videoArray;
		if(videoArray==null){
			videoArray = new ArrayList<VideoInfo>();
		}
		initImageLoader();
	}

	@Override
	public int getCount() {
		return videoArray.size();
	}

	@Override
	public Object getItem(int position) {
		return videoArray.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		if(convertView==null){
			holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.list_video_download_item, null);
			holder.image = (ImageView)convertView.findViewById(R.id.image_view);
			holder.name = (TextView)convertView.findViewById(R.id.video_name_view);
			holder.delete = (TextView)convertView.findViewById(R.id.video_delete_view);
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
		
		VideoInfo videoInfo = videoArray.get(position);
		if(videoInfo!=null){
			holder.name.setText(videoInfo.name);
			if(isSdPath){
				holder.image.setImageBitmap(getVideoThumbnail(videoInfo.sdPath, 160, 116, 
						MediaStore.Images.Thumbnails.MICRO_KIND));
				//holder.delete.setVisibility(View.VISIBLE);
			}else{
				Log.e("", "videoInfo.logourl:"+videoInfo.logourl);
				imageLoader.displayImage(videoInfo.logourl,holder.image, options);
				//holder.delete.setVisibility(View.GONE);
			}
		}
		return convertView;
	}

	public class ViewHolder{
		public ImageView image;
		public TextView name;
		public TextView delete;
	}
	
	
	/** 
     * 获取视频的缩略图 
     * 先通过ThumbnailUtils来创建一个视频的缩略图，然后再利用ThumbnailUtils来生成指定大小的缩略图。 
     * 如果想要的缩略图的宽和高都小于MICRO_KIND，则类型要使用MICRO_KIND作为kind的值，这样会节省内存。 
     * @param videoPath 视频的路径 
     * @param width 指定输出视频缩略图的宽度 
     * @param height 指定输出视频缩略图的高度度 
     * @param kind 参照MediaStore.Images.Thumbnails类中的常量MINI_KIND和MICRO_KIND。 
     *            其中，MINI_KIND: 512 x 384，MICRO_KIND: 96 x 96 
     * @return 指定大小的视频缩略图 
     */  
    private Bitmap getVideoThumbnail(String videoPath, int width, int height,  
            int kind) {  
        Bitmap bitmap = null;
		try {
			bitmap = null;  
			// 获取视频的缩略图  
			bitmap = ThumbnailUtils.createVideoThumbnail(videoPath, kind);
			/*
			bitmap = ThumbnailUtils.extractThumbnail(bitmap, width, height,  
			        ThumbnailUtils.OPTIONS_RECYCLE_INPUT);
			        */
		} catch (Exception e) {
			e.printStackTrace();
		}  
        return bitmap;  
    } 
    
    
    private void initImageLoader(){
		// 初始化imageLoader 否则会报错
		imageLoader = ImageLoader.getInstance();
		imageLoader.init(ImageLoaderConfiguration.createDefault(context));
		
		options = new DisplayImageOptions.Builder()
		.cacheInMemory(false) // 设置下载的图片是否缓存在内存中
		.cacheOnDisc(false) // 设置下载的图片是否缓存在SD卡中
		.build();
	}
}
