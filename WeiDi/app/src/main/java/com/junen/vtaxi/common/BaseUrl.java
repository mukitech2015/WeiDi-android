package com.junen.vtaxi.common;

import android.text.TextUtils;

public class BaseUrl {


	//public static  String IP = "http://192.168.1.188:19080";//develop


	// public static final String IP = "http://60.191.68.44:19080";//develop

//    public static String IP = "http://hulk.dianwoda.cn";

	// public static  String IP = "http://192.168.1.144:19080";
	// test_inner

	 //public static String IP = "http://60.191.68.46:19080";//test
	 public static String IP = "http://114.215.239.74";//test

	// public static final String IP = "http://testhulk.dianwoda.com";//new

    public String url;
    public static String UPLOAD_URL = IP + "/apis/image/upload.json";

    public static String ACCOUNT_NEED_TO_KNOW_URL = IP
            + "/h5/account.jsp";
    public static String MAKE_MONEY_URL = IP
            + "/h5/rider-invitation.html?riderId=";
    public static String MAKE_MONEY_HISTORY = IP
            + "/h5/invitation-record.html?riderId=";
    
    //骑士排班 /h5/rider-schedules/{riderId}.html?cityId={cityId}
    public static String KNIGHT_SCHEDULE_IP = IP + "/h5/rider-schedules/";
    //购买装备
    public static String BUY_EQUIPMENT = IP + "/h5/buy.html";
    //订单计费详情h5 /h5/billing-order.html?cityId={cityId}&orderId={orderId}&riderId={riderId}&his={0|1}
    //his  0:非历史订单 1:历史订单
    public static String ORDER_FEE_DETAIL = IP + "/h5/billing-order.html?";
    //老版订单计费规则h5
    public static String ORDER_FEE_DETAIL_OLD = IP + "/h5/billing.html";

    //新手接单奖励规则h5
    public static String NEW_USER_REWARD = IP + "/h5/newRunners-rule.html";
    public static String SHARE_URL = IP + "/h5/rider-invitation-sharing.html?riderId=";
}
