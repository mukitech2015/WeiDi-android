package com.junen.vtaxi.model;

public class UserInfo {
	public String id;
	public String name;
	public String phone;
	public String carno;
	public String sgcardno;
	public String address;
	public String logourl;
	public String money;
	public String score;
	public String ordernum;
	public String gname;
	public int sex;
}
