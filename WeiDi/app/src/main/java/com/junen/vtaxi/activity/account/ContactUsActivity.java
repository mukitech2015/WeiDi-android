package com.junen.vtaxi.activity.account;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.junen.vtaxi.R;
import com.junen.vtaxi.activity.base.BaseActivity;
import com.junen.vtaxi.http.ApiClient;
import com.junen.vtaxi.model.ShopResult;
import com.junen.vtaxi.rpc.RpcExcutor;
import com.junen.vtaxi.rpc.api.RpcApi;

public class ContactUsActivity extends BaseActivity {
	
	/** 公司信息 */
	private RpcExcutor<ShopResult> shopExcutor;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contact_us);
		
		initView();
		initData();
	}

	@Override
	public void initView() {
		super.initView();
		ImageView backView = (ImageView)findViewById(R.id.back_view);
		backView.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				back();
			}
		});
	}

	@Override
	public void initData() {
		super.initData();
		initRpcExcutor();
		shopExcutor.start();
	}
	
	
	protected void initRpcExcutor() {
		shopExcutor = new RpcExcutor<ShopResult>(this, 0, false) {
			@Override
			public void excute(Object... params) {
				ApiClient.createApi(ContactUsActivity.this, RpcApi.class)
						.getShopInfo(4,this);
			}

			@Override
			public void onRpcFinish(ShopResult result, Object... params) {
				doResult(result);
			}

			@Override
			public void onRpcException(int code, String msg, Object... params) {
				toast(msg, Toast.LENGTH_SHORT);
			}

		};
		shopExcutor.setShowProgressDialog(true);
		shopExcutor.setShowNetworkErrorView(false);
	}
	
	private void doResult(ShopResult result){
		if(result==null || null==result.info){
			toast("网络异常，请重试", Toast.LENGTH_LONG);
			return;
		}
		TextView nameView = (TextView)findViewById(R.id.company_name_view);
		TextView addressView = (TextView)findViewById(R.id.service_address_view);
		TextView telView = (TextView)findViewById(R.id.service_hotline_view);
		nameView.setText(result.info.prisename);
		addressView.setText(result.info.address);
		telView.setText(result.info.tel);
	}
}
